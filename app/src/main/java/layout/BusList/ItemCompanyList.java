package layout.BusList;

/**
 * Created by DELL3 on 2018-11-13.
 */

public class ItemCompanyList {

    boolean add_item;
    String company_name;
    String company_code;

    public ItemCompanyList(boolean add_item, String company_code, String company_name) {

        this.add_item = add_item;
        this.company_code = company_code;
        this.company_name = company_name;
    }

    public boolean getCompany_add(){return add_item;}
    public String getCompany_name(){return company_name;}
    public String getCompany_code(){return company_code;}

    public void setCompany_add(boolean add_item){this.add_item = add_item;}
    public void setCompany_name(String company_name){this.company_name = company_name;}
    public void setCompany_code(String company_code){this.company_code = company_code;}

}
