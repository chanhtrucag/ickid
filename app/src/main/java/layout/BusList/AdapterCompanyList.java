package layout.BusList;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kidsoft.buscare.R;

/**
 * Created by DELL3 on 2018-11-13.
 */

public class AdapterCompanyList extends ArrayAdapter<ItemCompanyList> {

    private List<ItemCompanyList> itemList = new ArrayList<>();

    private static class ItemViewHolder
    {
        ImageView imgAdd;
        TextView tvCompany;

    }

    public AdapterCompanyList(Context context, int resource, List<ItemCompanyList> itemList) {
        super(context, resource, itemList);

        this.itemList = itemList;
    }

    public void add(ItemCompanyList object) {
        itemList.add(object);
        super.add(object);
    }

    public int getCount() {
        return this.itemList.size();
    }

    @Override
    public ItemCompanyList getItem(int index){return this.itemList.get(index);}

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getView(position, convertView, parent);
    }


    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View row = convertView;
        AdapterCompanyList.ItemViewHolder viewHolder;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.layout_spinner_company_item, parent, false);

            viewHolder = new ItemViewHolder();
            viewHolder.tvCompany = (TextView)row.findViewById(R.id.sp_company_name_txt);
            viewHolder.imgAdd = (ImageView)row.findViewById(R.id.sp_company_add);
            row.setTag(viewHolder);

        } else {
            viewHolder = (ItemViewHolder) row.getTag();
        }

        ItemCompanyList item = itemList.get(position);

        if(item.getCompany_add() == true)
        {
            if(item.getCompany_code().equals("input_new_company") == true)
            {
                viewHolder.imgAdd.setVisibility(View.INVISIBLE);
                viewHolder.tvCompany.setVisibility(View.VISIBLE);
                viewHolder.tvCompany.setText(item.getCompany_name());
            }
            else
            {
                viewHolder.imgAdd.setVisibility(View.VISIBLE);
                viewHolder.tvCompany.setVisibility(View.INVISIBLE);
                viewHolder.tvCompany.setText("");
            }
        }
        else
        {
            viewHolder.imgAdd.setVisibility(View.GONE);
            viewHolder.tvCompany.setVisibility(View.VISIBLE);
            viewHolder.tvCompany.setText(item.getCompany_name());
        }
        return row;
    }
}
