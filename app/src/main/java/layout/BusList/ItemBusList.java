package layout.BusList;

/**
 * Created by DELL3 on 2018-10-25.
 */

public class ItemBusList {
    
    boolean add_item;
    boolean checked = false;
    String company_name;
    String company_code;
    String bus_name;
    String bus_code;
    int bus_colar;
    boolean bus_alarm;
    
    public ItemBusList(boolean add_item, boolean bus_alarm, String bus_name, String bus_code, int bus_colar, String company_name, String company_code) {

        this.add_item = add_item;
        this.bus_alarm = bus_alarm;
        this.bus_name = bus_name;
        this.bus_code = bus_code;
        this.bus_colar = bus_colar;

        this.company_name = company_name;
        this.company_code = company_code;
    }

    public boolean getBus_add(){return add_item;}
    public boolean getBus_checked(){return checked;}
    public boolean getBus_alarm(){return bus_alarm;}
    public String getBus_name(){return bus_name;}
    public String getBus_code(){return bus_code;}
    public int getBus_colar(){return bus_colar;}
    public String getCompany_name(){return company_name;}
    public String getCompany_code(){return  company_code;}

    public void setBus_add(boolean add_item){this.add_item = add_item;}
    public void setBus_checked(boolean checked){this.checked  = checked;}
    public void setBus_alarm(boolean bus_alarm){this.bus_alarm = bus_alarm;}
    public void setBus_name(String bus_name){this.bus_name = bus_name;}
    public void setBus_code(String bus_code){this.bus_code = bus_code;}
    public void setBus_colar(int bus_colar){this.bus_colar = bus_colar;}
    public void setCompany_name(String company_name){this.company_name = company_name;}
    public void setCompany_code(String company_code){this.company_code = company_code;}
    
}
