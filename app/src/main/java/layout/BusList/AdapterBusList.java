package layout.BusList;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kidsoft.buscare.BusAlarm.AlarmSettingActivity;
import kidsoft.buscare.BusInfo.NewBusAddActivity;
import kidsoft.buscare.BusList.DeleteBusInfoDialog;
import kidsoft.buscare.BusList.MainActivity;
import kidsoft.buscare.BusList.ModifyBusInfoDialog;
import kidsoft.buscare.BusSeatService.BusSeatServiceActivity;
import kidsoft.buscare.Database.DBHelper;
import kidsoft.buscare.Database.models.AlarmInfo;
import kidsoft.buscare.Database.models.BusInfo;
import kidsoft.buscare.Database.models.SeatInfo;
import kidsoft.buscare.R;

import static kidsoft.buscare.BusAlarm.AlarmSettingAdapter.SERVICE_START_ALARM;
import static kidsoft.buscare.BusInfo.SeatInfoActivity.REQUEST_CODE_SET_BUS_INFO;

/**
 * Created by DELL3 on 2018-10-25.
 */

public class AdapterBusList extends ArrayAdapter<ItemBusList> {


    private String TAG = "AdapterBusList";

    private DBHelper _dbHelper;

    public final static int ADD_BUS = -1, GREY = 0, RED = 1, ORANGE = 2 , YELLOW = 3, GREEN = 4, BLUE = 5 ;
    private List<ItemBusList> itemList = new ArrayList<>();
    private boolean[] isCheckedConfrim;
    private boolean checkBoxEnable;

    private Context context;
    private Activity activity;
    private MainActivity mainActivity;

    private static class ItemViewHolder {

        CheckBox checkBox;
        ImageView alarm;
        TextView busInfo;
        ImageView moreMenu;

    }

    public void setCheckBoxEnable(boolean enable)
    {
        this.checkBoxEnable = enable;

    }


    public void setAllChecked(boolean ischeked) {
        int tempSize = isCheckedConfrim.length;
        for(int a=0 ; a<tempSize ; a++){
            isCheckedConfrim[a] = ischeked;
        }
    }


    public void setChecked(int position) {
        isCheckedConfrim[position] = !isCheckedConfrim[position];
    }


    public void add(ItemBusList object) {
        itemList.add(object);
        super.add(object);
    }

    public int getCount() {
        return this.itemList.size();
    }


    @Override
    public ItemBusList getItem(int index){return this.itemList.get(index);}

    public long getItemId(int position) {
        return position;
    }

    public AdapterBusList(Context context, Activity activity, MainActivity mAct, int resource, List<ItemBusList> itemList, String company_name, String company_code) {
        super(context, resource, itemList);
        this.context = context;
        this.activity = activity;
        this.itemList = itemList;
        this.mainActivity = mAct;
        _dbHelper = new DBHelper(context);


    }

    @NonNull
    public View getView(int position, final View convertView, @NonNull ViewGroup parent)
    {
        View row = convertView;
        ItemViewHolder viewHolder;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.layout_buslist_item, parent, false);

            viewHolder = new ItemViewHolder();
            viewHolder.checkBox = (CheckBox)row.findViewById(R.id.bl_item_checkBox);
            viewHolder.alarm = (ImageView)row.findViewById(R.id.bl_item_alarm);
            viewHolder.busInfo = (TextView)row.findViewById(R.id.bl_item_info);
            viewHolder.moreMenu = (ImageView)row.findViewById(R.id.bl_item_more);
            row.setTag(viewHolder);

        } else {
            viewHolder = (ItemViewHolder) row.getTag();
        }

        /*
        viewHolder.checkBox.setClickable(false);
        viewHolder.checkBox.setFocusable(false);
        */

        final ItemBusList item = itemList.get(position);


        if(item.getBus_alarm() == false)
        {
            viewHolder.alarm.setBackgroundResource(R.drawable.alarm_off);
        }
        else {
            viewHolder.alarm.setBackgroundResource(R.drawable.alarm_on);
        }

        // "GREY, RED, ORANGE, YELLOW, GREEN, BLUE"

        if(item.getBus_name().isEmpty() == false) {
            viewHolder.busInfo.setText(item.getBus_name() + "(" + item.getBus_code()  +")");
        }
        else
        {
            viewHolder.busInfo.setText("");
        }

        if(item.getBus_add() == true)
        {
            viewHolder.checkBox.setVisibility(View.GONE);
            //viewHolder.checkBox.setChecked(false);
            //item.setBus_checked(false);

            viewHolder.alarm.setVisibility(View.GONE);
            viewHolder.moreMenu.setVisibility(View.GONE);
        }
        else
        {
            if(checkBoxEnable == false) {
                viewHolder.checkBox.setVisibility(View.GONE);
                viewHolder.alarm.setEnabled(true);
                //viewHolder.checkBox.setChecked(false);
                //item.setBus_checked(false);
            }else
            {
                viewHolder.checkBox.setVisibility(View.VISIBLE);
                viewHolder.alarm.setEnabled(false);
                //viewHolder.checkBox.setChecked(false);
                //item.setBus_checked(false);
            }

            viewHolder.alarm.setVisibility(View.VISIBLE);
            viewHolder.moreMenu.setVisibility(View.VISIBLE);
        }


        switch(item.getBus_colar())
        {
            case ADD_BUS:
                viewHolder.busInfo.setBackgroundResource(R.drawable.bus_add);
                break;
            case GREY:
                viewHolder.busInfo.setBackgroundResource(R.drawable.bus_gray);
                break;
            case RED:
                viewHolder.busInfo.setBackgroundResource(R.drawable.bus_red);
                break;
            case ORANGE:
                viewHolder.busInfo.setBackgroundResource(R.drawable.bus_orange);
                break;
            case YELLOW:
                viewHolder.busInfo.setBackgroundResource(R.drawable.bus_yellow);
                break;
            case GREEN:
                viewHolder.busInfo.setBackgroundResource(R.drawable.bus_green);
                break;
            case BLUE:
                viewHolder.busInfo.setBackgroundResource(R.drawable.bus_blue);
                break;
            default:
                viewHolder.busInfo.setBackgroundResource(R.drawable.bus_gray);
                break;
        }


        viewHolder.busInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e(TAG, "Clicked busInfo " + item.getBus_name() + " " + item.getBus_code());
                Log.e(TAG, "item.getBus_add() " + item.getBus_add());

                if(item.getBus_add() == true) {
                    if(item.getCompany_code() != null) {
                        Intent i = new Intent(context, NewBusAddActivity.class);
                        i.putExtra("company_name", item.getCompany_name());
                        i.putExtra("company_code", item.getCompany_code());
                        activity.startActivityForResult(i, REQUEST_CODE_SET_BUS_INFO);
                    }
                }
                else {
                    List<BusInfo> selectedBus = _dbHelper.mBus.QueryByCode(item.getBus_name(), item.getBus_code());
                    Intent mIntent = new Intent(context, BusSeatServiceActivity.class);
                    mIntent.putExtra("bus_name", selectedBus.get(0).getBusName());
                    mIntent.putExtra("bus_code", selectedBus.get(0).getBusCode());
                    mIntent.putExtra("bus_seat", selectedBus.get(0).getBusSeat());
                    activity.startActivity(mIntent);
                }
            }
        });

        viewHolder.alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "Clicked alarm " + item.getBus_alarm() );
                List<BusInfo> selectedBus = _dbHelper.mBus.QueryByCode(item.getBus_name(), item.getBus_code());
                Intent mIntent = new Intent(context, AlarmSettingActivity.class);
                mIntent.putExtra("bus_id",  selectedBus.get(0).getId());
                mIntent.putExtra("bus_name", selectedBus.get(0).getBusName());
                mIntent.putExtra("bus_code", selectedBus.get(0).getBusCode());
                mIntent.putExtra("bus_seat", selectedBus.get(0).getBusSeat());
                activity.startActivity(mIntent);

            }
        });

        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                item.setBus_checked(isChecked);
                Log.e(TAG, "Clicked checkBox "  +  item.getBus_name() +  "  " +  item.getBus_code() +  " " + item.getBus_checked());
            }
        });

        if(item.getBus_checked() == true)
        {
            viewHolder.checkBox.setChecked(true);
        }
        else
        {
            viewHolder.checkBox.setChecked(false);
        }

        viewHolder.moreMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "Clicked moreMenu " );

//                final String[] STRINGS = {"버스정보 수정","버스 삭제"};
                //"NCT-Vi"
                final String[] STRINGS = {"Sửa thông tin","Xóa thông tin"};
                final ListPopupWindow mPopupWindow = new ListPopupWindow(context);
                mPopupWindow.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, STRINGS));
                mPopupWindow.setVerticalOffset(-1 * v.getHeight());
                mPopupWindow.setAnchorView(v);
                mPopupWindow.setDropDownGravity(Gravity.RIGHT);
                mPopupWindow.setWidth(context.getResources().getDimensionPixelSize(R.dimen.overflow_width_0));
                mPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.e(TAG, "Clicked moreMenu " + position );

                        mPopupWindow.dismiss();

                        if(position == 0)
                        {
                            final ModifyBusInfoDialog modifyBusInfoDialog = new ModifyBusInfoDialog(activity, item.getBus_name(), item.getBus_code());
                            modifyBusInfoDialog.show();
                            modifyBusInfoDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                    if(modifyBusInfoDialog.getResult() == true)
                                    {

                                        String[] newBus = modifyBusInfoDialog.getNewBusInfo();
                                        Log.e(TAG, "new bus name : " + newBus[0]  + " new bus code : " + newBus[1]);

                                        if(newBus[0].isEmpty() || newBus[1].isEmpty())
                                        {
//                                            Log.e("AdpaterBusSeat", "Already resisted number" );
//                                            AlertDialog.Builder alt_bld = new AlertDialog.Builder(activity);
//                                            alt_bld.setTitle("차량 정보가 비어있습니다");
//                                            alt_bld.setMessage("차량 정보를 다시 입력해주세요.").setCancelable(
//                                                    false).setPositiveButton("확인",
//                                                    new DialogInterface.OnClickListener() {
//                                                        public void onClick(DialogInterface dialog, int id) {
//                                                            dialog.cancel();
//                                                        }
//                                                    });
//                                            AlertDialog alert = alt_bld.create();
//                                            alert.show();
                                            //"NCT-Vi"
                                            Log.e("AdpaterBusSeat", "Already resisted number" );
                                            AlertDialog.Builder alt_bld = new AlertDialog.Builder(activity);
                                            alt_bld.setTitle("Thông tin xe trống");
                                            alt_bld.setMessage("Vui lòng nhập lại thông tin xe.").setCancelable(
                                                    false).setPositiveButton("OK",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    });
                                            AlertDialog alert = alt_bld.create();
                                            alert.show();

                                            return;
                                        }

                                        if(_dbHelper.mBus.CheckDuplicate(newBus[0], newBus[1]) == false)
                                        {
                                            //SEAT 정보 수정
                                            List<SeatInfo>  mSeats = _dbHelper.mSeat.QuerybyCode(item.getBus_name(), item.getBus_code());
                                            List<AlarmInfo> mAlarms = _dbHelper.mAlarm.QueryByCode(item.getBus_name(), item.getBus_code());

                                            for(int i = 0; i< mSeats.size(); ++i)
                                            {
                                                mSeats.get(i).setBusName(newBus[0]);
                                                mSeats.get(i).setBusCode(newBus[1]);
                                                _dbHelper.mSeat.Update(mSeats.get(i));
                                            }

                                            //ALARM 정보 수정
                                            for(int i = 0 ; i< mAlarms.size(); ++i)
                                            {
                                                mAlarms.get(i).setBusName(newBus[0]);
                                                mAlarms.get(i).setBusCode(newBus[1]);
                                                _dbHelper.mAlarm.Update(mAlarms.get(i));
                                            }

                                            //BUS 정보 수정
                                            List<BusInfo> mBus = _dbHelper.mBus.QueryByCode(item.getBus_name(), item.getBus_code());
                                            for(int i = 0; i< mBus.size(); ++i)
                                            {
                                                mBus.get(i).setBusName(newBus[0]);
                                                mBus.get(i).setBusCode(newBus[1]);
                                                _dbHelper.mBus.Update(mBus.get(i));
                                            }

                                            mainActivity.refreshBusList();
                                        }
                                        else
                                        {
                                            Log.e(TAG, "Existed Bus!!");

//                                            Log.e("AdpaterBusSeat", "Already resisted number" );
//                                            AlertDialog.Builder alt_bld = new AlertDialog.Builder(activity);
//                                            alt_bld.setTitle("이미 등록된 차량 정보가 있습니다");
//                                            alt_bld.setMessage("다른 이름이나 번호로 등록해주세요.").setCancelable(
//                                                    false).setPositiveButton("확인",
//                                                    new DialogInterface.OnClickListener() {
//                                                        public void onClick(DialogInterface dialog, int id) {
//                                                            dialog.cancel();
//                                                        }
//                                                    });
//                                            AlertDialog alert = alt_bld.create();
//                                            alert.show();
                                            //"NCT-Vi"
                                            Log.e("AdpaterBusSeat", "Already resisted number" );
                                            AlertDialog.Builder alt_bld = new AlertDialog.Builder(activity);
                                            alt_bld.setTitle("Tên xe hoặc biển số xe này đã được đăng ký");
                                            alt_bld.setMessage("Vui lòng đăng ký với tên xe hoặc biển số xe khác.").setCancelable(
                                                    false).setPositiveButton("OK",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    });
                                            AlertDialog alert = alt_bld.create();
                                            alert.show();

                                        }

                                    }
                                    else
                                    {
                                        Log.e(TAG, "canceled");
                                    }

                                }
                            });
                        }
                        else
                        {
                            final DeleteBusInfoDialog deleteBusInfoDialog = new DeleteBusInfoDialog(activity, item.getBus_name(), item.getBus_code());
                            deleteBusInfoDialog.show();
                            deleteBusInfoDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                    if(deleteBusInfoDialog.getResult() == true)
                                    {
                                        //SEAT 정보 수정
                                        List<SeatInfo> mSeats = _dbHelper.mSeat.QuerybyCode(item.getBus_name(), item.getBus_code());
                                        for(int i = 0; i< mSeats.size(); ++i)
                                        {
                                            _dbHelper.mSeat.Delete(mSeats.get(i));
                                        }

                                        List<AlarmInfo> mAlarms = _dbHelper.mAlarm.QueryByCode(item.getBus_name(), item.getBus_code());
                                        for(int i = 0 ; i< mAlarms.size(); ++i)
                                        {
                                            cancelAlarm(mAlarms.get(i).getBusName(), mAlarms.get(i).getBusCode());
                                            _dbHelper.mAlarm.Delete(mAlarms.get(i));
                                        }

                                        //BUS 정보 수정
                                        List<BusInfo> mBus = _dbHelper.mBus.QueryByCode(item.getBus_name(), item.getBus_code());
                                        for(int i = 0; i< mBus.size(); ++i)
                                        {
                                            _dbHelper.mBus.Delete(mBus.get(i));
                                        }
                                        mainActivity.refreshBusList();
                                    }
                                    else
                                    {
                                        Log.e(TAG, "canceled");
                                    }
                                }
                            });
                        }


                    }
                });
                mPopupWindow.show();

            }
        });


        //viewHolder.busInfo.setBackgroundColor();

        return row;
    }

    private void cancelAlarm(String busName, String busCode)
    {
        List<BusInfo> busInfo = _dbHelper.mBus.QueryByCode(busName, busCode);

        AlarmManager mAlarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent serviceStartIntent = new Intent(SERVICE_START_ALARM);
        serviceStartIntent.putExtra("BUS_ID", busInfo.get(0).getId());
        serviceStartIntent.putExtra("BUS_NAME", busInfo.get(0).getBusName());
        serviceStartIntent.putExtra("BUS_CODE", busInfo.get(0).getBusCode());
        serviceStartIntent.putExtra("BUS_SEAT", busInfo.get(0).getBusSeat());

        List<PendingIntent> mDaysPendingIntent = new ArrayList<>();
        mDaysPendingIntent.clear();

        PendingIntent mDatePendingIntent = PendingIntent.getBroadcast(context, busInfo.get(0).getId(), serviceStartIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        for(int i=0; i < 7; ++i) {
            mDaysPendingIntent.add(i, PendingIntent.getBroadcast(context, (busInfo.get(0).getId() * 10 + i), serviceStartIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        }

        if (mDatePendingIntent != null) {
            mAlarmManager.cancel(mDatePendingIntent);
            mDatePendingIntent.cancel();
        }

        if(mDaysPendingIntent != null) {
            for(int i=0; i < 7; ++i) {
                mAlarmManager.cancel(mDaysPendingIntent.get(i));
                mDaysPendingIntent.get(i).cancel();
            }
        }
    }


}
