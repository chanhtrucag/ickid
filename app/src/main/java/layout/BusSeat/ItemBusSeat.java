package layout.BusSeat;

/**
 * Created by DELL3 on 2018-11-01.
 */

public abstract  class ItemBusSeat {

    public static final int TYPE_CENTER = 0;
    public static final int TYPE_EDGE = 1;
    public static final int TYPE_EMPTY = 2;

    private String label;
    private int[] position = new int[2];
    private String serial_number = "";

    public ItemBusSeat(String label) {
        this.label = label;
    }


    public String getLabel() {
        return label;
    }

    public void setSeatInfo(String serial_number)
    {
        position[0] = Integer.parseInt(getLabel());
        position[1] = Integer.parseInt(getLabel());
        this.serial_number = serial_number;
    }


    public void setSeatInfo(int[] position, String serial_number)
    {
        this.position = position;
        this.serial_number = serial_number;
    }

    public String getSeatSerialNumber()
    {
        return serial_number;
    }

    public int[] getSeatPosition()
    {
        return position;
    }


    abstract public int getType();

}


