package layout.BusSeat;

/**
 * Created by DELL3 on 2018-11-01.
 */

public class CenterItem extends ItemBusSeat{

    public CenterItem(String label) {
        super(label);
    }
    @Override
    public int getType() {
        return TYPE_CENTER;
    }
}
