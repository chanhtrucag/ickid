package layout.BusSeat;

public class EmptyItem extends ItemBusSeat {

    public EmptyItem(String label) {
        super(label);
    }


    @Override
    public int getType() {
        return TYPE_EMPTY;
    }

}
