package layout.BusSeat;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL3 on 2018-11-01.
 */

public abstract class SelectableAdapter <VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH>{

    private static final String TAG = SelectableAdapter.class.getSimpleName();

    private SparseBooleanArray selectedItems, selectedDeleteItems;

    public SelectableAdapter() {
        selectedItems = new SparseBooleanArray ();
        selectedDeleteItems = new SparseBooleanArray();
    }

    public boolean isSelected(int position) {
        return getSelectedItems().contains(position);
    }

    public boolean isSelectedForDel(int position) {
        return getSelectedDeleteItems().contains(position);
    }

    public void toggleSelection(int position) {

        if (selectedItems.get(position, false)) {
            selectedItems.delete(position);
        } else {
            selectedItems.put(position, true);


        }
        notifyItemChanged(position);
    }

    public void selectToDelete(int position)
    {
        if (selectedDeleteItems.get(position, false)) {
            selectedDeleteItems.delete(position);
        } else {
            selectedDeleteItems.put(position, true);
        }
        notifyItemChanged(position);
    }

    public void clearSelection() {
        List<Integer> selection = getSelectedItems();
        selectedItems.clear();
        for (Integer i : selection) {
            notifyItemChanged(i);
        }
    }

    public void clearDelSelection() {
        List<Integer> selection = getSelectedDeleteItems();
        selectedDeleteItems.clear();
        for (Integer i : selection) {
            notifyItemChanged(i);
        }
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public int getSelectedIDeltemCount() {
        return selectedDeleteItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); ++i) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public List<Integer> getSelectedDeleteItems() {
        List<Integer> items = new ArrayList<>(selectedDeleteItems.size());
        for (int i = 0; i < selectedDeleteItems.size(); ++i) {
            items.add(selectedDeleteItems.keyAt(i));
        }
        return items;
    }

}
