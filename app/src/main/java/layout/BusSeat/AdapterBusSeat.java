package layout.BusSeat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import kidsoft.buscare.BusInfo.SeatInfoActivity;
import kidsoft.buscare.BusInfoCamera.BarcodeScanActivity;
import kidsoft.buscare.R;

import static android.app.Activity.RESULT_OK;
import static kidsoft.buscare.BusInfo.SeatInfoActivity.REQUEST_CODE_SET_SEAT_INFO;

/**
 * Created by DELL3 on 2018-11-01.
 */

public class AdapterBusSeat extends SelectableAdapter<RecyclerView.ViewHolder>{

    private OnSeatSelected mOnSeatSelected;
    private int SelectedItem;

    private static class CenterViewHolder extends RecyclerView.ViewHolder {

        ImageView imgSeat;
        private final ImageView imgSeatSelected, imgSeatChecked;

        public CenterViewHolder(View itemView) {
            super(itemView);
            imgSeat = (ImageView) itemView.findViewById(R.id.img_seat);
            imgSeatSelected = (ImageView) itemView.findViewById(R.id.img_seat_selected);
            imgSeatChecked = itemView.findViewById(R.id.img_seat_checked);
        }

    }

    private static class EdgeViewHolder extends RecyclerView.ViewHolder {

        ImageView imgSeat;
        private final ImageView imgSeatSelected;


        public EdgeViewHolder(View itemView) {
            super(itemView);
            imgSeat = (ImageView) itemView.findViewById(R.id.img_seat);
            imgSeatSelected = (ImageView) itemView.findViewById(R.id.img_seat_selected);

        }

    }

    private static class EmptyViewHolder extends RecyclerView.ViewHolder {

        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

    }


    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<ItemBusSeat> mItems;
    private SeatInfoActivity activity;


    public AdapterBusSeat(Context context, List<ItemBusSeat> items, SeatInfoActivity activity) {
        mOnSeatSelected = (OnSeatSelected) context;
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mItems = items;
        this.activity = activity;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ItemBusSeat.TYPE_CENTER) {
            View itemView = mLayoutInflater.inflate(R.layout.layout_busseat_item, parent, false);
            return new CenterViewHolder(itemView);
        }
        else if (viewType == ItemBusSeat.TYPE_EDGE) {
            View itemView = mLayoutInflater.inflate(R.layout.layout_busseat_item, parent, false);
            return new EdgeViewHolder(itemView);
        } else
        {
            View itemView = new View(mContext);
            return new EmptyViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        int type = mItems.get(position).getType();

        if (type == ItemBusSeat.TYPE_CENTER) {
            //final CenterItem item = (CenterItem) mItems.get(position);
            CenterViewHolder holder = (CenterViewHolder) viewHolder;
            holder.imgSeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    activity.btDelAll.setTextColor(Color.parseColor("#6b6b6b"));
                    SelectedItem = position;

                    if( mItems.get(SelectedItem).getSeatSerialNumber().isEmpty() == false)
                    {
                        //Log.e("AdpaterBusSeat", "이미 등록된 좌석입니다.");

                        if(activity.getDoDelete() == false)
                        {

//                            AlertDialog.Builder alt_bld = new AlertDialog.Builder((mContext));
//                            alt_bld.setTitle("좌석 정보 수정");
//                            alt_bld.setMessage("이미 정보가 있는 좌석을 선택하셨습니다. 좌석정보를 수정하시겠습니까?").setCancelable(
//                                    false).setPositiveButton("확인",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//
//                                            Intent intent = new Intent(mContext, BarcodeScanActivity.class);
//                                            intent.putExtra("seat_position", SelectedItem);
//                                            Log.e("AdpaterBusSeat", "set position " + SelectedItem  + " " + position);
//                                            ((Activity) mContext).startActivityForResult(intent, REQUEST_CODE_SET_SEAT_INFO);
//
//                                        }
//                                    }).setNegativeButton("취소",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            dialog.cancel();
//                                        }
//                                    });
//                            AlertDialog alert = alt_bld.create();
//                            alert.show();
                            //"NCT-Vi"
                            AlertDialog.Builder alt_bld = new AlertDialog.Builder((mContext));
                            alt_bld.setTitle("Sửa thông tin ghế");
                            alt_bld.setMessage("Ghế được chọn đã có thông tin. Xác nhận chỉnh sửa thông tin ghế?").setCancelable(
                                    false).setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            Intent intent = new Intent(mContext, BarcodeScanActivity.class);
                                            intent.putExtra("seat_position", SelectedItem);
                                            Log.e("AdpaterBusSeat", "set position " + SelectedItem  + " " + position);
                                            ((Activity) mContext).startActivityForResult(intent, REQUEST_CODE_SET_SEAT_INFO);

                                        }
                                    }).setNegativeButton("Hủy",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = alt_bld.create();
                            alert.show();
                        }
                        else
                        {
                            selectToDelete(SelectedItem);
                        }
                    }
                    else
                    {
                        Intent intent = new Intent(mContext, BarcodeScanActivity.class);
                        intent.putExtra("seat_position", SelectedItem);
                        Log.e("AdpaterBusSeat", "set position " + SelectedItem  + " " + position);
                        ((Activity) mContext).startActivityForResult(intent, REQUEST_CODE_SET_SEAT_INFO);
                    }



                    /*
                    Intent i = new Intent(mContext, BarcodeScanActivity.class);
                    mContext.startActivity(i);
                    */
                    /*
                    toggleSelection(SelectedItem);
                    mOnSeatSelected.onSeatSelected(getSelectedItemCount());
                    */
                }
            });

            holder.imgSeatSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.btDelAll.setTextColor(Color.parseColor("#6b6b6b"));
                    SelectedItem = position;

                    if( mItems.get(SelectedItem).getSeatSerialNumber().isEmpty() == false)
                    {
                        //Log.e("AdpaterBusSeat", "이미 등록된 좌석입니다.");

                        if(activity.getDoDelete() == false)
                        {

//                            AlertDialog.Builder alt_bld = new AlertDialog.Builder((mContext));
//                            alt_bld.setTitle("좌석 정보 수정");
//                            alt_bld.setMessage("이미 정보가 있는 좌석을 선택하셨습니다. 좌석정보를 수정하시겠습니까?").setCancelable(
//                                    false).setPositiveButton("확인",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//
//                                            Intent intent = new Intent(mContext, BarcodeScanActivity.class);
//                                            intent.putExtra("seat_position", SelectedItem);
//                                            Log.e("AdpaterBusSeat", "set position " + SelectedItem  + " " + position);
//                                            ((Activity) mContext).startActivityForResult(intent, REQUEST_CODE_SET_SEAT_INFO);
//
//                                        }
//                                    }).setNegativeButton("취소",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            dialog.cancel();
//                                        }
//                                    });
//                            AlertDialog alert = alt_bld.create();
//                            alert.show();
                            //"NCT-Vi"
                            AlertDialog.Builder alt_bld = new AlertDialog.Builder((mContext));
                            alt_bld.setTitle("Sửa thông tin ghế");
                            alt_bld.setMessage("Ghế được chọn đã có thông tin. Xác nhận chỉnh sửa thông tin ghế?").setCancelable(
                                    false).setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            Intent intent = new Intent(mContext, BarcodeScanActivity.class);
                                            intent.putExtra("seat_position", SelectedItem);
                                            Log.e("AdpaterBusSeat", "set position " + SelectedItem  + " " + position);
                                            ((Activity) mContext).startActivityForResult(intent, REQUEST_CODE_SET_SEAT_INFO);

                                        }
                                    }).setNegativeButton("Hủy",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = alt_bld.create();
                            alert.show();
                        }
                        else
                        {
                            selectToDelete(SelectedItem);
                        }
                    }

                }
            });

            holder.imgSeatChecked.setVisibility(isSelectedForDel(position) ? View.VISIBLE : View.INVISIBLE);
            holder.imgSeatSelected.setVisibility(isSelected(position) ? View.VISIBLE : View.INVISIBLE);
            holder.imgSeat.setVisibility(isSelected(position) ? View.INVISIBLE : View.VISIBLE);
        }
        /*
        else if (type == ItemBusSeat.TYPE_EDGE)
        {
            EdgeViewHolder holder = (EdgeViewHolder) viewHolder;
            holder.imgSeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    toggleSelection(position);
                    mOnSeatSelected.onSeatSelected(getSelectedItemCount());
                }
            });

            holder.imgSeatSelected.setVisibility(isSelected(position) ? View.VISIBLE : View.INVISIBLE);
        }
        */

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public ItemBusSeat getItemInfo(int position) {
        return mItems.get(position);
    }

     @Override
    public int getItemViewType(int position) {
        return mItems.get(position).getType();
    }

    public  void onActivityResult(int requestCode, int resultCode, Intent data) {

         if((resultCode == RESULT_OK) && (requestCode == REQUEST_CODE_SET_SEAT_INFO))
         {
             if(data.getStringExtra("result_seat").toString().contains("CANCEL_REGI") == false)
             {
                 Log.e("AdpaterBusSeat", "result position " + SelectedItem );

                 boolean mSearch = false;
                 for(int i = 0; i < mItems.size() ; ++i)
                 {
                     if(mItems.get(i).getSeatSerialNumber().equals(data.getStringExtra("result_seat").toString()) == true)
                     {
                         mSearch = true;
                         break;
                     }
                 }

                 if(mSearch == false)
                 {
                     if(mItems.get(SelectedItem).getSeatSerialNumber().isEmpty() == true)
                     {
                         toggleSelection(SelectedItem);
                         mOnSeatSelected.onSeatSelected(getSelectedItemCount());
                     }
                     mItems.get(SelectedItem).setSeatInfo(data.getStringExtra("result_seat").toString());
                 }
                 else {
//                     Log.e("AdpaterBusSeat", "Already resisted number" );
//                     AlertDialog.Builder alt_bld = new AlertDialog.Builder(mContext);
//                     alt_bld.setTitle("이미 등록된 제품코드입니다");
//                     alt_bld.setMessage("제품코드가 등록된 좌석을 삭제해야합니다.").setCancelable(
//                             false).setPositiveButton("확인",
//                             new DialogInterface.OnClickListener() {
//                                 public void onClick(DialogInterface dialog, int id) {
//                                     dialog.cancel();
//                                 }
//                             });
//                     AlertDialog alert = alt_bld.create();
//                     alert.show();
                     //"NCT-Vi"

                     Log.e("AdpaterBusSeat", "Already resisted number" );
                     AlertDialog.Builder alt_bld = new AlertDialog.Builder(mContext);
                     alt_bld.setTitle("Mã nệm ngồi này đã được sử dụng");
                     alt_bld.setMessage("Vui lòng xóa ghế đã được đăng ký với mã nệm ngồi này.").setCancelable(
                             false).setPositiveButton("OK",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {
                                     dialog.cancel();
                                 }
                             });
                     AlertDialog alert = alt_bld.create();
                     alert.show();
                 }
             }
             else {
                 Log.e("AdpaterBusSeat", "Canceled" );
             }
         }
    }

}
