package layout.BusSeatService;

import kidsoft.buscare.Database.models.SeatInfo;

/**
 * Created by DELL3 on 2018-11-07.
 */

public class bsCenterSeat extends bsBusSeatItem {

    public bsCenterSeat(String label, SeatInfo seatInfo) {
        super(label, seatInfo);
    }

    public bsCenterSeat(String label) {
        super(label);
    }
    @Override
    public int getType() {
        return TYPE_CENTER;
    }
}
