package layout.BusSeatService;

import kidsoft.buscare.Database.models.SeatInfo;

/**
 * Created by DELL3 on 2018-11-07.
 */

public abstract class bsBusSeatItem {

    public static final int TYPE_CENTER = 0;
    public static final int TYPE_EDGE = 1;
    public static final int TYPE_EMPTY = 2;

    public static final String STATUS_NONE = "status_none";
    public static final String STATUS_EMPTY = "status_empty";
    public static final String STATUS_OCCUPIED = "status_occupied";
    public static final String STATUS_LOW_BATTERY = "status_low_battery";
    public static final String STATUS_ERROR = "status_cannot_connect";
    public static final String STATUS_REMAIN_CHILD = "status_remain_child";

    private String label;
    private int position;
    private String seat_type = "";
    private String bus_code = "";
    private String serial_number = "";
    private String seat_status = bsBusSeatItem.STATUS_EMPTY;
    private String seat_battery = "-1";

    private boolean seat_checked = false;
    private boolean seat_add = false;
    private boolean caution_status = false;
    private boolean caution_error = false;
    private boolean seat_sync = false;

    private SeatInfo seatInfo = null;

    public bsBusSeatItem(String label, SeatInfo seatInfo) {
        this.label = label;
        this.seatInfo = seatInfo;
        this.bus_code = seatInfo.getBusCode();
        this.serial_number = seatInfo.getSeatNumber();
        this.seat_status = seatInfo.getSeatStatus();
        this.position = Integer.parseInt(label);
        this.seat_battery = seatInfo.getSeatBattery();
    }

    public bsBusSeatItem(String label) {

        this.label = label;
        this.seatInfo = new SeatInfo();
        this.serial_number = "";
    }


    public String getLabel() {
        return label;
    }

    public void setSeatInfo(String serial_number)
    {
        position = Integer.parseInt(getLabel());
        this.serial_number = serial_number;
    }


    public void setSeatInfo(int position, String serial_number)
    {
        this.position = position;
        this.serial_number = serial_number;
    }

    public String getSeatSerialNumber()
    {
        return serial_number;
    }

    public int getSeatPosition()
    {
        return position;
    }


    public void setStatus(String status) {
        this.seat_status = status;
    }

    public void setCautionStatus(boolean status) {
        this.caution_status = status;
    }

    public boolean getCautionStatus() {
        return caution_status ;
    }

    public void setSync(boolean sync) {
        this.seat_sync =  sync;
    }

    public boolean getSync() {
        return seat_sync ;
    }


    public void setErrorStatus(boolean status){
        this.caution_error = status;
    }

    public boolean getErrorStatus() {
        return caution_error;
    }

    public String getStatus() {
        return this.seat_status;
    }

    public void setBatteryLevel(String b_level) {
        this.seat_battery = b_level;
    }

    public String getBetteryLevel()
    {
        return seat_battery;
    }


    public boolean getSeatChecked()
    {
        return seat_checked;
    }

    public boolean getSeatAdd()
    {
        return seat_add;
    }

    public void setBusCode(String busCode)
    {
        this.bus_code = busCode;
    }

    public String getBusCode()
    {
        return this.bus_code;
    }

    public void setSeatChecked(boolean seat_checked)
    {
        this.seat_checked = seat_checked;
    }

    public void setSeatAdd(boolean seat_add)
    {
        this.seat_add = seat_add;
    }


    abstract public int getType();
}
