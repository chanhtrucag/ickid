package layout.BusSeatService;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kidsoft.buscare.BusSeatService.BusSeatServiceActivity;
import kidsoft.buscare.Database.models.SeatInfo;

/**
 * Created by DELL3 on 2018-11-08.
 */

public abstract class bsSelectableAdapter <VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH>{

    private static final String TAG = bsSelectableAdapter.class.getSimpleName();

    private SparseBooleanArray selectedItems;

    private int preSelectedItem = -1;

    public bsSelectableAdapter() {
        selectedItems = new SparseBooleanArray ();
    }

    public boolean isSelected(int position) {
        return getSelectedItems().contains(position);
    }


    public void toggleForDelete(int position)
    {

    }



    public void toggleSelection(int position) {

        //Log.e(TAG, "toggled position : " + position );

        if (selectedItems.get(position, false)) {
            selectedItems.delete(position);
        } else {
            selectedItems.put(position, true);
        }
        notifyItemChanged(position);
    }

    public void toggleSelection(int position, BusSeatServiceActivity bssAct, bsBusSeatItem mSeat) {

        //Log.e(TAG, "Seat Info : " +  "preSelectedItem " + preSelectedItem + "/ position " + position );

        if(preSelectedItem != -1)
        {
            if(position != preSelectedItem)
            {
                if (selectedItems.get(preSelectedItem, false))
                {
                    selectedItems.delete(preSelectedItem);
                    bssAct.HideSeatInfo();
                }

                notifyItemChanged(preSelectedItem);
            }
        }

        if (selectedItems.get(position, false))
        {
            selectedItems.delete(position);
            bssAct.HideSeatInfo();

        } else {
            selectedItems.put(position, true);
            bssAct.ShowSeatInfo(mSeat);
        }

        preSelectedItem = position;

        notifyItemChanged(position);
    }

    public void clearSelection() {
        List<Integer> selection = getSelectedItems();
        selectedItems.clear();
        for (Integer i : selection) {
            notifyItemChanged(i);
        }
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); ++i) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }


}
