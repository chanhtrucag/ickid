package layout.BusSeatService;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import kidsoft.buscare.BusInfoCamera.BarcodeScanActivity;
import kidsoft.buscare.BusSeatService.BusSeatServiceActivity;
import kidsoft.buscare.Database.models.SeatInfo;
import kidsoft.buscare.R;
import layout.BusSeat.ItemBusSeat;

import static android.app.Activity.RESULT_OK;

/**
 * Created by DELL3 on 2018-11-08.
 */

public class bsAdapterBusSeat extends bsSelectableAdapter<RecyclerView.ViewHolder>{

    private static final String TAG = "bsAdapterBusSeat";

    public static final int REQUEST_CODE_SEAT_INFO_MODI_ADD = 5555;

    private bsOnSeatSelected mOnSeatSelected;
    private int SelectedItem, preSelectedItem = -1;
    private boolean selected = false;

    private static class CenterViewHolder extends RecyclerView.ViewHolder {

        ImageView imgSeat;
        private final ImageView imgSeatSelected, imgSeatChecked, imgSeatAdd, imgSeatAlert;
        private RelativeLayout layoutBackground;

        private TextView tvBatteryLv;

        public CenterViewHolder(View itemView) {
            super(itemView);
            imgSeat = (ImageView) itemView.findViewById(R.id.bs_img_seat);
            imgSeatSelected = (ImageView) itemView.findViewById(R.id.bs_img_seat_selected);
            imgSeatChecked = itemView.findViewById(R.id.bs_img_seat_checked);
            imgSeatAdd = itemView.findViewById(R.id.bs_img_seat_add);
            tvBatteryLv = (TextView) itemView.findViewById(R.id.bs_battery_level);
            layoutBackground = itemView.findViewById(R.id.bs_background);
            imgSeatAlert = itemView.findViewById(R.id.bs_seat_alert);
        }

    }

    private static class EdgeViewHolder extends RecyclerView.ViewHolder {

        ImageView imgSeat;
        private final ImageView imgSeatSelected;

        public EdgeViewHolder(View itemView) {
            super(itemView);
            imgSeat = (ImageView) itemView.findViewById(R.id.bs_img_seat);
            imgSeatSelected = (ImageView) itemView.findViewById(R.id.bs_img_seat_selected);
        }

    }

    private static class EmptyViewHolder extends RecyclerView.ViewHolder {

        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

    }

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<bsBusSeatItem> mItems;

    private Activity activity;
    private  BusSeatServiceActivity bssActivity;

    public bsAdapterBusSeat(Context context, Activity activity, BusSeatServiceActivity mAct, List<bsBusSeatItem> items) {
        mOnSeatSelected = (bsOnSeatSelected) context;
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mItems = items;

        this.activity = activity;
        this.bssActivity = mAct;
    }

    public List<bsBusSeatItem> getmItems()
    {
        return this.mItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == bsBusSeatItem.TYPE_CENTER) {
            View itemView = mLayoutInflater.inflate(R.layout.layout_bs_service_item, parent, false);
            return new CenterViewHolder(itemView);
        }
        else if (viewType == bsBusSeatItem.TYPE_EDGE) {
            View itemView = mLayoutInflater.inflate(R.layout.layout_bs_service_item, parent, false);
            return new EdgeViewHolder(itemView);
        } else
        {
            View itemView = new View(mContext);
            return new EmptyViewHolder(itemView);
        }
    }

    public int getSelectedItem()
    {
        return SelectedItem;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        int type = mItems.get(position).getType();

        if (type == ItemBusSeat.TYPE_CENTER) {

            final CenterViewHolder holder = (CenterViewHolder) viewHolder;
            holder.imgSeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SelectedItem = position;

                    if( mItems.get(position).getStatus().equals(bsBusSeatItem.STATUS_NONE) == false)
                    {
                        //Log.e(TAG, "Seat Info : " +  mItems.get(position).getSeatPosition() + " " + mItems.get(position).getSeatSerialNumber() );

                        if(bssActivity.getDoDelete() == true)
                        {
                            //Log.e(TAG, "Do delete" );
                            toggleSelection(position);
                        }
                        else
                        {
                            //Log.e(TAG, "Do Selected" );
                            if(bssActivity.getDoModify()== false)
                            {
                                toggleSelection(position, bssActivity, mItems.get(position));
                            }
                        }

                        /*
                        if(preSelectedItem != SelectedItem)
                        {
                            toggleSelection(preSelectedItem, bssActivity);
                            toggleSelection(SelectedItem, bssActivity);
                            preSelectedItem = SelectedItem;
                        }
                        else
                        {
                            toggleSelection(position, bssActivity);
                        }
                        */


                        //holder.imgSeat.setImageResource(R.drawable.seat_touch);

                        //mOnSeatSelected.onSeatSelected(getSelectedItemCount());
                    }
                    else
                    {
                        if(bssActivity.getDoModify() == true) {

                            Intent intent = new Intent(mContext, BarcodeScanActivity.class);
                            intent.putExtra("seat_position", SelectedItem);
                            Log.e(TAG, "set position " + SelectedItem  + " " + position);
                            ((Activity) mContext).startActivityForResult(intent, REQUEST_CODE_SEAT_INFO_MODI_ADD);

                        }
                    }
                }
            });

            if(mItems.get(position).getSeatSerialNumber().isEmpty() == false)
            {

                Log.e(TAG, "seat Status " + mItems.get(position).getSeatSerialNumber() + " / " + position + " / "  + mItems.get(position).getStatus() + " / " + mItems.get(position).getBetteryLevel());

                if(Integer.parseInt(mItems.get(position).getBetteryLevel()) > 2100)
                {
                    holder.tvBatteryLv.setText("");
                }
                else if(Integer.parseInt(mItems.get(position).getBetteryLevel()) <= 2100 && Integer.parseInt(mItems.get(position).getBetteryLevel()) >= 1900)
                {
                    holder.tvBatteryLv.setText("");
                }
                else if(Integer.parseInt(mItems.get(position).getBetteryLevel()) < 1900 && Integer.parseInt(mItems.get(position).getBetteryLevel()) > -1)
                {
//                    holder.tvBatteryLv.setText("교체");
                    //"NCT-Vi"
                    holder.tvBatteryLv.setText("Thay thế");
                }

                //--- holder.tvBatteryLv.setText(mItems.get(position).getBetteryLevel());

                if(Integer.parseInt(mItems.get(position).getBetteryLevel()) < 1900 && Integer.parseInt(mItems.get(position).getBetteryLevel()) > -1)
                {
                    holder.imgSeat.setImageResource(R.drawable.seat_red);
                }
                else
                {
                    if(mItems.get(position).getStatus().equals(bsBusSeatItem.STATUS_EMPTY))
                    {
                        holder.imgSeat.setImageResource(R.drawable.seat_white);
                    }
                    else if(mItems.get(position).getStatus().equals(bsBusSeatItem.STATUS_OCCUPIED))
                    {
                        holder.imgSeat.setImageResource(R.drawable.seat_on);
                    }
                    else if(mItems.get(position).getStatus().equals(bsBusSeatItem.STATUS_LOW_BATTERY))
                    {
                        holder.imgSeat.setImageResource(R.drawable.seat_red);
                    }
                    else
                    {
                        holder.imgSeat.setImageResource(R.drawable.seat_white);
                    }
                }
                //mItems.get(position).setStatus(bsBusSeatItem.STATUS_EMPTY);
                //holder.imgSeat.setImageResource(R.drawable.seat_white);

                //Log.e(TAG, "seat battery " + position + " / "  + mItems.get(position).getBetteryLevel());

                if(mItems.get(position).getBetteryLevel().equals("-1"))
                {
//                    holder.tvBatteryLv.setText("대기");
                    //"NCT-Vi"
                    holder.tvBatteryLv.setText("Chờ");

                }
                else if(mItems.get(position).getBetteryLevel().equals("-2"))
                {
//                    holder.tvBatteryLv.setText("확인");
                    //"NCT-Vi"
                    holder.tvBatteryLv.setText("OK");
                }
                /*
                else{
                    holder.tvBatteryLv.setText(mItems.get(position).getBetteryLevel());
                }
                */

                if(bssActivity.getDoDelete() == true)
                {
                    holder.tvBatteryLv.setVisibility(View.INVISIBLE);
                } else
                {
                    holder.tvBatteryLv.setVisibility(View.VISIBLE);
                }
            }
            else
            {
                mItems.get(position).setStatus(bsBusSeatItem.STATUS_NONE);

                if(bssActivity.getDoModify() == true)
                {
                    holder.imgSeat.setImageResource(R.drawable.seat_add);
                }
                else
                {
                    holder.imgSeat.setImageResource(R.drawable.seat_off);
                }

                holder.tvBatteryLv.setVisibility(View.INVISIBLE);
            }

            if(mItems.get(position).getCautionStatus() == true)
            {
                holder.imgSeatAlert.setVisibility(View.VISIBLE);
            }
            else
            {
                if(mItems.get(position).getStatus().equals(bsBusSeatItem.STATUS_LOW_BATTERY))
                {
                    holder.imgSeatAlert.setVisibility(View.VISIBLE);
                }
                else
                {
                    holder.imgSeatAlert.setVisibility(View.INVISIBLE);
                }

                if(mItems.get(position).getStatus().equals(bsBusSeatItem.STATUS_ERROR))
                {
                    holder.imgSeatAlert.setVisibility(View.VISIBLE);
                }
                else
                {
                    holder.imgSeatAlert.setVisibility(View.INVISIBLE);
                }

                // --- alert_msg
                //holder.imgSeatAlert.setVisibility(View.INVISIBLE);
            }




            if(bssActivity.getDoDelete() == true)
            {
                holder.imgSeatChecked.setVisibility(isSelected(position)? View.VISIBLE : View.INVISIBLE);
            }
            else
            {
                holder.imgSeatSelected.setVisibility(isSelected(position) ? View.VISIBLE : View.INVISIBLE);
            }
        }
    }

    public void changeSeatCautionStatus(SeatInfo seatInfo)
    {
        int position = Integer.parseInt(seatInfo.getSeatPosition());
        mItems.get(position).setCautionStatus(seatInfo.getCautionStatus());
        mItems.get(position).setStatus(seatInfo.getSeatStatus());
        mItems.get(position).setErrorStatus(seatInfo.getErrorStatus());
        mItems.get(position).setSync(seatInfo.getSync());
        notifyItemChanged(position);
    }

    public void changeSeatCautionStatus(boolean status, int position) {

        mItems.get(position).setCautionStatus(status);
        notifyItemChanged(position);
    }

    public void changeSeatBatteryLv(String btLv, int position)
    {
        mItems.get(position).setBatteryLevel(btLv);
        if(Integer.parseInt(btLv) < 1900) // battery 10 -> 3000
        {
            mItems.get(position).setStatus(bsBusSeatItem.STATUS_LOW_BATTERY);

            notifyItemChanged(position);
        }
    }

    public void changeSeatStatus(String status, int position)
    {
        if(status.equals(bsBusSeatItem.STATUS_EMPTY))
        {
            mItems.get(position).setStatus(bsBusSeatItem.STATUS_EMPTY);
        }
        else if(status.equals(bsBusSeatItem.STATUS_OCCUPIED))
        {
            mItems.get(position).setStatus(bsBusSeatItem.STATUS_OCCUPIED);
        }
        else if(status.equals(bsBusSeatItem.STATUS_LOW_BATTERY))
        {
            mItems.get(position).setStatus(bsBusSeatItem.STATUS_LOW_BATTERY);
        }
        else if(status.equals(bsBusSeatItem.STATUS_NONE))
        {
            mItems.get(position).setStatus(bsBusSeatItem.STATUS_NONE);
        }

        notifyItemChanged(position);
    }



    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public bsBusSeatItem getItemInfo(int position) {
        return mItems.get(position);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if((resultCode == RESULT_OK) && (requestCode == REQUEST_CODE_SEAT_INFO_MODI_ADD))
        {
            if(data.getStringExtra("result_seat").toString().contains("CANCEL_REGI") == false)
            {
                Log.e(TAG, "result position : " + SelectedItem + " new code : " + data.getStringExtra("result_seat") );

                //mItems.get(SelectedItem).setSeatInfo(data.getStringExtra("result_seat").toString());
            }
            else {
                Log.e(TAG, "Canceled" );
            }
        }
    }
}
