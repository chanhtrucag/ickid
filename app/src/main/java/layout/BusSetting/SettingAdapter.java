package layout.BusSetting;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

import kidsoft.buscare.BusList.LicenceActivity;
import kidsoft.buscare.BusList.SettingActivity;
import kidsoft.buscare.R;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by DELL3 on 2019-01-29.
 */

public class SettingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final String TAG = "SettingAdapter";

    public static final int TYPE_DISPLAY = 0;
    public static final int TYPE_OPENSOURCE = 1;
    public static final int TYPE_CONTACT = 2;

    private Context mContext;
    private Activity mActivity;
    private LayoutInflater mLayoutInflater;
    private ArrayList<SettingModel> mItems = new ArrayList<SettingModel>();
    private SharedPreferences settings;
    public boolean flagAlways;

    public static class DiplayViewHolder extends RecyclerView.ViewHolder {

        private Switch swDisplayAlways;
        private ImageView imgOptInfo;
        public DiplayViewHolder(View itemView) {
            super(itemView);
            swDisplayAlways = itemView.findViewById(R.id.sdo_switch);
            imgOptInfo = itemView.findViewById(R.id.sdo_info);
        }
    }

    public static class OpenSourceViewHolder extends  RecyclerView.ViewHolder {

        private TextView txOpenSourceInfo;

        public OpenSourceViewHolder(View itemView) {
            super(itemView);
            txOpenSourceInfo = itemView.findViewById(R.id.so_text);
        }
    }

    public static class ContactViewHolder extends  RecyclerView.ViewHolder{

        private ImageView imgArrow;
        private boolean mClicked = false;
        private RelativeLayout mContents;

        public ContactViewHolder(View itemView) {
            super(itemView);
            imgArrow = itemView.findViewById(R.id.sc_arrow);
            mContents = itemView.findViewById(R.id.sc_contents);

            if(mClicked == false)
            {
                mContents.setVisibility(View.GONE);
            }
            else
            {
                mContents.setVisibility(View.VISIBLE);
            }
        }

        public void setClicked(boolean tag) {
            mClicked = tag;
        }

        public boolean getClicked() {
            return  mClicked;
        }
    }

    public SettingAdapter (Context context, Activity activity, ArrayList<SettingModel> items) {
        Log.e(TAG, "SettingAdapter");

        this.mContext = context;
        this. mActivity = activity;
        this.mLayoutInflater = LayoutInflater.from(context);
        this.settings = mContext.getSharedPreferences("settings", MODE_PRIVATE);
        this.mItems = items;
        flagAlways = settings.getBoolean("DisplayAlwaysOn", false);

    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_DISPLAY:
                view = mLayoutInflater.inflate(R.layout.layout_setting_display_on, parent, false);
                return new DiplayViewHolder(view);

            case TYPE_OPENSOURCE:
                view = mLayoutInflater.inflate(R.layout.layout_setting_opensource_info, parent, false);
                return new OpenSourceViewHolder(view);

            case TYPE_CONTACT:
                view = mLayoutInflater.inflate(R.layout.layout_setting_contact, parent, false);
                return new ContactViewHolder(view);
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).getType();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder convertView, int position) {
        int type = getItemViewType(position);

        Log.e(TAG, "onBindViewHolder " + type);

        if(type == TYPE_DISPLAY)
        {
            DiplayViewHolder mDiplayViewHolder = (DiplayViewHolder)convertView;
            mDiplayViewHolder.swDisplayAlways.setChecked(flagAlways);
            mDiplayViewHolder.swDisplayAlways.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    SharedPreferences.Editor prefEditor = settings.edit();
                    prefEditor.putBoolean("DisplayAlwaysOn", isChecked);
                    prefEditor.apply();
                }
            });

            mDiplayViewHolder.imgOptInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(mActivity);
//                    alt_bld.setTitle("화면 항상 켜짐");
//                    alt_bld.setMessage("화면 항상 켜짐 기능을 사용하시면 더 정확하게 자리이탈을 확인할 수 있습니다. 실행시에는 배터리 소모가 증가합니다.").setCancelable(
//                            false).setPositiveButton("확인",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.cancel();
//                                }
//                            });
//                    AlertDialog alert = alt_bld.create();
//                    alert.show();
                    //"NCT-Vi"
                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(mActivity);
                    alt_bld.setTitle("Luôn mở màn hình");
                    alt_bld.setMessage("Luôn mở màn hình để theo dõi chi tiết sự thay đổi trạng thái của ghế. Thiết bị sẽ tiêu thụ nhiều pin hơn khi mở chế độ này.").setCancelable(
                            false).setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = alt_bld.create();
                    alert.show();
                }
            });


        }
        else if(type == TYPE_OPENSOURCE)
        {
            OpenSourceViewHolder mOpenSourceViewHolder = (OpenSourceViewHolder)convertView;
            mOpenSourceViewHolder.txOpenSourceInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent mIntent = new Intent(mContext, LicenceActivity.class);
                    mContext.startActivity(mIntent);
                }
            });
        }
        else if(type == TYPE_CONTACT)
        {
            final ContactViewHolder mContactViewHolder = (ContactViewHolder)convertView;
            mContactViewHolder.imgArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(mContactViewHolder.getClicked() == false)
                    {
                        mContactViewHolder.setClicked(true);
                        mContactViewHolder.imgArrow.setImageResource(R.drawable.up);
                        mContactViewHolder.mContents.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        mContactViewHolder.setClicked(false);
                        mContactViewHolder.imgArrow.setImageResource(R.drawable.down);
                        mContactViewHolder.mContents.setVisibility(View.GONE);
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


}
