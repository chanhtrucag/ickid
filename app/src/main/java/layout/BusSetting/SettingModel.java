package layout.BusSetting;

/**
 * Created by DELL3 on 2019-01-29.
 */

public abstract class SettingModel {

    public static final int TYPE_DISPLAY = 0;
    public static final int TYPE_OPENSOURCE = 1;
    public static final int TYPE_CONTACT = 2;


    abstract public int getType();
}
