package kidsoft.buscare.BusSeatService;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import kidsoft.buscare.BaseActivity;
import kidsoft.buscare.BusInfo.NewBusAddActivity;
import kidsoft.buscare.BusInfoCamera.BarcodeScanActivity;
import kidsoft.buscare.Database.DBHelper;
import kidsoft.buscare.Database.models.SeatInfo;
import kidsoft.buscare.R;
import layout.BusSeatService.bsAdapterBusSeat;
import layout.BusSeatService.bsBusSeatItem;
import layout.BusSeatService.bsCenterSeat;
import layout.BusSeatService.bsOnSeatSelected;

import static kidsoft.buscare.BusInfo.SeatInfoActivity.REQUEST_CODE_SET_SEAT_INFO;
import static layout.BusSeatService.bsAdapterBusSeat.REQUEST_CODE_SEAT_INFO_MODI_ADD;

public class BusSeatServiceActivity extends BaseActivity implements bsOnSeatSelected {

    // Custom code
    private int K = 0;

    private static final String TAG = "BusSeatServiceActivity";
    public final static String MINI_BUS = "3X5", SMALL_BUS = "4X6", MEDIUM_BUS = "5X9", ETC_BUS = "5X11";

    private String bus_name, bus_code, bus_type;

    private int COLUMNS = 4;

    private RecyclerView recyclerView;
    private bsAdapterBusSeat mAdapterBusSeat;

    private Button btStart, btModify;
    private Button btSeatDel, btSeatDelCancel, btSeatModiOk;
    private ImageView ibBack;
    private TextView tvBusName, tvSeatUsed, tvSeatEmpty, tvSeatLowBattery;
    private int seatUsed, seatEmpty, seatLowBattery;
    private ImageView ivSeatLowBattery;
    //private ArrayList<SeatInfo> mSeats;

    private RelativeLayout mBssInfoLayout, mBssInfoBackLayout;

    private TextView bssInfoNumber,  bssInfoBattery, bssInfoAlert;
    private ImageView bt_bssClose, bt_bssModi;

    public static final int REQUEST_CODE_MOD_SEAT_INFO = 3000;

    private DBHelper _dbHelper;
    private int number_of_seat = 0;

    private boolean serviceStart = false, inforModifying = false;

    private ServiceConnection mConnection;
    private HashMap<String, SeatInfo> mDeviceMap;  // key - MAC address, value - poistion
    //private BusSeatService mBusSeatService;
    private BusSeatServiceClass mBusService;

    private static final int PERMISSIONS = 100;
    private String[] keyArray;
    private boolean doModify = false, doDelete = false;


    private List<bsBusSeatItem> items;
    private List<SeatInfo> mSeats, mTmpSeats;

    private Context mContext;
   // private String seatInfor[][];


    public boolean getDoModify()
    {
        return doModify;
    }

    public boolean getDoDelete()
    {
        return doDelete;
    }


    private PowerManager.WakeLock screenOn;


    /*
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("bus_name", bus_name);
        savedInstanceState.putString("bus_code", bus_code);
        savedInstanceState.putString("bus_seat", bus_type);

    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if(bus_name == null || bus_code == null || bus_type == bus_type )
        {
            bus_name = savedInstanceState.getString("bus_name");
            bus_code = savedInstanceState.getString("bus_code");
            bus_type = savedInstanceState.getString("bus_seat");
        }

    }
    */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bus_seat_service);
        _dbHelper = new DBHelper(getApplicationContext());

        mContext = this;

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS);

        mDeviceMap = new HashMap();

        bus_name = getIntent().getStringExtra("bus_name");
        bus_code = getIntent().getStringExtra("bus_code");
        bus_type = getIntent().getStringExtra("bus_seat");


        if(getIntent().getBooleanExtra("alarm", false) == true) {
//            AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//            //alt_bld.setTitle("");
//            alt_bld.setMessage(bus_name + " 출발 예정입니다. 서비스를 시작해주세요").setCancelable(
//                    false).setPositiveButton("확인",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.cancel();
//                        }
//                    });
//            AlertDialog alert = alt_bld.create();
//            alert.show();

            //"NCT-Vi"
            AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
            //alt_bld.setTitle("");
            alt_bld.setMessage(bus_name + " Xe chuẩn bị khởi hành. Bắt đầu chuyến").setCancelable(
                    false).setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = alt_bld.create();
            alert.show();
        }

        Log.e(TAG, "Bus info : " +  "bus_name :" + bus_name + " " + "bus code : " + bus_code + " bus type : " + bus_type);

        tvBusName = (TextView)findViewById(R.id.bsServiceTitle);
        tvBusName.setText(bus_name);

        tvSeatUsed  = (TextView)findViewById(R.id.bsUsedtext);
        tvSeatEmpty  = (TextView)findViewById(R.id.bsEmptytext);
        tvSeatLowBattery  = (TextView)findViewById(R.id.bsBatterytext);
        ivSeatLowBattery = (ImageView)findViewById(R.id.bssImg3);


        btStart = (Button)findViewById(R.id.bsServiceStart_btn);
        btModify = (Button)findViewById(R.id.bsServiceModify);
        ibBack = findViewById(R.id.bsServiceBackBtn);

        mBssInfoLayout = findViewById(R.id.bssInfoLayout);
        mBssInfoBackLayout = findViewById(R.id.bssInfoBackLayout);
        bssInfoNumber = findViewById(R.id.bssInfoSeatNumber);
        bssInfoBattery =  findViewById(R.id.bssInfoBatteryLv);
        bssInfoAlert = findViewById(R.id.bssInfoAlert);
        bt_bssClose = findViewById(R.id.bssInfoClose);
        bt_bssModi = findViewById(R.id.bssInfoModi);

        btSeatDel = findViewById(R.id.bssSeatDelBt);
        btSeatDelCancel = findViewById(R.id.bssSeatDelCancelBt);

        btSeatModiOk = findViewById(R.id.bssSeatModiOK);

        initSeatInfoLayout();

        if(bus_type.equals(BusSeatServiceActivity.MINI_BUS)) {
            COLUMNS = 3;
            number_of_seat = 15;
        }else if(bus_type.equals(BusSeatServiceActivity.SMALL_BUS)) {
            COLUMNS = 4;
            number_of_seat = 24;
        }else if(bus_type.equals(BusSeatServiceActivity.MEDIUM_BUS)) {
            COLUMNS = 5;
            number_of_seat = 45;
        }else if(bus_type.equals(BusSeatServiceActivity.ETC_BUS)) {
            COLUMNS = 5;
            number_of_seat = 55;
        }else {
            COLUMNS = 5;
            number_of_seat = 55;
        }

        items = new ArrayList<>();
        mTmpSeats = new ArrayList<>();

        GridLayoutManager manager = new GridLayoutManager(this, COLUMNS);
        recyclerView = (RecyclerView) findViewById(R.id.bsService_items);
        recyclerView.setLayoutManager(manager);

        mSeats = _dbHelper.mSeat.QuerybyCode(bus_name, bus_code);

        Log.e(TAG, "Bus info : " +  " " + "bus code : " + bus_code + " bus type : " + bus_type);

        for(int i=0; i<mSeats.size(); ++i)
        {
            Log.e(TAG, "Regi Seat____ : " +  " " + mSeats.get(i).getSeatNumber()  +  " / " + mSeats.get(i).getSeatPosition() + " / " + mSeats.get(i).getSeatStatus());
        }


        LocalBroadcastManager.getInstance(this).registerReceiver(mRefreshBroadcastReceiver, new IntentFilter(BusSeatServiceClass.REFRESH_LOCAL_BROADCAST));
        LocalBroadcastManager.getInstance(this).registerReceiver(mCautionReceiver, new IntentFilter(BusSeatServiceClass.CAUTION_SEAT_BROADCAST));


        for(int i=0; i<number_of_seat; ++i)
        {
            items.add(new bsCenterSeat(String.valueOf(i)));
        }

        for(int i=0; i<mSeats.size(); ++i)
        {
            items.remove(Integer.parseInt(mSeats.get(i).getSeatPosition()));
            items.add(Integer.parseInt(mSeats.get(i).getSeatPosition()), new bsCenterSeat(String.valueOf(mSeats.get(i).getSeatPosition()), mSeats.get(i)));
            mDeviceMap.put(mSeats.get(i).getSeatNumber(),mSeats.get(i));
            SeatInfo mtmp = mDeviceMap.get(mSeats.get(i).getSeatNumber());
            Log.e(TAG, "Regi Seat : " +  " " + items.get(Integer.parseInt(mSeats.get(i).getSeatPosition())).getSeatSerialNumber()  +  " / " + items.get(Integer.parseInt(mSeats.get(i).getSeatPosition())).getSeatPosition() + " / " + items.get(Integer.parseInt(mSeats.get(i).getSeatPosition())).getStatus());
        }


        Set<String> keySet = mDeviceMap.keySet();
        keyArray = keySet.toArray(new String[keySet.size()]);
        int init_low_battery_number = 0, init_error_number = 0;
        for(int i =0 ; i <keyArray.length; ++i)
        {
            SeatInfo mtmp = mDeviceMap.get(keyArray[i]);

            if(mtmp.getSeatStatus().equals(bsBusSeatItem.STATUS_ERROR))
            {
                ++init_error_number;
            }

            if(Integer.parseInt(mtmp.getSeatBattery()) < 1900 && Integer.parseInt(mtmp.getSeatBattery()) > -1)
            {
                ++init_low_battery_number;
            }
            //Log.e(TAG, "device key: " + keyArray[i]  +  " :-> " +  mtmp.getSeatPosition());
        }

//        tvSeatUsed.setText("0"+"좌석");
//        tvSeatEmpty.setText((keyArray.length - init_low_battery_number)+"좌석");
//        tvSeatLowBattery.setText(init_low_battery_number +"좌석");
        //"NCT-Vi"
        tvSeatUsed.setText("0"+" Có trẻ");
        tvSeatEmpty.setText((keyArray.length - init_low_battery_number)+" Ghế trống");
        tvSeatLowBattery.setText(init_low_battery_number +" Pin yếu");
        //ivSeatLowBattery.setVisibility(View.VISIBLE);

        if(init_error_number > 0)
        {
//            AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//            alt_bld.setTitle("방석을 확인해주세요");
//            alt_bld.setMessage("연결할 수 없는 방석이 있습니다. 직접 확인해주세요.")
//                    .setCancelable(false)
//                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.cancel();
//                        }
//                    });
//            AlertDialog alert = alt_bld.create();
//            alert.show();
            //"NCT-Vi"
            AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
            alt_bld.setTitle("Kiểm tra lại nệm ngồi");
            alt_bld.setMessage("Phát hiện nệm ngồi không được kết nối. Vui lòng kiểm tra lại.")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = alt_bld.create();
            alert.show();
        }

        if(init_low_battery_number > 0 )
        {
//            AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//            alt_bld.setTitle("방석을 확인해주세요");
//            alt_bld.setMessage("배터리가 부족한 방석이 있습니다. 직접 확인해주세요.")
//                    .setCancelable(false)
//                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.cancel();
//                        }
//                    });
//            AlertDialog alert = alt_bld.create();
//            alert.show();
            //"NCT-Vi"
            AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
            alt_bld.setTitle("Kiểm tra lại nệm ngồi");
            alt_bld.setMessage("Phát hiện có nệm ngồi ở trạng thái pin yếu. Vui lòng kiểm tra lại.")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = alt_bld.create();
            alert.show();
        }



        mAdapterBusSeat = new bsAdapterBusSeat(this, this, this, items);
        recyclerView.setAdapter(mAdapterBusSeat);

        mConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName className, IBinder service) {

                /*  --- old
                BusSeatService.LocalBinder binder = (BusSeatService.LocalBinder) service;
                Log.e(TAG, "mDeviceMap: " + mDeviceMap.size());
                binder.GetDeviceMap(mDeviceMap);
                mBusSeatService = binder.getService();
                */

                refreshSeatList();
                BusSeatServiceClass.LocalBinder binder = (BusSeatServiceClass.LocalBinder) service;
                Log.e(TAG, "Service Connected, mDeviceMap: " + mDeviceMap.size());
                binder.GetDeviceMap(mDeviceMap, BusSeatServiceActivity.this, bus_name, bus_code, bus_type);
                mBusService = binder.getService();

            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.e(TAG, "Service Disconnected");
            }
        };

        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(serviceStart == true)
                {
//                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//                    alt_bld.setTitle("현재 서비스 실행 중입니다");
//                    alt_bld.setMessage("서비스를 종료해야 나가실수 있습니다.")
//                            .setCancelable(false)
//                            .setPositiveButton("확인", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.cancel();
//                                }
//                            });
//                    AlertDialog alert = alt_bld.create();
//                    alert.show();
                    //"NCT-Vi"
                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
                    alt_bld.setTitle("Có chuyến xe đang được mở.");
                    alt_bld.setMessage("Vui lòng kết thúc chuyến để thoát.")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = alt_bld.create();
                    alert.show();
                }
                else if(mTmpSeats.size()>0)
                {
//                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//                    alt_bld.setTitle("정보가 저장되지 않습니다");
//                    alt_bld.setMessage("저장하지 않고 나가시겠습니까?").setCancelable(
//                            false).setPositiveButton("확인",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//
//                                    Log.e(TAG, "Delete Seats " + mTmpSeats.size());
//                                    for(int i=0; i<mTmpSeats.size(); ++i) {
//                                        _dbHelper.mSeat.Delete(mTmpSeats.get(i));
//                                    }
//
//                                    finish();
//                                }
//                            }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                        }
//                    })
//                    ;
                    //"NCT-Vi"
                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
                    alt_bld.setTitle("Vui lòng kết thúc chuyến để thoát.");
                    alt_bld.setMessage("Xác nhận thoát và không lưu dữ liệu?").setCancelable(
                            false).setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    Log.e(TAG, "Delete Seats " + mTmpSeats.size());
                                    for(int i=0; i<mTmpSeats.size(); ++i) {
                                        _dbHelper.mSeat.Delete(mTmpSeats.get(i));
                                    }

                                    finish();
                                }
                            }).setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    ;
                    AlertDialog alert = alt_bld.create();
                    alert.show();
                }
                else
                {
                    finish();
                }
            }
        });



        btStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(serviceStart == false)
                {
                    if(doDelete == true || doModify == true)
                    {
//                        AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//                        alt_bld.setTitle("현재 좌석 편집 중입니다");
//                        alt_bld.setMessage("편집을 완료해야 시작할 수 있습니다.").setCancelable(
//                                false).setPositiveButton("확인",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.cancel();
//                                    }
//                                });
//                        AlertDialog alert = alt_bld.create();
//                        alert.show();
                        //"NCT-Vi"
                        AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
                        alt_bld.setTitle("Đang chỉnh sửa thông tin");
                        alt_bld.setMessage("Vui lòng hoàn tất chỉnh sửa trước khi bắt đầu chuyến.").setCancelable(
                                false).setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = alt_bld.create();
                        alert.show();
                    }
                    else
                    {
                        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                        if (mBluetoothAdapter == null) {
//                            final AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//                            alt_bld.setTitle("블루투스 기능을 지원하지 않는 장치입니다");
//                            alt_bld.setMessage("정상 서비스를 위해서 해당 기능이 필요합니다.").setCancelable(
//                                    false).setPositiveButton("확인",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                        }
//                                    });
//                            AlertDialog alert = alt_bld.create();
//                            alert.show();
                            //"NCT-Vi"
                            final AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
                            alt_bld.setTitle("Thiết bị không hỗ trợ kết nối Bluetooth");
                            alt_bld.setMessage("Ứng dụng yêu cầu cần có kết nối Bluetooth để hoạt động.").setCancelable(
                                    false).setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        }
                                    });
                            AlertDialog alert = alt_bld.create();
                            alert.show();
                        } else {
                            if (!mBluetoothAdapter.isEnabled()) {
//                                AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//                                alt_bld.setTitle("블루투스 기능을 실행해주세요");
//                                alt_bld.setMessage("정상 서비스를 위해서 해당 기능이 필요합니다.").setCancelable(
//                                        false).setPositiveButton("확인",
//                                        new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                startActivity(new Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
//                                            }
//                                        });
//                                AlertDialog alert = alt_bld.create();
//                                alert.show();
                                //"NCT-Vi"
                                AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
                                alt_bld.setTitle("Vui lòng mở kết nối Bluetooth");
                                alt_bld.setMessage("Ứng dụng yêu cầu cần có kết nối Bluetooth để hoạt động.").setCancelable(
                                        false).setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                startActivity(new Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
                                            }
                                        });
                                AlertDialog alert = alt_bld.create();
                                alert.show();
                            }
                            else
                            {
                                LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
                                boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                                if(statusOfGPS == false)
                                {
//                                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//                                    alt_bld.setTitle("위치서비스를 실행해주세요");
//                                    alt_bld.setMessage("정상 서비스를 위해서 위치서비스가 필요합니다.").setCancelable(
//                                            false).setPositiveButton("확인",
//                                            new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//                                                }
//                                            });
//                                    AlertDialog alert = alt_bld.create();
//                                    alert.show();
                                    //"NCT-Vi"
                                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
                                    alt_bld.setTitle("Vui lòng mở Định vị");
                                    alt_bld.setMessage("Ứng dụng yêu cầu cần có dịch vụ Định vị để hoạt động.").setCancelable(
                                            false).setPositiveButton("OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                                }
                                            });
                                    AlertDialog alert = alt_bld.create();
                                    alert.show();
                                }
                                else
                                {
//                                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//                                    alt_bld.setTitle("운행을 시작합니다");
//                                    alt_bld.setMessage(bus_name + " 가 안전한 운행을 시작합니다.").setCancelable(
//                                            true);
//                                    final AlertDialog alert = alt_bld.create();
//                                    alert.show();
                                    //"NCT-Vi"
                                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
                                    alt_bld.setTitle("Bắt đầu chuyến");
                                    alt_bld.setMessage(bus_name + " Xe bắt đầu khởi hành.").setCancelable(
                                            true);
                                    final AlertDialog alert = alt_bld.create();
                                    alert.show();

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            alert.cancel();
                                        }
                                    }, 1500);



                                    serviceStart = true;
                                    startBusSeatService();
//                                    btStart.setText("종료");
                                    //"NCT-Vi"
                                    btStart.setText("Kết thúc");
                                    btStart.setBackgroundColor(Color.parseColor("#ff6260"));
                                    for(int i=0; i<number_of_seat; ++i)
                                    {
                                        if(mAdapterBusSeat.getItemInfo(i).getSeatSerialNumber().isEmpty() == false)
                                        {
                                            // --- alert_msg
                                            //mAdapterBusSeat.changeSeatStatus(bsBusSeatItem.STATUS_EMPTY, i);
                                        }
                                    }
                                    mAdapterBusSeat.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }
                else
                {
                    boolean alertSset = false;
                    for(int j = 0; j<mDeviceMap.size(); ++j) {

                        SeatInfo checkSeat = mBusService.getSeatInformation(keyArray[j]);
                        if (checkSeat.getSeatStatus() == bsBusSeatItem.STATUS_OCCUPIED) {

                            alertSset = true;
                            break;
                        }
                    }

                    if(alertSset == true) {

                        final Vibrator vib = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                        long[] pattern = {0, 2000, 2000};
                        vib.vibrate(pattern, 0);

//                        AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//                        alt_bld.setTitle("좌석 확인 필요");
//                        alt_bld.setMessage("아직 자리에 남아있는 아이가 있습니다. 확인해주세요!").setCancelable(
//                                false).setPositiveButton("확인",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.cancel();
//                                        vib.cancel();
//                                    }
//                                });
//                        AlertDialog alert = alt_bld.create();
//                        alert.show();
                        //"NCT-Vi"
                        AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
                        alt_bld.setTitle("Yêu cầu kiểm tra ghế");
                        alt_bld.setMessage("Phát hiện có trẻ còn trên xe. Vui lòng kiểm tra lại!").setCancelable(
                                false).setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        vib.cancel();
                                    }
                                });
                        AlertDialog alert = alt_bld.create();
                        alert.show();

                    }
                    else
                    {
                        for(int j = 0; j<mDeviceMap.size(); ++j) {
                            SeatInfo checkSeat = mBusService.getSeatInformation(keyArray[j]);
                            if (checkSeat.getSeatStatus() == bsBusSeatItem.STATUS_LOW_BATTERY) {

//                                AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//                                alt_bld.setTitle("좌석 확인 필요");
//                                alt_bld.setMessage("배터리가 부족한 좌석이 있습니다. 직접 확인해주세요.").setCancelable(
//                                        false).setPositiveButton("확인",
//                                        new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.cancel();
//                                            }
//                                        });
//                                AlertDialog alert = alt_bld.create();
//                                alert.show();
                                //"NCT-Vi"
                                AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
                                alt_bld.setTitle("Yêu cầu kiểm tra ghế");
                                alt_bld.setMessage("Phát hiện ghế có trạng thái pin yếu. Vui lòng kiểm tra lại.").setCancelable(
                                        false).setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                AlertDialog alert = alt_bld.create();
                                alert.show();

                                break;
                            }
                        }

                        // Custom code hear
                        Intent endScanIntent = new Intent(getApplicationContext(), BarcodeScanActivity.class);
                        startActivityForResult(endScanIntent, 1);
                        if (K == 1) {
                            Log.d("NCT","AHIHI");
                            serviceStart = false;
                            mBusService.setAbnormalEnd(false);
                            btStart.setText("Bắt đầu");
                        }

//                        AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//                        alt_bld.setTitle("운행을 종료합니다");
//                        alt_bld.setMessage("안전한 운행 되셨나요?\n종료하기 전에 한 번 더 차량을 확인해주세요.").setCancelable(
//                                true);
//                        final AlertDialog alert = alt_bld.create();
//                        alert.show();
                        //"NCT-Vi"
                        AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
                        alt_bld.setTitle("Kết thúc chuyến");
                        alt_bld.setMessage("Xác nhận kết thúc chuyến?\nVui lòng kiểm tra lại trước khi rời xe.").setCancelable(
                                true);
                        final AlertDialog alert = alt_bld.create();
                        alert.show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                alert.cancel();
                            }
                        }, 1500);

                        serviceStart = false;
                        mBusService.setAbnormalEnd(false);

//                        btStart.setText("출발");
                        //"NCT-Vi"
                        btStart.setText("Bắt đầu");
                        btStart.setBackgroundColor(Color.parseColor("#60a9ff"));

                            /* --- alert_msg
                            for(int i=0; i<number_of_seat; ++i)
                            {
                                if(mAdapterBusSeat.getItemInfo(i).getSeatSerialNumber().isEmpty() == false)
                                {
                                    mAdapterBusSeat.changeSeatStatus(bsBusSeatItem.STATUS_EMPTY, i);
                                }
                            }
                            --- */

                        for(int i = 0; i<mDeviceMap.size(); ++i) {
                            SeatInfo mEndSeat = mBusService.getSeatInformation(keyArray[i]);
                            Log.e(TAG, "End Seat : " + mEndSeat.getSeatPosition() +  " / " + mEndSeat.getSeatNumber() +  " / " +  mEndSeat.getSeatBattery() + " / " + mEndSeat.getSeatStatus());
                            //mEndSeat.setSeatBattery(mBusSeatService.getSeatInformation(keyArray[i]).getSeatBattery());
                            _dbHelper.mSeat.Update(mEndSeat);
                        }


                        stopBusSeatService();

                        mAdapterBusSeat.notifyDataSetChanged();

                    }
                }
            }
        });

        btModify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(serviceStart == true)
                {
//                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//                    alt_bld.setTitle("현재 서비스 실행 중입니다");
//                    alt_bld.setMessage("서비스를 종료해야 편집할수 있습니다.").setCancelable(
//                            false).setPositiveButton("확인",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.cancel();
//                                }
//                            });
//                    AlertDialog alert = alt_bld.create();
//                    alert.show();
                    //"NCT-Vi"
                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
                    alt_bld.setTitle("Có chuyến xe đang được mở.");
                    alt_bld.setMessage("Vui lòng kết thúc chuyến để chỉnh sửa.").setCancelable(
                            false).setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = alt_bld.create();
                    alert.show();
                }
                else
                {
//                    final String[] STRINGS = {"좌석 삭제","좌석 추가"};
                    //"NCT-Vi"
                    final String[] STRINGS = {"Xóa ghế","Thêm ghế"};
                    final ListPopupWindow mPopupWindow = new ListPopupWindow(BusSeatServiceActivity.this);
                    mPopupWindow.setAdapter(new ArrayAdapter<String>(BusSeatServiceActivity.this, android.R.layout.simple_list_item_1, STRINGS));
                    mPopupWindow.setVerticalOffset((-1 * v.getHeight()) + 20);
                    mPopupWindow.setHorizontalOffset(-100);
                    mPopupWindow.setAnchorView(v);
                    mPopupWindow.setDropDownGravity(Gravity.RIGHT);
                    mPopupWindow.setWidth(BusSeatServiceActivity.this.getResources().getDimensionPixelSize(R.dimen.overflow_width_1));
                    mPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            mPopupWindow.dismiss();

                            if(position == 0)
                            {
                                //삭제

                                btStart.setVisibility(View.INVISIBLE);
                                btStart.setClickable(false);
                                btModify.setVisibility(View.GONE);
                                btSeatDel.setVisibility(View.VISIBLE);
                                btSeatDelCancel.setVisibility(View.VISIBLE);

                                List<Integer> selectedSeats = mAdapterBusSeat.getSelectedItems();
                                for(int i=0; i < mAdapterBusSeat.getSelectedItemCount(); ++i)
                                {
                                    mAdapterBusSeat.toggleSelection(selectedSeats.get(i));
                                    HideSeatInfo();
                                }

                                doDelete = true;
                                mAdapterBusSeat.clearSelection();
                                refreshSeatList();
                                //mAdapterBusSeat.notifyDataSetChanged();

                                btSeatDel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        final List<Integer> selectedSeats = mAdapterBusSeat.getSelectedItems();
                                        final List<bsBusSeatItem> seatItems = mAdapterBusSeat.getmItems();

                                        final BusSeatDeleteDialog busSeatDeleteDialog = new BusSeatDeleteDialog(BusSeatServiceActivity.this,mAdapterBusSeat.getSelectedItemCount() );
                                        busSeatDeleteDialog.show();
                                        busSeatDeleteDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {

                                                if(busSeatDeleteDialog.getResult() == true)
                                                {
                                                    for(int i=0; i < mAdapterBusSeat.getSelectedItemCount(); ++i)
                                                    {
                                                        //Log.e(TAG, "selected Seat : " + seatItems.get(selectedSeats.get(i)).getSeatPosition() +  " / " + seatItems.get(selectedSeats.get(i)).getSeatSerialNumber());

                                                        List<SeatInfo> delSeats = _dbHelper.mSeat.QuerybySeatCode(seatItems.get(selectedSeats.get(i)).getSeatSerialNumber(), bus_name, seatItems.get(selectedSeats.get(i)).getBusCode());
                                                        for(int j = 0; j < delSeats.size(); ++j)
                                                        {
                                                            Log.e(TAG, "Deleted Seat : " + delSeats.get(j).getSeatPosition() +  " / " + delSeats.get(j).getSeatNumber());
                                                            _dbHelper.mSeat.Delete(delSeats.get(j));
                                                        }
                                                    }

                                                    for(int i=0; i < mAdapterBusSeat.getSelectedItemCount(); ++i)
                                                    {
                                                        mAdapterBusSeat.toggleSelection(selectedSeats.get(i));
                                                    }
                                                    mAdapterBusSeat.clearSelection();
                                                    doDelete = false;

                                                    refreshSeatList();
                                                    mAdapterBusSeat.notifyDataSetChanged();

                                                    btStart.setVisibility(View.VISIBLE);
                                                    btStart.setClickable(true);
                                                    btModify.setVisibility(View.VISIBLE);
                                                    btSeatDel.setVisibility(View.GONE);
                                                    btSeatDelCancel.setVisibility(View.GONE);

                                                }
                                                else
                                                {

                                                }

                                            }
                                        });


                                    }
                                });
                                btSeatDelCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        List<Integer> selectedSeats = mAdapterBusSeat.getSelectedItems();
                                        for(int i=0; i < mAdapterBusSeat.getSelectedItemCount(); ++i)
                                        {
                                            mAdapterBusSeat.toggleSelection(selectedSeats.get(i));
                                        }

                                        doDelete = false;

                                        refreshSeatList();
                                        mAdapterBusSeat.notifyDataSetChanged();

                                        btStart.setVisibility(View.VISIBLE);
                                        btStart.setClickable(true);
                                        btModify.setVisibility(View.VISIBLE);
                                        btSeatDel.setVisibility(View.GONE);
                                        btSeatDelCancel.setVisibility(View.GONE);


                                        mAdapterBusSeat.clearSelection();
                                    }
                                });
                            }
                            else
                            {
                                //추가

                                Log.e(TAG, "doModify");

                                doModify = true;

                                List<Integer> selectedSeats = mAdapterBusSeat.getSelectedItems();
                                for(int i=0; i < mAdapterBusSeat.getSelectedItemCount(); ++i)
                                {
                                    mAdapterBusSeat.toggleSelection(selectedSeats.get(i));
                                    HideSeatInfo();
                                }

                                mAdapterBusSeat.notifyDataSetChanged();
                                mTmpSeats.clear();

                                btStart.setVisibility(View.INVISIBLE);
                                btStart.setClickable(false);

                                btModify.setVisibility(View.GONE);
                                btSeatModiOk.setVisibility(View.VISIBLE);
                                btSeatModiOk.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        doModify = false;
                                        mAdapterBusSeat.notifyDataSetChanged();
                                        mTmpSeats.clear();

                                        btStart.setVisibility(View.VISIBLE);
                                        btStart.setClickable(true);
                                        btModify.setVisibility(View.VISIBLE);
                                        btSeatModiOk.setVisibility(View.GONE);
                                    }
                                });

                            }



                        }
                    });
                    mPopupWindow.show();
                }

                mAdapterBusSeat.notifyDataSetChanged();
            }
        });


    }

    private void refreshSeatList()
    {
        mSeats = _dbHelper.mSeat.QuerybyCode(bus_name, bus_code);
        items.clear();
        mDeviceMap.clear();

        for(int i=0; i<number_of_seat; ++i)
        {
            items.add(new bsCenterSeat(String.valueOf(i)));
        }

        for(int i=0; i<mSeats.size(); ++i)
        {
            items.remove(Integer.parseInt(mSeats.get(i).getSeatPosition()));
            items.add(Integer.parseInt(mSeats.get(i).getSeatPosition()), new bsCenterSeat(String.valueOf(mSeats.get(i).getSeatPosition()), mSeats.get(i)));
            mDeviceMap.put(mSeats.get(i).getSeatNumber(),mSeats.get(i));
            SeatInfo mtmp = mDeviceMap.get(mSeats.get(i).getSeatNumber());
            Log.e(TAG, "Regi Seat : " +  " " + mtmp.getSeatNumber()  +  " / " + mSeats.get(i).getSeatPosition());
        }

        Set<String> keySet = mDeviceMap.keySet();
        keyArray = keySet.toArray(new String[keySet.size()]);

//        tvSeatUsed.setText("0"+"좌석");
//        tvSeatEmpty.setText(keyArray.length +"좌석");
//        tvSeatLowBattery.setText("0"+"좌석");

        //"NCT-Vi"
        tvSeatUsed.setText("0"+" Có trẻ");
        tvSeatEmpty.setText(keyArray.length +" Ghế trống");
        tvSeatLowBattery.setText("0"+" Pin yếu");

        mAdapterBusSeat = new bsAdapterBusSeat(this, this, this, items);
        recyclerView.setAdapter(mAdapterBusSeat);

    }


    public interface IDeviceMapService {
        public void GetDeviceMap(HashMap<String, SeatInfo> mDeviceMap);
    }

    boolean isBound = false;

    private void startBusSeatService()
    {
        // Bind to LocalService
        // -- old Intent intent = new Intent(this, BusSeatService.class);
        Intent intent = new Intent(this, BusSeatServiceClass.class);
        //intent.putExtra("device_map", mDeviceMap);
        isBound = mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        Log.e(TAG, " isBound : " +  isBound );

    }


    private void stopBusSeatService()
    {
        mContext.unbindService(mConnection);
    }



    private bsBusSeatItem selectedSeat;
    public void ShowSeatInfo(bsBusSeatItem mSeat)
    {
        Log.e(TAG, "Seat Info : " +  mSeat.getSeatPosition() + "/" + mSeat.getSeatSerialNumber() + "/" + mSeat.getStatus());
        selectedSeat = mSeat;
        bssInfoNumber.setText(mSeat.getSeatSerialNumber());
        bssInfoAlert.setVisibility(View.INVISIBLE);


        if(mSeat.getCautionStatus() == true) {
            if(!mSeat.getStatus().equals(bsBusSeatItem.STATUS_ERROR) && !mSeat.getErrorStatus()) {
                bssInfoAlert.setVisibility(View.VISIBLE);
//                bssInfoAlert.setText("착석확인");
                //"NCT-Vi"
                bssInfoAlert.setText("Kiểm tra ghế");
            }else
            {
                if(mSeat.getErrorStatus() == true) {
                    bssInfoAlert.setVisibility(View.VISIBLE);
//                    bssInfoAlert.setText("방석확인");
                    //"NCT-Vi"
                    bssInfoAlert.setText("Kiểm tra nệm ngồi");
                }
            }
        }

        if(mSeat.getStatus().equals(bsBusSeatItem.STATUS_ERROR))
        {
            bssInfoAlert.setVisibility(View.VISIBLE);
//            bssInfoAlert.setText("방석확인");
            //"NCT-Vi"
            bssInfoAlert.setText("Kiểm tra nệm ngồi");
        }

        if(Integer.parseInt(mSeat.getBetteryLevel()) > 2100)
        {
//            bssInfoBattery.setText("여유");
            //"NCT-Vi"
            bssInfoBattery.setText("Pin đầy");
        }
        else if(Integer.parseInt(mSeat.getBetteryLevel()) <= 2100 && Integer.parseInt(mSeat.getBetteryLevel()) >= 1900)
        {
//            bssInfoBattery.setText("부족");
            //"NCT-Vi"
            bssInfoBattery.setText("Pin yếu");
        }
        else if(Integer.parseInt(mSeat.getBetteryLevel()) < 1900 && Integer.parseInt(mSeat.getBetteryLevel()) > -1)
        {
//            bssInfoBattery.setText("교체");
//            bssInfoAlert.setVisibility(View.VISIBLE);
//            bssInfoAlert.setText("방석교체");
            //"NCT-Vi"
            bssInfoBattery.setText("Thay thế");
            bssInfoAlert.setVisibility(View.VISIBLE);
            bssInfoAlert.setText("Thay thế");
        }
        else if(Integer.parseInt(mSeat.getBetteryLevel()) == -1)
        {
//            bssInfoBattery.setText("대기");
            //"NCT-Vi"
            bssInfoBattery.setText("Chờ");
        }
        else
        {
            bssInfoBattery.setText(mSeat.getBetteryLevel());
        }

        bt_bssClose.setVisibility(View.VISIBLE);
        bt_bssModi.setVisibility(View.VISIBLE);

        if(Integer.parseInt(mSeat.getBetteryLevel()) < 1900)
        {
            bssInfoBattery.setBackgroundResource(R.drawable.battery_no);
        }
        else if(Integer.parseInt(mSeat.getBetteryLevel()) > 2100)
        {
            bssInfoBattery.setBackgroundResource(R.drawable.battery_ok);
        }
        else
        {
            bssInfoBattery.setBackgroundResource(R.drawable.battery_low);
        }

        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                mBssInfoLayout.getHeight(),  // fromYDelta
                0);                // toYDelta

        animate.setDuration(200);
        animate.setFillAfter(true);
        mBssInfoLayout.startAnimation(animate);
        mBssInfoLayout.setClickable(true);
        mBssInfoLayout.setVisibility(View.VISIBLE);
        mBssInfoBackLayout.setVisibility(View.VISIBLE);
    }

    public void HideSeatInfo()
    {
        TranslateAnimation animate = new TranslateAnimation(
                    0,                 // fromXDelta
                    0,                 // toXDelta
                    0,                 // fromYDelta
                    2*mBssInfoLayout.getHeight()); // toYDelta
        animate.setDuration(100);
        animate.setFillAfter(true);
        mBssInfoLayout.startAnimation(animate);

        mBssInfoLayout.setClickable(false);
        mBssInfoLayout.setVisibility(View.GONE);
        mBssInfoBackLayout.setVisibility(View.GONE);
        bt_bssClose.setVisibility(View.GONE);
        bt_bssModi.setVisibility(View.GONE);
    }

    private void initSeatInfoLayout()
    {
        //bssInfoNumber = findViewById(R.id.bssInfoSeatNumber);
        //bssInfoBattery =  findViewById(R.id.bssInfoBatteryLv);
        bt_bssClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideSeatInfo();
                mAdapterBusSeat.toggleSelection(selectedSeat.getSeatPosition());
            }
        });

        bt_bssModi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(serviceStart == true)
                {
//                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//                    alt_bld.setTitle("현재 서비스 실행 중입니다");
//                    alt_bld.setMessage("서비스를 종료해야 편집할수 있습니다.").setCancelable(
//                            false).setPositiveButton("확인",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.cancel();
//                                }
//                            });
//                    AlertDialog alert = alt_bld.create();
//                    alert.show();
                    //"NCT-Vi"
                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
                    alt_bld.setTitle("Có chuyến xe đang được mở.");
                    alt_bld.setMessage("Vui lòng kết thúc chuyến để chỉnh sửa.").setCancelable(
                            false).setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = alt_bld.create();
                    alert.show();
                }
                else
                {
                    Intent i = new Intent(BusSeatServiceActivity.this, BusSeatModifyDialog.class);
                    i.putExtra("seat_number", selectedSeat.getSeatSerialNumber());
                    i.putExtra("seat_position", selectedSeat.getSeatPosition());
                    startActivityForResult(i, REQUEST_CODE_MOD_SEAT_INFO);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if((resultCode == RESULT_OK) && (requestCode == REQUEST_CODE_MOD_SEAT_INFO))
        {
            if(selectedSeat.getSeatSerialNumber().equals(data.getStringExtra("result_seat")))
            {
                Log.e(TAG, "Same Seat Number");
            }
            else
            {
                Log.e(TAG, "New Seat Number: " + data.getStringExtra("result_seat"));


                List<SeatInfo> mCheckSet = _dbHelper.mSeat.QuerybySeatCode(data.getStringExtra("result_seat"), bus_name, bus_code);
                if(mCheckSet.size() !=0)
                {
                    Log.e(TAG, "Already resisted number");
//                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
//                    alt_bld.setTitle("이미 등록된 제품코드입니다");
//                    alt_bld.setMessage("제품코드가 등록된 좌석을 삭제해야합니다.").setCancelable(
//                            false).setPositiveButton("확인",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.cancel();
//                                }
//                            });
//                    AlertDialog alert = alt_bld.create();
//                    alert.show();

                    //"NCT-Vi"
                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
                    alt_bld.setTitle("Mã nệm ngồi này đã được sử dụng");
                    alt_bld.setMessage("Vui lòng xóa ghế đã được đăng ký với mã nệm ngồi này.").setCancelable(
                            false).setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = alt_bld.create();
                    alert.show();
                }
                else
                {
                    List<SeatInfo> mSeats = _dbHelper.mSeat.QuerybySeatCode(selectedSeat.getSeatSerialNumber(), bus_name, selectedSeat.getBusCode());
                    for(int i=0; i < mSeats.size(); ++i)
                    {
                        mSeats.get(i).setSeatNumber(data.getStringExtra("result_seat"));
                        _dbHelper.mSeat.Update(mSeats.get(i));
                    }

                    selectedSeat.setSeatInfo(data.getStringExtra("result_seat"));
                    bssInfoNumber.setText(data.getStringExtra("result_seat"));

                    mDeviceMap.clear();
                    mSeats = _dbHelper.mSeat.QuerybyCode(bus_name, selectedSeat.getBusCode());
                    for(int i =0; i<mSeats.size(); ++i)
                    {
                        mDeviceMap.put(mSeats.get(i).getSeatNumber(), mSeats.get(i));
                    }

                    Set<String> keySet = mDeviceMap.keySet();
                    keyArray = keySet.toArray(new String[keySet.size()]);

                    //SeatInfo mtmp = mDeviceMap.get(mSeats.get(i).getSeatNumber());

                    //mAdapterBusSeat.notifyDataSetChanged();
                    mAdapterBusSeat.notifyDataSetChanged();

                }
            }
        }
        else if((resultCode == RESULT_OK) && (requestCode == REQUEST_CODE_SEAT_INFO_MODI_ADD))
        {

            if(data.getStringExtra("result_seat").equals("CANCEL_REGI") == false)
            {
                List<SeatInfo> searchSeats = _dbHelper.mSeat.QuerybySeatCode(data.getStringExtra("result_seat"), bus_name, bus_code);
                if(searchSeats.size() !=0) {
                    Log.e(TAG, "Already resisted number");
//                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
//                    alt_bld.setTitle("이미 등록된 제품코드입니다");
//                    alt_bld.setMessage("제품코드가 등록된 좌석을 삭제해야합니다.").setCancelable(
//                            false).setPositiveButton("확인",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.cancel();
//                                }
//                            });
//                    AlertDialog alert = alt_bld.create();
//                    alert.show();
                    //"NCT_Vi"

                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
                    alt_bld.setTitle("Mã nệm ngồi này đã được sử dụng");
                    alt_bld.setMessage("Vui lòng xóa ghế đã được đăng ký với mã nệm ngồi này.").setCancelable(
                            false).setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = alt_bld.create();
                    alert.show();
                }
                else {
                    Log.e(TAG, "New Added Seat Number: " + data.getStringExtra("result_seat")  +  " / " + mAdapterBusSeat.getSelectedItem());
                    SeatInfo mSeat = new SeatInfo();

                    mSeat.setBusName(bus_name);
                    mSeat.setBusCode(bus_code);
                    mSeat.setBusSeat(bus_type);

                    mSeat.setSeatNumber(data.getStringExtra("result_seat"));
                    mSeat.setSeatPosition(Integer.toString(mAdapterBusSeat.getSelectedItem()));
                    mSeat.setSeatStatus("status_empty");
                    mSeat.setSeatBattery("-1");

                    mTmpSeats.add(0, mSeat);
                    _dbHelper.mSeat.Add(mSeat);

                    refreshSeatList();
                    mAdapterBusSeat.notifyDataSetChanged();
                }
            }
            else
            {
                Log.e(TAG, "New Added Canceled : " + mAdapterBusSeat.getSelectedItem());
            }
        }
    }


    public void onBackPressed() {

        if(serviceStart == true)
        {
//            AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//            alt_bld.setTitle("현재 서비스 실행 중입니다");
//            alt_bld.setMessage("서비스를 종료해야 나가실수 있습니다.").setCancelable(
//                    false).setPositiveButton("확인",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.cancel();
//                        }
//                    });
//            AlertDialog alert = alt_bld.create();
//            alert.show();
            //"NCT-Vi"
            AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
            alt_bld.setTitle("Có chuyến xe đang được mở.");
            alt_bld.setMessage("Vui lòng kết thúc chuyến để thoát.").setCancelable(
                    false).setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = alt_bld.create();
            alert.show();
        }
        else if(mTmpSeats.size()>0)
        {
//            AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
//            alt_bld.setTitle("정보가 저장되지 않습니다");
//            alt_bld.setMessage("저장하지 않고 나가시겠습니까?").setCancelable(
//                    false).setPositiveButton("확인",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//
//                            Log.e(TAG, "Delete Seats " + mTmpSeats.size());
//                            for(int i=0; i<mTmpSeats.size(); ++i) {
//                                _dbHelper.mSeat.Delete(mTmpSeats.get(i));
//                            }
//
//                            finish();
//                        }
//                    }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
//                    @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                    }
//            })
//            ;

            //"NCT-Vi"
            AlertDialog.Builder alt_bld = new AlertDialog.Builder(BusSeatServiceActivity.this);
            alt_bld.setTitle("Dữ liệu sẽ không được lưu");
            alt_bld.setMessage("Xác nhận thoát và không lưu dữ liệu?").setCancelable(
                    false).setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            Log.e(TAG, "Delete Seats " + mTmpSeats.size());
                            for(int i=0; i<mTmpSeats.size(); ++i) {
                                _dbHelper.mSeat.Delete(mTmpSeats.get(i));
                            }

                            finish();
                        }
                    }).setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            })
            ;
            AlertDialog alert = alt_bld.create();
            alert.show();

        }
        else
        {
            finish();
        }
    }




    private void updateSeatInfo(List<Object> items)
    {

    }

    @Override
    public void onSeatSelected(int count) {

    }


    protected void onPause() {

        super.onPause();

        Log.e(TAG, "onPause()");

    }

    protected void onStop() {

        super.onStop();

        Log.e(TAG, "onStop()");
    }

    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRefreshBroadcastReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mCautionReceiver);
    }

    private BroadcastReceiver mCautionReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String device_key = intent.getStringExtra(BusSeatServiceClass.REFRESH_DEVICE);
            SeatInfo mTmp = mBusService.getSeatInformation(device_key);
            if(mTmp != null) {
                //mAdapterBusSeat.changeSeatCautionStatus(mTmp.getCautionStatus(), Integer.parseInt(mTmp.getSeatPosition()));
                mAdapterBusSeat.changeSeatCautionStatus(mTmp);
            }
        }
    };


    private BroadcastReceiver mRefreshBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {

            String device_key = intent.getStringExtra(BusSeatServiceClass.REFRESH_DEVICE);
            SeatInfo mTmp = mBusService.getSeatInformation(device_key);

            if(mTmp != null) {
                //Log.e(TAG, "device state: " + mTmp.getSeatNumber()  +  " / " +  mTmp.getSeatPosition() + " / " + mTmp.getSeatStatus()  +" / " + mTmp.getSeatBattery());
                mAdapterBusSeat.changeSeatCautionStatus(mTmp);
                mAdapterBusSeat.changeSeatStatus(mTmp.getSeatStatus(), Integer.parseInt(mTmp.getSeatPosition()));
                mAdapterBusSeat.changeSeatBatteryLv(mTmp.getSeatBattery(), Integer.parseInt(mTmp.getSeatPosition()));

                seatUsed=0; seatEmpty=0; seatLowBattery=0;
                for(int i = 0; i<mDeviceMap.size(); ++i)
                {
                    if(mDeviceMap.get(keyArray[i]).getSeatStatus().equals(bsBusSeatItem.STATUS_OCCUPIED)) {
                        ++seatUsed;
                    }
                    else if(mDeviceMap.get(keyArray[i]).getSeatStatus().equals(bsBusSeatItem.STATUS_EMPTY)) {
                        ++seatEmpty;
                    }
                    else if(mDeviceMap.get(keyArray[i]).getSeatStatus().equals(bsBusSeatItem.STATUS_LOW_BATTERY)) {
                        ++seatLowBattery;
                    }
                }

//                tvSeatUsed.setText(seatUsed+"좌석");
//                tvSeatEmpty.setText(seatEmpty+"좌석");
//                tvSeatLowBattery.setText(seatLowBattery+"좌석");
                //"NCT-Vi"
                tvSeatUsed.setText(seatUsed+" Có trẻ");
                tvSeatEmpty.setText(seatEmpty+" Ghế trống");
                tvSeatLowBattery.setText(seatLowBattery+" Pin yếu");

            }
        }
    };

    

}
