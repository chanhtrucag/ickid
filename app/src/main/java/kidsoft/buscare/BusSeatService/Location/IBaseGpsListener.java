package kidsoft.buscare.BusSeatService.Location;

import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/**
 * Created by DELL3 on 2018-11-27.
 */

public interface IBaseGpsListener extends LocationListener{

    public void onLocationChanged(Location location);

    public void onProviderDisabled(String provider);

    public void onProviderEnabled(String provider);

    public void onStatusChanged(String provider, int status, Bundle extras);

}
