package kidsoft.buscare.BusSeatService;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanSettings;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

import kidsoft.buscare.BTcomm.BTmodule.BluetoothLeDevice;
import kidsoft.buscare.BTcomm.BTmodule.BluetoothLeDeviceStore;
import kidsoft.buscare.BTcomm.BTmodule.BluetoothLeScanner;
import kidsoft.buscare.BTcomm.BTmodule.BluetoothUtils;
import kidsoft.buscare.BTcomm.Beacon;
import kidsoft.buscare.BTcomm.ByteUtils;
import kidsoft.buscare.BTcomm.ParserUtils;

import kidsoft.buscare.Database.models.SeatInfo;
import layout.BusSeatService.bsBusSeatItem;


/**
 * Created by DELL3 on 2018-11-09.
 */

public class BusSeatService extends Service
{

    private BluetoothLeScanner mScanner;
    private BluetoothUtils mBluetoothUtils;
    private BluetoothLeDeviceStore mDeviceStore;

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeAdvertiser mBluetoothLeAdvertiser;


    private static final int PERMISSIONS = 100;
    private static final String HEXES = "0123456789ABCDEF";
    private Vector<Beacon> beacon;
    private ArrayList<BluetoothLeDevice> mBluetoothLeDevice;
    private ScanSettings.Builder mScanSettings;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss.SSS", Locale.KOREAN);

    private HashMap<String, SeatInfo> mDeviceMap;
    private String[] keyArray;

    private final IBinder mBinder = new LocalBinder();
    private final Random mGenerator = new Random();

    public static final String REFRESH_LOCAL_BROADCAST = "kidsoft.buscare.BusSeatService.BusSeatService.REFRESH_LOCAL_BROADCAST";
    public static final String REFRESH_DEVICE = "kidsoft.buscare.BusSeatService.BusSeatService.REFRESH_DEVICE";

    private static final String TAG = "BusSeatService";

    public class LocalBinder extends Binder {

        public void GetDeviceMap(HashMap<String, SeatInfo> mMap) {
            Log.e(TAG, "GetDeviceMap" );
            mDeviceMap = new HashMap();
            mDeviceMap = mMap;
        }

        public BusSeatService getService() {
            Log.e(TAG, "getService()" );

            //setNotification();


            Set<String> keySet = mDeviceMap.keySet();
            keyArray = keySet.toArray(new String[keySet.size()]);

            for(int i = 0; i < keyArray.length ; ++i)
            {
                Log.e(TAG, "device : " + keyArray[i]);
            }

            mScanner.scanLeDevice(-1, true);

            return BusSeatService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG, "onBind" );

        //mDeviceMap = (HashMap<String, SeatInfo>)intent.getSerializableExtra("device_map");

        return mBinder;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.e(TAG, "onStartCommand" );

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.e(TAG, "onCreate()" );



        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBluetoothLeAdvertiser = mBluetoothAdapter.getBluetoothLeAdvertiser();
        mBluetoothLeDevice = new ArrayList<BluetoothLeDevice>();
        mBluetoothLeDevice.clear();
        beacon = new Vector<>();

        ScanFilter.Builder builder = new ScanFilter.Builder();
        ScanFilter filter = builder.build();

        mScanSettings = new ScanSettings.Builder();
        //Vector<ScanFilter> filter = new Vector<ScanFilter>();
        mScanSettings.setScanMode(ScanSettings.SCAN_MODE_BALANCED);
        ScanSettings scanSettings = mScanSettings.build();

        mDeviceStore = new BluetoothLeDeviceStore();
        mDeviceStore.clear();

        mBluetoothUtils = new BluetoothUtils(getApplicationContext());
        mScanner = new BluetoothLeScanner(mLeScanCallback, mBluetoothUtils);



/*
        for(int i = 0; i <mDeviceMap.size(); ++i)
        {
            Log.e(TAG, "device : " + mDeviceMap.keySet());
        }
        */


    }

    private static final int NOTIFICATION_ID = 9999;

    private Handler mDataDownLoadHandler = new Handler();
    private Runnable mDataDownload;
    public void setNotification()
    {
        mDataDownload = new Runnable() {

            int k = 0;

            public void run() {

                Log.e(TAG, "----------------------------------------- count : " + ( ++ k));

                mDataDownLoadHandler.postDelayed(mDataDownload, 1000);
            }
        };


        mDataDownLoadHandler.postDelayed(mDataDownload, 1000);
        /*
        PendingIntent contentIntent;
        contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, BusSeatServiceActivity.class), 0);

        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                        .setContentTitle("BusCare")
                        .setOngoing(true)
                        .setAutoCancel(false)
                        .setContentText("서비스 실행중입니다");
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

        Notification notification = mBuilder.build();
        startForeground(NOTIFICATION_ID, notification);
        */

    }

    private final BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {

            int rawData = ParserUtils.decodeUint16BigEndian(scanRecord, 15); //13->15
            final BluetoothLeDevice deviceLe = new BluetoothLeDevice(device, rssi, scanRecord, System.currentTimeMillis(), rawData);

            if(mDeviceMap.containsKey(deviceLe.getAddress()) == true)
            {
                //--mDeviceStore.addDevice(deviceLe);
                String dataString = ByteUtils.byteArrayToHexString(deviceLe.getScanRecord());
                byte[] tmp = deviceLe.getScanRecord();

                final int bettery =  ParserUtils.decodeUint16BigEndian(tmp, 13);
                final int voltage = ParserUtils.decodeUint16BigEndian(tmp, 15);

                //Log.e(TAG, "Device : " + deviceLe.getAddress().toString() +  " / voltage " + voltage + " / battery " + bettery );

                SeatInfo curSeat =  mDeviceMap.get(deviceLe.getAddress());

                if(voltage < 100)
                {
                    curSeat.setSeatStatus(bsBusSeatItem.STATUS_OCCUPIED);
                }
                else
                {
                    curSeat.setSeatStatus(bsBusSeatItem.STATUS_EMPTY);
                }

                if(bettery < 10)  // 기준 10 -> 3000
                {
                    curSeat.setSeatStatus(bsBusSeatItem.STATUS_LOW_BATTERY);
                }

                curSeat.setSeatBattery(Integer.toString(bettery));

                //Log.e(TAG, "Device : " + mDeviceMap.get(deviceLe.getAddress()).getSeatNumber() +  " / voltage " + mDeviceMap.get(deviceLe.getAddress()).getSeatStatus() + " / battery " + mDeviceMap.get(deviceLe.getAddress()).getSeatBattery() );

                Intent intent = new Intent(REFRESH_LOCAL_BROADCAST);
                intent.putExtra(REFRESH_DEVICE, deviceLe.getAddress());
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

            }
        }
    };

    public SeatInfo getSeatInformation(String device_code)
    {
        if(mDeviceMap.containsKey(device_code) == true)
        {
            return mDeviceMap.get(device_code);
        }
        else
        {
            return null;
        }
    }



    /** method for clients */
    public int getRandomNumber() {

        Log.e(TAG, "TEST" );

        return mGenerator.nextInt(100);
    }



    @Override
    public void onDestroy() {

        Log.e(TAG, "onDestroy() " );
        mScanner.scanLeDevice(-1, false);
        //stopSelf();
    }
}
