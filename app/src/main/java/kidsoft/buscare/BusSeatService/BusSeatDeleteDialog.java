package kidsoft.buscare.BusSeatService;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import kidsoft.buscare.R;

public class BusSeatDeleteDialog extends Dialog {

    private int number;
    private TextView mTitle;
    private Button btOk, btCancel;
    private boolean result;

    public BusSeatDeleteDialog(Context context) {
        super(context);
    }

    public BusSeatDeleteDialog(Context context, int number) {
        super(context);
        this.number = number;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_bus_seat_delete);
        mTitle = findViewById(R.id.bsd_title);
        btCancel = findViewById(R.id.bsd_cancel);
        btOk = findViewById(R.id.bsd_ok);

//        mTitle.setText(number + "개의 좌석을 삭제합니다.");
        //"NCT-Vi"
        mTitle.setText(number + "Xóa chỗ ngồi");

        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = false;
                dismiss();
            }
        });

        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = true;
                dismiss();
            }
        });
    }

    public boolean getResult()
    {
        return result;
    }
}
