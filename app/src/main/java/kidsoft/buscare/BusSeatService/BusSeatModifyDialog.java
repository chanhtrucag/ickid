package kidsoft.buscare.BusSeatService;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import kidsoft.buscare.BaseActivity;
import kidsoft.buscare.BusInfoCamera.BarcodeScanActivity;
import kidsoft.buscare.R;
import layout.BusSeatService.bsBusSeatItem;

import static android.app.Activity.RESULT_OK;
import static kidsoft.buscare.BusInfo.SeatInfoActivity.REQUEST_CODE_SET_SEAT_INFO;

public class BusSeatModifyDialog extends BaseActivity {

    //private BusSeatServiceActivity activity;
    //private bsBusSeatItem mSeat;
    //private Context context;

    private EditText ebSeatNumber;
    private ImageView ibClose;
    private Button btModi, btOk;

    private boolean result;
    String seat_number;
    String seat_position;

    /*
    public BusSeatModifyDialog(@NonNull Context context) {
        //super(context);
    }

    public BusSeatModifyDialog(Context context, bsBusSeatItem mSeat, BusSeatServiceActivity activity) {
        //super(context);

        this.context = context;
        this.mSeat = mSeat;
        this.activity = activity;
    }
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_bus_seat_modify);

        seat_number = getIntent().getStringExtra("seat_number");
        seat_position = getIntent().getStringExtra("seat_position");

        ebSeatNumber = findViewById(R.id.bssm_number_eb);
        ibClose = findViewById(R.id.bssm_close);
        btModi = findViewById(R.id.bssm_button);
        btOk = findViewById(R.id.bssm_ok);

        ebSeatNumber.setText(seat_number);


        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = false;
                finish();
                overridePendingTransition(0,0);
                //dismiss();
            }
        });

        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = true;
                seat_number = ebSeatNumber.getText().toString();
                Intent resultIntent = new Intent();
                resultIntent.putExtra("result_seat", seat_number);
                setResult(RESULT_OK, resultIntent);
                finish();
                overridePendingTransition(0,0);
                //dismiss();
            }
        });

        btModi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), BarcodeScanActivity.class);
                intent.putExtra("seat_position", seat_position);
                startActivityForResult(intent, REQUEST_CODE_SET_SEAT_INFO);

            }
        });

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if((resultCode == RESULT_OK) && (requestCode == REQUEST_CODE_SET_SEAT_INFO))
        {
            if(data.getStringExtra("result_seat").toString().contains("CANCEL_REGI") == false)
            {
                Log.e("BusSeatModifyDialog", "OK" );
                ebSeatNumber.setText(data.getStringExtra("result_seat").toString());
            }
            else {
                Log.e("BusSeatModifyDialog", "Canceled" );
            }
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        overridePendingTransition(0,0);
    }

    public boolean getResult()
    {
        return result;
    }

    public String getNewSeatNumber()
    {
        return seat_number;
    }
}
