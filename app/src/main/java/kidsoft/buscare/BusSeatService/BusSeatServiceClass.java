package kidsoft.buscare.BusSeatService;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;

import android.bluetooth.BluetoothAdapter;

/*
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
*/

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.display.DisplayManager;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import kidsoft.buscare.AlarmWakeLock;
import kidsoft.buscare.BTcomm.ParserUtils;
import kidsoft.buscare.BusList.MainActivity;
import kidsoft.buscare.BusSeatService.Location.CLocation;
import kidsoft.buscare.BusSeatService.Location.IBaseGpsListener;
import kidsoft.buscare.Database.models.SeatInfo;
import kidsoft.buscare.Network.NetworkService;
import kidsoft.buscare.R;
import layout.BusSeatService.bsBusSeatItem;

import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;

import static android.app.Activity.DEFAULT_KEYS_SEARCH_GLOBAL;

/**
 * Created by DELL3 on 2018-11-20.
 */

public class BusSeatServiceClass extends Service implements IBaseGpsListener {

    //-----private BluetoothAdapter mBluetoothAdapter;
    private boolean abnormalEnd = true;
    private HashMap<String, SeatInfo> mServiceDeviceMap;
    private String[] keyArray;
    public String bus_name, bus_code, bus_seat;
    private boolean holdScreenOn = true;
    private boolean checkCarMovement = false;
    private boolean checkFirst = false;
    private boolean flagAlways;
    private SharedPreferences settings;
    private int disconnected_device = 0, cannot_connect_seat = 0;
    private boolean all_device_disconnected = false;

    private IBinder mBinder = new BusSeatServiceClass.LocalBinder();

    public static final String REFRESH_LOCAL_BROADCAST = "kidsoft.buscare.BusSeatService.BusSeatServiceClass.REFRESH_LOCAL_BROADCAST";
    public static final String REFRESH_DEVICE = "kidsoft.buscare.BusSeatService.BusSeatServiceClass.REFRESH_DEVICE";
    public static final String CAUTION_SEAT_BROADCAST = "kidsoft.buscare.BusSeatService.BusSeatServiceClass.CAUTION_SEAT_BROADCAST";

    private static final String TAG = "BusSeatServiceClass";

    private BluetoothLeScannerCompat scanner;

    //private BluetoothAdapter mBluetoothAdapter;
    //private BeaconManager mBeaconManager;

    private LocationManager locationManager;
    private long ScreenOffTime = 0;
    private boolean ScreenOffFlag = false;
    private int CHECK_INTERVAL = 1000; // ms
    private int CHECK_DELAY = 10; // min
    private int SYNC_MAX_DELAY = 30; // sec
    private int MIN_SPEED = 6; // m/s

    private Runnable mSyncCheckProcess;
    private Handler  mSyncCheckHandler;

    private Runnable mDisConnectCheckProcess;
    private Handler  mDisConnectCheckHandler;

    private Handler mDeviceAllDisconnted;
    private float nCurrentSpeed;
    private boolean blockAlert = true;

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            CLocation myLocation = new CLocation(location, true);
            this.updateSpeed(myLocation);
        } else {
            Log.e(TAG, "onLocationChanged: " + "location null");
            WakeUpDevice();
        }

    }

    @Override
    public void onProviderDisabled(String provider) {

        Log.e(TAG, "onProviderDisabled: " + provider);
        if (provider.contains("gps")) {
            GPSmoduleStopMsg(null);
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.e(TAG, "onProviderEnabled: " + provider);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    private GpsStatus.Listener gpsListener = new GpsStatus.Listener() {
        public void onGpsStatusChanged(int event) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            GpsStatus gpsStatus = locationManager.getGpsStatus(null);

            switch (event) {
                case GpsStatus.GPS_EVENT_STARTED:
                    Log.e(TAG, "onGpsStatusChanged: " + "GpsStatus.GPS_EVENT_STARTED");
                    break;
                case GpsStatus.GPS_EVENT_STOPPED:
                    Log.e(TAG, "onGpsStatusChanged: " + "GpsStatus.GPS_EVENT_STOPPED");
                    break;
                case GpsStatus.GPS_EVENT_FIRST_FIX:
                    Log.e(TAG, "onGpsStatusChanged: time to first fix in ms = " + gpsStatus.getTimeToFirstFix());
                    break;
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                    // Do Something with mStatus info
                    int cnt = 0;
                    Iterable sats = gpsStatus.getSatellites();Iterator satI = sats.iterator();
                    while(satI.hasNext()){ GpsSatellite gpssatellite = (GpsSatellite) satI.next();
                        if (gpssatellite.usedInFix()){cnt++;}}

                    if(isScreenOn(getApplicationContext()) == false) {
                        if(cnt==0) {
                            WakeUpDevice();
                        }Log.e(TAG, "onGpsStatusChanged: " + "GpsStatus.GPS_EVENT_SATELLITE_STATUS / " + cnt + "/" + gpsStatus.getMaxSatellites());
                    }

                    if(cnt == 0) {blockAlert= true;}
                    else {blockAlert = false;}

                    break;
            }

        }
    };
    public void setLocationService() {
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }

        /*
        if (locationManager.isProviderEnabled(locationManager.NETWORK_PROVIDER) == true) {
            Log.e(TAG, "isProviderEnabled : " + locationManager.isProviderEnabled(locationManager.NETWORK_PROVIDER));
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
        }
        */

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
        locationManager.addGpsStatusListener(gpsListener);


        this.updateSpeed(null);
    }

    private void updateSpeed(CLocation location) {

        nCurrentSpeed = 0;

        if (location != null) {
            location.setUseMetricunits(true);
            nCurrentSpeed = location.getSpeed();
        }

        Formatter fmt = new Formatter(new StringBuilder());
        fmt.format(Locale.US, "%5.1f", nCurrentSpeed);
        String strCurrentSpeed = fmt.toString();
        strCurrentSpeed = strCurrentSpeed.replace(' ', '0');
        //Log.e(TAG, "Speed : " + strCurrentSpeed + " m/s");

        //nCurrentSpeed = 10;

        if(nCurrentSpeed < MIN_SPEED) {
            checkCarMovement = false; //test -> false
        }
        else {
            checkCarMovement = true;
        }


        if (nCurrentSpeed < MIN_SPEED) {
            if (sCpuWakeLock != null) {

                if (sCpuWakeLock.isHeld() == true) {
                    sCpuWakeLock.release();
                }
                sCpuWakeLock = null;
                sCpuWakeLock = pm.newWakeLock(
                        PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                                PowerManager.ACQUIRE_CAUSES_WAKEUP |
                                PowerManager.ON_AFTER_RELEASE, "BusSeatService");
                sCpuWakeLock.acquire();

                //Log.e(TAG, "sCpuWakeLock.isHeld() " + sCpuWakeLock.isHeld());
                Log.e(TAG, "Speed Under: " + strCurrentSpeed + " m/s");
            }
        } else {
            if (sCpuWakeLock != null) {
                Log.e(TAG, "go sCpuWakeLock.isHeld() " + sCpuWakeLock.isHeld());
                if (sCpuWakeLock.isHeld() == true) {
                    sCpuWakeLock.release();
                }

                //Log.e(TAG, "sCpuWakeLock.isHeld() " + sCpuWakeLock.isHeld());
                Log.e(TAG, "Speed Over: " + strCurrentSpeed + " m/s");
            }
        }


        //Log.e(TAG, "sCpuWakeLock.isHeld() " + sCpuWakeLock.isHeld());
    }

 /*
    private boolean checkBTenabled() {

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return mBluetoothAdapter.isEnabled();

    } */

    private boolean checkGPSenabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    public class LocalBinder extends Binder {

        private BusSeatServiceActivity activity;


        public void GetDeviceMap(HashMap<String, SeatInfo> mMap, BusSeatServiceActivity activity, String bus_name1, String bus_code1, String bus_seat1) {

            Log.e(TAG, "GetDeviceMap");
            mServiceDeviceMap = new HashMap<>();
            mServiceDeviceMap.clear();
            mServiceDeviceMap = mMap;
            this.activity = activity;

            bus_name = bus_name1;
            bus_code = bus_code1;
            bus_seat = bus_seat1;

            //-- Net  mNetworkService = new NetworkService(activity.getApplicationContext());

        }

        public BusSeatServiceClass getService() {

            startForeground();

            settings = getSharedPreferences("settings", MODE_PRIVATE);
            flagAlways = settings.getBoolean("DisplayAlwaysOn", false);

            Log.e(TAG, "getService() " + flagAlways);

            Set<String> keySet = mServiceDeviceMap.keySet();
            keyArray = keySet.toArray(new String[keySet.size()]);

            for (int i = 0; i < keyArray.length; ++i) {
                Log.e(TAG, "device : " + keyArray[i]);
            }

            mDeviceAllDisconnted = new Handler();
            mDisConnectCheckHandler = new Handler();
            mDisConnectCheckProcess = new Runnable() {
                @Override
                public void run() {

                    if (isScreenOn(getApplicationContext()) == true) {

                        disconnected_device = 0;
                        cannot_connect_seat = 0;
                        for (int i = 0; i < mServiceDeviceMap.size(); ++i) {
                            if (mServiceDeviceMap.get(keyArray[i]).getSyncTime() != null) {
                                //Log.e(TAG, "isScreenOn " + isScreenOn(getApplicationContext()));
                                //if(isScreenOn(getApplicationContext()) == true)
                                //{
                                if (mServiceDeviceMap.get(keyArray[i]).getSync() == true) {
                                    Date beforeSyncTime = mServiceDeviceMap.get(keyArray[i]).getSyncTime();

                                    long diff = ((new Date().getTime() - beforeSyncTime.getTime()) / 1000) - ScreenOffTime;

                                    SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");

                                    Log.e(TAG, "SyncCheck diff : " + keyArray[i] + " - " + formatter.format(new Date().getTime()) + " | " + formatter.format(beforeSyncTime.getTime()));
                                    Log.e(TAG, "SyncCheck diff : " + (( new Date().getTime() - beforeSyncTime.getTime()) / 1000) +  " / " + ScreenOffTime + " | " + diff);
                                    //Log.e(TAG, "SyncCheckProcess diff " + keyArray[i] + " | " + diff);

                                    // alert_msg 잔류 착석자 확인 알람
                                    if (diff > SYNC_MAX_DELAY) {
                                        mServiceDeviceMap.get(keyArray[i]).setSync(false);

                                        if (mServiceDeviceMap.get(keyArray[i]).getSeatStatus() == bsBusSeatItem.STATUS_OCCUPIED) {

                                            mServiceDeviceMap.get(keyArray[i]).setCautionStatus(true);
                                            CautionSeatStatus(mServiceDeviceMap.get(keyArray[i]));
                                            AlertMsg(mServiceDeviceMap.get(keyArray[i]));
                                        }
                                    }
                                }
                            }

                            if(mServiceDeviceMap.get(keyArray[i]).getSync() == false && (mServiceDeviceMap.get(keyArray[i]).getSeatStatus() == bsBusSeatItem.STATUS_EMPTY))
                            {
                                ++disconnected_device;
                            }

                            if(mServiceDeviceMap.get(keyArray[i]).getSeatStatus().equals(bsBusSeatItem.STATUS_ERROR))
                            {
                                ++cannot_connect_seat;
                            }
                        }

                        Log.e(TAG, "keyArray.length  : " + keyArray.length);
                        Log.e(TAG, "cannot_connect_seat : " + cannot_connect_seat);
                        Log.e(TAG, "disconnected_device : " + disconnected_device);


                        if((keyArray.length - cannot_connect_seat) == disconnected_device) {
                            if(all_device_disconnected == false) {
                                all_device_disconnected = true;
                                DeviceAlldisconnected();
                            }
                        }

                        if (ScreenOffFlag == true) {
                            CHECK_INTERVAL = 12000;
                        } else {
                            CHECK_INTERVAL = 1000;
                        }

                        ScreenOffFlag = false;
                        ScreenOffTime = 0;
                    } else {

                        if(ScreenOffFlag == false)
                        {
                            //ScreenOffTime = 0;
                            CHECK_INTERVAL = 1000;
                            ScreenOffFlag = true;

                        }
                        else {
                            ScreenOffTime += 1;
                        }

                        Log.e(TAG, "ScreenOffTime " + ScreenOffTime);
                    }

                    for (int i = 0; i < mServiceDeviceMap.size(); ++i) {
                        //-- Net  mNetworkService.updateSeatInfo(mServiceDeviceMap.get(keyArray[i]), "test");
                    }

                    mDisConnectCheckHandler.postDelayed(this, CHECK_INTERVAL);
                }
            };





            mSyncCheckHandler = new Handler();
            mSyncCheckProcess = new Runnable() {
                @Override
                public void run() {

                    // Log.e(TAG, "SyncCheckProcess");

                    /* test ---
                    if (isScreenOn(getApplicationContext()) == true)
                    */
                    {

                        /* test ---
                        disconnected_device = 0;
                        cannot_connect_seat = 0;
                        */

                        for (int i = 0; i < mServiceDeviceMap.size(); ++i) {
                            if (mServiceDeviceMap.get(keyArray[i]).getSyncTime() != null)
                            {
                                //Log.e(TAG, "isScreenOn " + isScreenOn(getApplicationContext()));
                                //if(isScreenOn(getApplicationContext()) == true)
                                //{

                                /* test ---
                                if (mServiceDeviceMap.get(keyArray[i]).getSync() == true) {
                                    Date beforeSyncTime = mServiceDeviceMap.get(keyArray[i]).getSyncTime();

                                    long diff = ((new Date().getTime() - beforeSyncTime.getTime()) / 1000) - ScreenOffTime;

                                    Log.e(TAG, "SyncCheckProcess diff " + (( new Date().getTime() - beforeSyncTime.getTime()) / 1000) +  " / " + ScreenOffTime);
                                    Log.e(TAG, "SyncCheckProcess diff " + keyArray[i] + " | " + diff);

                                    // alert_msg 잔류 착석자 확인 알람
                                    if (diff > 30) {
                                        mServiceDeviceMap.get(keyArray[i]).setSync(false);
                                        if (mServiceDeviceMap.get(keyArray[i]).getSeatStatus() == bsBusSeatItem.STATUS_OCCUPIED) {

                                            mServiceDeviceMap.get(keyArray[i]).setCautionStatus(true);
                                            CautionSeatStatus(mServiceDeviceMap.get(keyArray[i]));
                                            AlertMsg(activity);
                                        }
                                    }
                                }
                                */
                            }
                            else
                            {
                                // Log.e(TAG, "CantSyncMsg(activity)");
                                // alert_msg 연결 이상 좌석 확인 알람
                                if (mServiceDeviceMap.get(keyArray[i]).getSync() == false)
                                {
                                    Log.e(TAG, "CantSyncMsg(activity) " + keyArray[i]);
                                    mServiceDeviceMap.get(keyArray[i]).setSync(true);
                                    mServiceDeviceMap.get(keyArray[i]).setCautionStatus(true);
                                    mServiceDeviceMap.get(keyArray[i]).setSeatStatus(bsBusSeatItem.STATUS_ERROR);
                                    mServiceDeviceMap.get(keyArray[i]).setErrorStatus(true);

                                    CautionSeatStatus(mServiceDeviceMap.get(keyArray[i]));
                                    CantSyncMsg(activity);
                                }
                            }

                            /* test ---
                            if(mServiceDeviceMap.get(keyArray[i]).getSync() == false && (mServiceDeviceMap.get(keyArray[i]).getSeatStatus() == bsBusSeatItem.STATUS_EMPTY))
                            {
                                ++disconnected_device;
                            }

                            if(mServiceDeviceMap.get(keyArray[i]).getSeatStatus().equals(bsBusSeatItem.STATUS_ERROR))
                            {
                                ++cannot_connect_seat;
                            }
                            */
                        }


                        /* test ---
                        Log.e(TAG, "keyArray.length  : " + keyArray.length);
                        Log.e(TAG, "cannot_connect_seat : " + cannot_connect_seat);
                        Log.e(TAG, "disconnected_device : " + disconnected_device);


                        if((keyArray.length - cannot_connect_seat) == disconnected_device) {
                            if(all_device_disconnected == false) {
                                all_device_disconnected = true;
                                DeviceAlldisconnected();
                            }
                        }

                        */

                        /* test ---
                        if (ScreenOffFlag == true) {
                            CHECK_INTERVAL = 10000;
                        } else {
                            CHECK_INTERVAL = 1000;
                        }

                        ScreenOffFlag = false;
                        ScreenOffTime = 0;
                        */
                    }
                    /* test ---

                    else {

                        if(ScreenOffFlag == false)
                        {
                            //ScreenOffTime = 0;
                            CHECK_INTERVAL = 5000;
                            ScreenOffFlag = true;

                        }
                        else {
                            ScreenOffTime += 5;
                        }

                        Log.e(TAG, "ScreenOffTime " + ScreenOffTime);
                    }

                    mSyncCheckHandler.postDelayed(this, CHECK_INTERVAL);
                    */

                }
            };

            if(mServiceDeviceMap.size() > 0)
            {
                mSyncCheckHandler.postDelayed(mSyncCheckProcess, CHECK_DELAY * 60000);
                mDisConnectCheckHandler.postDelayed(mDisConnectCheckProcess, 1000);
            }


            new Handler().postDelayed(new Runnable()//do
            {
                public void run() {
                  holdScreenOn = false;
                    Log.e(TAG, "holdScreenOn " + holdScreenOn);
                }
            }, CHECK_DELAY * 60000);

            oldMethod();
            return BusSeatServiceClass.this;
        }
    }




    public boolean isScreenOn(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            DisplayManager dm = (DisplayManager) context.getSystemService(Context.DISPLAY_SERVICE);
            boolean screenOn = false;
            for (Display display : dm.getDisplays()) {
                if (display.getState() != Display.STATE_OFF) {
                    screenOn = true;
                }
            }
            return screenOn;
        } else {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            //noinspection deprecation
            return pm.isScreenOn();
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {

        //Log.e(TAG, "onUnbind()");

        return false;
    }

    private void checkStatus()
    {

    }

    private void BTmoduleStopMsg(String msg)
    {
        WakeUpDevice();
        Log.e(TAG, "BTmoduleStopMsg");

//        Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        notificationIntent.putExtra("msg", "운행중에는 반드시 블루투스를 작동시켜 주시기 바랍니다.");
//        notificationIntent.putExtra("code", "bluetooth");
//        notificationIntent.putExtra("bus_name", bus_name);
//        notificationIntent.putExtra("bus_code", bus_code);
//        notificationIntent.putExtra("bus_seat", bus_seat);
//        this.startActivity(notificationIntent);
        //"NCT-Vi"
        Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("msg", "Hãy đảm bảo kết nối Bluetooth luôn được mở trong suốt quá trình sử dụng.");
        notificationIntent.putExtra("code", "bluetooth");
        notificationIntent.putExtra("bus_name", bus_name);
        notificationIntent.putExtra("bus_code", bus_code);
        notificationIntent.putExtra("bus_seat", bus_seat);
        this.startActivity(notificationIntent);

    }

    private void GPSmoduleStopMsg(String msg)
    {
        WakeUpDevice();
        Log.e(TAG, "GPSmoduleStopMsg");

//        Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        notificationIntent.putExtra("msg", "운행중에는 반드시 위치서비스를 작동시켜 주시기 바랍니다.");
//        notificationIntent.putExtra("code", "location");
//        notificationIntent.putExtra("bus_name", bus_name);
//        notificationIntent.putExtra("bus_code", bus_code);
//        notificationIntent.putExtra("bus_seat", bus_seat);
//        this.startActivity(notificationIntent);
        //"NCT-Vi"
        Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("msg", "Hãy đảm bảo Định vị luôn được mở trong suốt quá trình sử dụng.");
        notificationIntent.putExtra("code", "location");
        notificationIntent.putExtra("bus_name", bus_name);
        notificationIntent.putExtra("bus_code", bus_code);
        notificationIntent.putExtra("bus_seat", bus_seat);
        this.startActivity(notificationIntent);

    }

    private void AlertSeatMoved(SeatInfo alertSeat)
    {
        WakeUpDevice();

        Log.e(TAG, "AlertSeatMoved" + alertSeat.getSeatPosition());

        int COLUMNS, number_of_seat;

        if(bus_seat.equals(BusSeatServiceActivity.MINI_BUS)) {
            COLUMNS = 3;
            number_of_seat = 15;
        }else if(bus_seat.equals(BusSeatServiceActivity.SMALL_BUS)) {
            COLUMNS = 4;
            number_of_seat = 24;
        }else if(bus_seat.equals(BusSeatServiceActivity.MEDIUM_BUS)) {
            COLUMNS = 5;
            number_of_seat = 45;
        }else if(bus_seat.equals(BusSeatServiceActivity.ETC_BUS)) {
            COLUMNS = 5;
            number_of_seat = 55;
        }else {
            COLUMNS = 5;
            number_of_seat = 55;
        }

        int alert_column =  ((Integer.parseInt(alertSeat.getSeatPosition())) / COLUMNS)  + 1;
        int alert_row    =  ((Integer.parseInt(alertSeat.getSeatPosition())) % COLUMNS)  + 1 ;

//        Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        notificationIntent.putExtra("msg", "차량 " + bus_name + " "  +  alert_column +"열 " + alert_row +"번째 "  +     "좌석에서 이동이 발생했습니다. 직접 확인해주세요!");
//        notificationIntent.putExtra("code", "move");
//        notificationIntent.putExtra("bus_name", bus_name);
//        notificationIntent.putExtra("bus_code", bus_code);
//        notificationIntent.putExtra("bus_seat", bus_seat);
//        this.startActivity(notificationIntent);
        //"NCT-Vi"
        Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("msg", "Xe " + bus_name + " "  +  alert_column +"Hàng " + alert_row +"Cột "  +     "Phát hiện có sự di chuyển ở ghế. Vui lòng kiểm tra lại.");
        notificationIntent.putExtra("code", "move");
        notificationIntent.putExtra("bus_name", bus_name);
        notificationIntent.putExtra("bus_code", bus_code);
        notificationIntent.putExtra("bus_seat", bus_seat);
        this.startActivity(notificationIntent);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        PendingIntent contentIntent;
        contentIntent = PendingIntent.getActivity(this, 998880, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        final NotificationManager mNotificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder  builder = null;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("ICHAK_NOTIFICATION_ID", "iChak Notifications", importance);
            mNotificationManager.createNotificationChannel(notificationChannel);
            builder = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext());
        }

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
//        bigTextStyle.bigText("차량 " + bus_name + " "  +  alert_column +"열 " + alert_row +"번째 "  +     "좌석에서 일어난 아이가 있습니다!. 직접 확인해주세요!");
        //"NCT-Vi"
        bigTextStyle.bigText("Xe " + bus_name + " "  +  alert_column +"Hàng " + alert_row +"Cột "  +     "Phát hiện có trẻ đứng lên khỏi ghế!. Vui lòng kiểm tra lại!.");

//        builder =  builder
//                .setSmallIcon(R.drawable.icon_bus_care)
//                .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
//                .setContentTitle("좌석을 확인해주세요")
//                .setContentIntent(contentIntent)
//                .setOngoing(false)
//                .setAutoCancel(true)
//                .setPriority(NotificationManager.IMPORTANCE_HIGH)
//                .setStyle(bigTextStyle)
//                .setVibrate(new long[] {1000, 1000})
//                .setSound(alarmSound)
//                .setContentText("차량 " + bus_name + " "  +  alert_column +"열 " + alert_row +"번째 "  +     "좌석에서 일어난 아이가 있습니다!. 직접 확인해주세요!").setStyle(bigTextStyle);
        //"NCT-Vi"
        builder =  builder
                .setSmallIcon(R.drawable.icon_bus_care)
                .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                .setContentTitle("Vui lòng kiểm tra lại ghế")
                .setContentIntent(contentIntent)
                .setOngoing(false)
                .setAutoCancel(true)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setStyle(bigTextStyle)
                .setVibrate(new long[] {1000, 1000})
                .setSound(alarmSound)
                .setContentText("Xe " + bus_name + " "  +  alert_column +"Hàng " + alert_row +"Cột "  +     "Phát hiện có trẻ đứng lên khỏi ghế. Vui lòng kiểm tra lại.").setStyle(bigTextStyle);

        builder.setContentIntent(contentIntent);
        mNotificationManager.notify(998880, builder.build());



    }

    private void DeviceAlldisconnected()
    {
        Log.e(TAG, "DeviceAlldisconnected()");

        int cnt = 0;
        for(int i =0; i < mServiceDeviceMap.size(); ++i)
        {
            if(mServiceDeviceMap.get(keyArray[i]).getSeatUser() == true) {
                ++cnt;
            }
        }

        if(cnt > 0) {
            mDeviceAllDisconnted.postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (all_device_disconnected == true) {
                        DeviceAllDisconnectedMsg();
                    } else {
                        Log.e(TAG, "Device Re-connected");
                    }
                }
            }, 10 * 60000);

        }
    }

    private void DeviceAllDisconnectedMsg()
    {
        WakeUpDevice();
        Log.e(TAG, "DeviceAlldisconnected");
        Intent notificationIntent = new Intent(this, BusSeatServiceActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("bus_name", bus_name);
        notificationIntent.putExtra("bus_code", bus_code);
        notificationIntent.putExtra("bus_seat", bus_seat);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        PendingIntent contentIntent;
        contentIntent = PendingIntent.getActivity(this, 998880, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        final NotificationManager mNotificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder  builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("ICHAK_NOTIFICATION_ID", "iChak Notifications", importance);
            mNotificationManager.createNotificationChannel(notificationChannel);
            builder = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext());
        }

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
//        bigTextStyle.bigText("좌석에 사람이 없습니다. 운행이 끝났다면, 종료해주세요.");
        //"NCT-Vi"
        bigTextStyle.bigText("Không có trẻ ngồi trên ghế. Nếu đã kết thúc chuyến, vui lòng thoát ứng dụng.");


//        builder =  builder
//                .setSmallIcon(R.drawable.icon_bus_care)
//                .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
//                .setContentTitle("운행을 종료하시겠습니까?")
//                .setContentIntent(contentIntent)
//                .setOngoing(false)
//                .setVibrate(new long[] {1000, 1000})
//                .setSound(alarmSound)
//                .setAutoCancel(true)
//                .setPriority(NotificationManager.IMPORTANCE_HIGH)
//                .setContentText("좌석에 사람이 없습니다. 운행이 끝났다면, 종료해주세요.").setStyle(bigTextStyle);
        //"NCT-Vi"
        builder =  builder
                .setSmallIcon(R.drawable.icon_bus_care)
                .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                .setContentTitle("Xác nhận kết thúc chuyến?")
                .setContentIntent(contentIntent)
                .setOngoing(false)
                .setVibrate(new long[] {1000, 1000})
                .setSound(alarmSound)
                .setAutoCancel(true)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setContentText("Không có trẻ ngồi trên ghế. Nếu đã kết thúc chuyến, vui lòng thoát ứng dụng.").setStyle(bigTextStyle);

        builder.setContentIntent(contentIntent);
        mNotificationManager.notify(998880, builder.build());
    }

    private void CantSyncMsg(Activity activity)
    {
        WakeUpDevice();

//        Log.e(TAG, "CantSyncMsg");
//        Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        notificationIntent.putExtra("msg", "차량 " + bus_name + " 에 상태를 알 수 없는 방석이 있습니다. 직접 확인해주세요!");
//        notificationIntent.putExtra("code", "unknown");
//        notificationIntent.putExtra("bus_name", bus_name);
//        notificationIntent.putExtra("bus_code", bus_code);
//        notificationIntent.putExtra("bus_seat", bus_seat);
//        this.startActivity(notificationIntent);
        //"NCT-Vi"
        Log.e(TAG, "CantSyncMsg");
        Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("msg", "Xe " + bus_name + " Có nệm ngồi hiện không xác định được trạng thái. Vui lòng kiểm tra lại.");
        notificationIntent.putExtra("code", "unknown");
        notificationIntent.putExtra("bus_name", bus_name);
        notificationIntent.putExtra("bus_code", bus_code);
        notificationIntent.putExtra("bus_seat", bus_seat);
        this.startActivity(notificationIntent);

        PendingIntent contentIntent;
        contentIntent = PendingIntent.getActivity(this, 998879, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        final NotificationManager mNotificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder  builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("ICHAK_NOTIFICATION_ID", "iChak Notifications", importance);
            mNotificationManager.createNotificationChannel(notificationChannel);
            builder = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext());
        }

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
//        bigTextStyle.bigText("차량 " + bus_name + " 에 상태를 알 수 없는 방석이 있습니다. 직접 확인해주세요!");
        //"NCT-Vi"
        bigTextStyle.bigText("Xe " + bus_name + " Có nệm ngồi hiện không xác định được trạng thái. Vui lòng kiểm tra lại.");

//        builder =  builder
//                        .setSmallIcon(R.drawable.icon_bus_care)
//                        .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
//                        .setContentTitle("방석을 확인해주세요")
//                        .setContentIntent(contentIntent)
//                        .setOngoing(false)
//                        .setAutoCancel(true)
//                        .setPriority(NotificationManager.IMPORTANCE_HIGH)
//                        .setContentText("차량 " + bus_name + " 에 상태를 알 수 없는 방석이 있습니다. 직접 확인해주세요!").setStyle(bigTextStyle);
        //"NCT-Vi"
        builder =  builder
                .setSmallIcon(R.drawable.icon_bus_care)
                .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                .setContentTitle("Kiểm tra lại nệm ngồi")
                .setContentIntent(contentIntent)
                .setOngoing(false)
                .setAutoCancel(true)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setContentText("Xe " + bus_name + " Có nệm ngồi hiện không xác định được trạng thái. Vui lòng kiểm tra lại.").setStyle(bigTextStyle);

        builder.setContentIntent(contentIntent);
        mNotificationManager.notify(998879, builder.build());

    }

    private void LowBatteryMsg()
    {
        Log.e(TAG, "LowBatteryMsg");

        WakeUpDevice();

//        Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        notificationIntent.putExtra("msg", "차량 " + bus_name + " 에 배터리가 부족한 방석이 있습니다. 직접 확인해주세요!");
//        notificationIntent.putExtra("code", "battery");
//        notificationIntent.putExtra("bus_name", bus_name);
//        notificationIntent.putExtra("bus_code", bus_code);
//        notificationIntent.putExtra("bus_seat", bus_seat);
//        this.startActivity(notificationIntent);
        //"NCT-Vi"

        Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("msg", "Xe " + bus_name + " Phát hiện có nệm ngồi ở trạng thái pin yếu. Vui lòng kiểm tra lại.");
        notificationIntent.putExtra("code", "battery");
        notificationIntent.putExtra("bus_name", bus_name);
        notificationIntent.putExtra("bus_code", bus_code);
        notificationIntent.putExtra("bus_seat", bus_seat);
        this.startActivity(notificationIntent);

        PendingIntent contentIntent;
        contentIntent = PendingIntent.getActivity(this, 998878, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        final NotificationManager mNotificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder  builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("ICHAK_NOTIFICATION_ID", "iChak Notifications", importance);
            mNotificationManager.createNotificationChannel(notificationChannel);
            builder = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext());
        }
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
//        bigTextStyle.bigText("차량 " + bus_name + " 에 배터리가 부족한 방석이 있습니다. 직접 확인해주세요!");
        //"NCT-Vi"
        bigTextStyle.bigText("Xe " + bus_name + " Phát hiện có nệm ngồi ở trạng thái pin yếu. Vui lòng kiểm tra lại.");

//        builder =  builder
//                        .setSmallIcon(R.drawable.icon_bus_care)
//                        .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
//                        .setContentTitle("방석을 확인해주세요")
//                        .setContentIntent(contentIntent)
//                        .setOngoing(false)
//                        .setAutoCancel(true)
//                        .setPriority(NotificationManager.IMPORTANCE_HIGH)
//                        .setContentText("차량 " + bus_name + " 에 배터리가 부족한 방석이 있습니다. 직접 확인해주세요!").setStyle(bigTextStyle);
        //"NCT-Vi"

        builder =  builder
                .setSmallIcon(R.drawable.icon_bus_care)
                .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                .setContentTitle("Kiểm tra lại nệm ngồi")
                .setContentIntent(contentIntent)
                .setOngoing(false)
                .setAutoCancel(true)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setContentText("Xe " + bus_name + " Phát hiện có nệm ngồi ở trạng thái pin yếu. Vui lòng kiểm tra lại.").setStyle(bigTextStyle);


        builder.setContentIntent(contentIntent);
        mNotificationManager.notify(998878, builder.build());
    }


    private void AlertMsg(SeatInfo alertSeat)
    {

        WakeUpDevice();

        Log.e(TAG, "AlertMsg");

//        Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        notificationIntent.putExtra("msg", "차량 " + bus_name + " 에 남아있는 아이가 있습니다. 다시 차량을 확인해주세요!");
//        notificationIntent.putExtra("code", "remain");
//        notificationIntent.putExtra("bus_name", bus_name);
//        notificationIntent.putExtra("bus_code", bus_code);
//        notificationIntent.putExtra("bus_seat", bus_seat);
//        this.startActivity(notificationIntent);

        //"NCT-Vi"
        Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("msg", "Xe " + bus_name + " Phát hiện có trẻ còn trên xe. Vui lòng kiểm tra lại xe.");
        notificationIntent.putExtra("code", "remain");
        notificationIntent.putExtra("bus_name", bus_name);
        notificationIntent.putExtra("bus_code", bus_code);
        notificationIntent.putExtra("bus_seat", bus_seat);
        this.startActivity(notificationIntent);

        PendingIntent contentIntent;
        contentIntent = PendingIntent.getActivity(this, 998877, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        final NotificationManager mNotificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
        //NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder  builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("ICHAK_NOTIFICATION_ID", "iChak Notifications", importance);
            mNotificationManager.createNotificationChannel(notificationChannel);
            builder = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext());
        }

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
//        bigTextStyle.bigText("차량 " + bus_name + " 에 남아있는 아이가 있습니다. 다시 차량을 확인해주세요!");
        //"NCT-Vi"
        bigTextStyle.bigText("Xe " + bus_name + " Phát hiện có trẻ còn trên xe. Vui lòng kiểm tra lại xe.");

//        builder =  builder
//                        .setSmallIcon(R.drawable.icon_bus_care)
//                        .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
//                        .setContentTitle("좌석을 확인해주세요")
//                        .setContentIntent(contentIntent)
//                        .setOngoing(false)
//                        .setAutoCancel(true)
//                        .setPriority(NotificationManager.IMPORTANCE_HIGH)
//                        .setContentText("차량 " + bus_name + " 에 남아있는 아이가 있습니다. 다시 차량을 확인해주세요!").setStyle(bigTextStyle);
        //"NCT-Vi"
        builder =  builder
                .setSmallIcon(R.drawable.icon_bus_care)
                .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                .setContentTitle("Vui lòng kiểm tra lại ghế")
                .setContentIntent(contentIntent)
                .setOngoing(false)
                .setAutoCancel(true)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setContentText("Xe " + bus_name + " Phát hiện có trẻ còn trên xe. Vui lòng kiểm tra lại xe.").setStyle(bigTextStyle);

        builder.setContentIntent(contentIntent);
        mNotificationManager.notify(998877, builder.build());

        /*
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(activity);
        alt_bld.setTitle("좌석 확인 필요");
        alt_bld.setMessage("아직 자리에 남아있는 아이가 있습니다. 확인해주세요!").setCancelable(
                false).setPositiveButton("확인",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        vib.cancel();
                    }
                });
        AlertDialog alert = alt_bld.create();
        alert.show();
        */
    }

    public void oldMethod()
    {

        ScanFilter.Builder builder = new ScanFilter.Builder();
            //String serviceUuidString = "0000feaa-0000-1000-8000-00805f9b34fb";
        String serviceUuidString = "0000feaa-0000-1000-8000-00805f9b34fb";
            //builder.setManufacturerData(0x00E0, new byte[] {});
            // 0000feaa-0000-1000-8000-00805f9b34fb
        String serviceUuidMaskString = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
        ParcelUuid parcelUuid = ParcelUuid.fromString(serviceUuidString);
        ParcelUuid parcelUuidMask = ParcelUuid.fromString(serviceUuidMaskString);
        builder.setServiceUuid(parcelUuid, parcelUuidMask);

        scanner = BluetoothLeScannerCompat.getScanner();
        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .setReportDelay(100)
                .setUseHardwareBatchingIfSupported(false).build();

        //-List<ScanFilter> filters = new ArrayList<>();
        List<ScanFilter> filters = Collections.singletonList(builder.build());

        //-filters.add(builder.build());

        //final List<ScanFilter> filters = Collections.singletonList(new Builder().build());
        scanner.startScan(filters, settings, scanCallback);

    }



    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);

            if(result.getDevice().getName() != null)
            {
                if(result.getDevice().getName().equals("SLCP") == true)
                {
                    UpdateSeatInformation(result);
                }
            }
        }

        @Override
        public void onBatchScanResults(final List<ScanResult> results) {
            //Log.e(TAG, "onBatchScanResults: " + results.size());
           for(int i = 0; i<results.size(); ++i)
           {
               //Log.e(TAG, "onBatchScanResults : " + results.get(i).getDevice().getAddress()  +   " / "  +  results.get(i).getDevice().getName() + " / " + results.get(i).getRssi());
               if(results.get(i).getDevice().getName() != null)
               {
                   if(results.get(i).getDevice().getName().equals("SLCP") == true)
                   {
                       UpdateSeatInformation(results.get(i));
                   }
               }
           }
        }

        @Override
        public void onScanFailed(final int errorCode) {
            // should never be called
            Log.e(TAG, "onScanFailed: " + errorCode);
        }
    };


    private void startForeground()
    {

        /*Notification notification = new Notification(R.drawable.ic_launcher_foreground, "서비스 실행중", System.currentTimeMillis());
        Intent notificationIntent = new Intent(this, BusSeatService.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        notification.setLatestEventInfo(this, getText(R.string.notification_title),
                getText(R.string.notification_message), pendingIntent);
        startForeground(9876, notification);
        */

        Intent notificationIntent = new Intent(this, BusSeatServiceActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent;
        //contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, BusSeatServiceClass.class), 0);
        contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder  builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("ICHAK_NOTIFICATION_ID", "iChak Notifications", importance);
            mNotificationManager.createNotificationChannel(notificationChannel);
            builder = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext());
        }

//        builder =  builder
//                .setSmallIcon(R.drawable.icon_bus_care)
//                .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
//                .setContentTitle("좌석 상태 확인")
//                .setContentIntent(contentIntent)
//                .setOngoing(true)
//                .setAutoCancel(false)
//                .setContentText("서비스 실행중입니다.");
//        builder.setContentIntent(contentIntent);
//        mNotificationManager.notify(656555,  builder.build());
        //"NCT-Vi"
        builder =  builder
                .setSmallIcon(R.drawable.icon_bus_care)
                .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                .setContentTitle("Kiểm tra trạng thái ghế")
                .setContentIntent(contentIntent)
                .setOngoing(true)
                .setAutoCancel(false)
                .setContentText("Chuyến xe đang được mở.");
        builder.setContentIntent(contentIntent);
        mNotificationManager.notify(656555,  builder.build());

        Notification notification =  builder.build();
        startForeground(656555, notification);


        /*
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("ID", "Name", importance);
            notificationManager.createNotificationChannel(notificationChannel);
            builder = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext());
        }

        builder = builder
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setColor(ContextCompat.getColor(context, R.color.color))
                .setContentTitle(context.getString(R.string.getTitel))
                .setTicker(context.getString(R.string.text))
                .setContentText(message)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true);
        notificationManager.notify(requestCode, builder.build());
        */

    }



    private void UpdateSeatInformation(ScanResult result)
    {

        byte[] tmp = result.getScanRecord().getBytes();
        final String MACaddress = ParserUtils.bytesToHex(tmp);
        //Log.e(TAG, "mac address : " + MACaddress);

        if(mServiceDeviceMap.containsKey(result.getDevice().getAddress()) == true || mServiceDeviceMap.containsKey(MACaddress) == true)
        {
            String deviceKey = null;
            if(mServiceDeviceMap.containsKey(result.getDevice().getAddress()) == true){
                deviceKey = result.getDevice().getAddress();
            }
            else if(mServiceDeviceMap.containsKey(MACaddress) == true) {
                deviceKey = MACaddress;
            }

            if(all_device_disconnected == true) {
                all_device_disconnected = false;
            }

            //Log.e(TAG, "UpdateSeatInformation : " + deviceKey  +   " / "  +  result.getDevice().getName() + " / " + result.getRssi());

            int bettery =  ParserUtils.decodeUint16BigEndian(tmp, 13);
            final int voltage = ParserUtils.decodeInt16BigEndian(tmp, 15);
            final int rssi = result.getRssi();

            SeatInfo curSeat =  mServiceDeviceMap.get(deviceKey);

            /*
            if(curSeat.getErrorStatus() == true)
            {
                curSeat.setErrorStatus(false);
            }
            */

            if(voltage < 300) {
                if(curSeat.getCautionStatus() == true) {
                    if(bettery < 1900 && bettery > -1) {
                        curSeat.setCautionStatus(true);
                    }
                    else {
                        curSeat.setCautionStatus(false);
                    }

                    if(curSeat.getErrorStatus() == true) {
                        curSeat.setErrorStatus(false);
                    }
                }
                curSeat.setSeatStatus(bsBusSeatItem.STATUS_OCCUPIED);
            }
            else {
                curSeat.setSeatStatus(bsBusSeatItem.STATUS_EMPTY);
            }

            if(bettery < 1900 && bettery > -1) {
                curSeat.setSeatStatus(bsBusSeatItem.STATUS_LOW_BATTERY);
                if(curSeat.getLowBattery() == false) {
                    Log.e(TAG, "UpdateSeatInfomation : " + "curSeat.getCntError() " + curSeat.getCntError());

                    if(curSeat.getCntError() > 40) {
                        curSeat.setCntError(-1);

                        curSeat.setCautionStatus(true);
                        // alert_msg 배터리 부족 알람
                        CautionSeatStatus(curSeat);

                        LowBatteryMsg();
                        curSeat.setLowBattery(true);
                    }
                    else {
                        curSeat.setCntError(1);
                    }
                }
            }
            else {
                if(curSeat.getLowBattery() == true) {
                    curSeat.setLowBattery(false);
                }

                if(curSeat.getCntError() > 0) {
                    //Log.e(TAG, "UpdateSeatInfomation : " + "curSeat.getCntError() " + curSeat.getCntError());
                    curSeat.setCntError(-1);
                }
            }

            curSeat.setRssi(rssi);
            curSeat.setSyncTime(new Date());
            curSeat.setSync(true);
            curSeat.setSeatUser(true);
            curSeat.setSeatBattery(Integer.toString(bettery));

            if(isScreenOn(getApplicationContext()) == true) {
                //curSeat.setSyncTime(new Date());
                if(curSeat.getCntMoved() == 0) {
                    curSeat.setCntMoved(1);
                    Log.e(TAG, "checkFirst : " + curSeat.getCntMoved());
                }
                else {
                    if(checkCarMovement == true) {
                        if(blockAlert == false) {
                            if(curSeat.getPreStatus().equals(curSeat.getSeatStatus()) == false) {
                                if(curSeat.getPreStatus().equals(bsBusSeatItem.STATUS_OCCUPIED)) {
                                    curSeat.setCntMoved(1);
                                    // AlertSeatMoved();
                                }
                            }
                            else {
                                if(curSeat.getCntMoved() > 1) {
                                    if(curSeat.getSeatStatus().equals(bsBusSeatItem.STATUS_OCCUPIED) == false) {
                                        if(curSeat.getCntMoved() > 80) {
                                            Log.e(TAG, "AlertSeatMoved() : " + curSeat.getCntMoved());

                                            // alert_msg 좌석 이동 알람
                                            curSeat.setCautionStatus(true);
                                            CautionSeatStatus(curSeat);

                                            AlertSeatMoved(curSeat);
                                            curSeat.setCntMoved(-1);
                                        }
                                        else {
                                            curSeat.setCntMoved(1);
                                        }
                                    }
                                    else {
                                        curSeat.setCntMoved(-1);
                                    }
                                }
                            }
                            Log.e(TAG, "curSeat.getCntMoved() : " + curSeat.getCntMoved());
                        }
                        else {
                            Log.e(TAG, "Blocked AlertSeatMoved()");
                            curSeat.setCntMoved(-1);
                        }
                    }
                }
                curSeat.setPreSeatStatus(curSeat.getSeatStatus());
            }

            Log.e(TAG, "UpdateSeatInfomation : " + deviceKey  +   " / "  +  result.getDevice().getName()  + " / " + curSeat.getSeatStatus() +  " /  " + voltage +  " / " + curSeat.getSeatBattery()  + " / " + curSeat.getRssi());

            Intent intent = new Intent(REFRESH_LOCAL_BROADCAST);
            intent.putExtra(REFRESH_DEVICE, curSeat.getSeatNumber());
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }
    }

    private void CautionSeatStatus(SeatInfo cautionSeat)
    {
        Intent intent = new Intent(CAUTION_SEAT_BROADCAST);
        intent.putExtra(REFRESH_DEVICE, cautionSeat.getSeatNumber());
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }


    public SeatInfo getSeatInformation(String device_code)
    {
        //Log.e(TAG, "Check Device " + device_code);

        if(mServiceDeviceMap.containsKey(device_code) == true) {
            return mServiceDeviceMap.get(device_code);
        }
        else {
            return null;
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        Log.e(TAG, "onBind()");
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }
    private PowerManager.WakeLock sCpuWakeLock, sPatialWakeLock;
    private PowerManager pm, pPm;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.e(TAG, "onCreate()" );

        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mScreenStateReceiver, screenStateFilter);

        IntentFilter bluetoothFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mBluetoothReceiver, bluetoothFilter);


        pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        sCpuWakeLock = pm.newWakeLock(
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                        PowerManager.ACQUIRE_CAUSES_WAKEUP |
                        PowerManager.ON_AFTER_RELEASE, "BusSeatService");

        pPm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        sPatialWakeLock = pPm.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK |
                        PowerManager.ACQUIRE_CAUSES_WAKEUP |
                        PowerManager.ON_AFTER_RELEASE, "PartialBusSeatService");
        sPatialWakeLock.acquire();

        setLocationService();
    }

    private BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        Log.e(TAG, "BluetoothAdapter.STATE_OFF");
                        BTmoduleStopMsg(null);
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Log.e(TAG, "BluetoothAdapter.STATE_TURNING_OFF");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Log.e(TAG, "BluetoothAdapter.STATE_ON");
                        scanner.stopScan(scanCallback);
                        oldMethod();
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Log.e(TAG, "BluetoothAdapter.STATE_TURNING_ON");
                        break;
                }
            }
        }
    };



    private BroadcastReceiver mScreenStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equalsIgnoreCase(Intent.ACTION_SCREEN_OFF)) {
                Log.e(TAG, "Intent.ACTION_SCREEN_OFF" );

                if(nCurrentSpeed < MIN_SPEED) {
                    WakeUpDevice();
                }

                if(flagAlways == true) {
                    Log.e(TAG, "Display always on" );
                    if (sCpuWakeLock.isHeld() == true) {
                        sCpuWakeLock.release();
                        sCpuWakeLock = null;
                        sCpuWakeLock = pm.newWakeLock(
                                PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                                        PowerManager.ACQUIRE_CAUSES_WAKEUP |
                                        PowerManager.ON_AFTER_RELEASE, "BusSeatService");
                    }
                    sCpuWakeLock.acquire();
                }
                else
                {
                    if(holdScreenOn == true)
                    {
                        Log.e(TAG, "Display still held" );
                        if (sCpuWakeLock.isHeld() == true) {
                            sCpuWakeLock.release();
                            sCpuWakeLock = null;

                            sCpuWakeLock = pm.newWakeLock(
                                    PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                                            PowerManager.ACQUIRE_CAUSES_WAKEUP |
                                            PowerManager.ON_AFTER_RELEASE, "BusSeatService");
                        }
                        sCpuWakeLock.acquire();
                    }
                    //AlarmWakeLock.wakeLock(getApplicationContext());
                }

            }
            else if(intent.getAction().equalsIgnoreCase(Intent.ACTION_SCREEN_ON)) {
                Log.e(TAG, "Intent.ACTION_SCREEN_ON" );
            }
        }
    };

    public void setAbnormalEnd(boolean abnormal)
    {
        Log.e(TAG, "setAbnormalEnd "  + abnormal);
        this.abnormalEnd = abnormal;
    }

    private void WakeUpDevice()
    {
        if(sCpuWakeLock!= null) {
            if (sCpuWakeLock.isHeld() == true) {
                sCpuWakeLock.release();
                sCpuWakeLock = null;
                sCpuWakeLock = pm.newWakeLock(
                        PowerManager.SCREEN_DIM_WAKE_LOCK |
                                PowerManager.ACQUIRE_CAUSES_WAKEUP |
                                PowerManager.ON_AFTER_RELEASE, "BusSeatService");
            }
            sCpuWakeLock.acquire();
            //sCpuWakeLock.release();
        }
    }


    @Override
    public void onDestroy() {

        Log.e(TAG, "onDestroy() " );
        unregisterReceiver(mScreenStateReceiver);
        unregisterReceiver(mBluetoothReceiver);

        if(mDeviceAllDisconnted !=null) {
            mDeviceAllDisconnted.removeCallbacksAndMessages(null);
        }

        if(mSyncCheckHandler !=null) {
            mSyncCheckHandler.removeCallbacksAndMessages(null);
        }

        if(mDisConnectCheckHandler != null) {
            mDisConnectCheckHandler.removeCallbacksAndMessages(null);
        }

        //mBeaconManager.unbind(this);

        locationManager.removeUpdates(this);

        if(sCpuWakeLock!= null) {
            if (sCpuWakeLock.isHeld() == true) {
                sCpuWakeLock.release();
                sCpuWakeLock = null;
            }
        }

        if(sPatialWakeLock!=null) {
            if (sPatialWakeLock.isHeld() == true) {
                sPatialWakeLock.release();
                sPatialWakeLock = null;
            }
        }

        scanner.stopScan(scanCallback);

        if(abnormalEnd == true) {
            if(sCpuWakeLock!= null) {
                if (sCpuWakeLock.isHeld() == true) {
                    sCpuWakeLock.release();
                    sCpuWakeLock = null;
                    sCpuWakeLock = pm.newWakeLock(
                            PowerManager.SCREEN_DIM_WAKE_LOCK |
                                    PowerManager.ACQUIRE_CAUSES_WAKEUP |
                                    PowerManager.ON_AFTER_RELEASE, "BusSeatService");
                }
                sCpuWakeLock.acquire(5000);
            }

//            Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
//            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            notificationIntent.putExtra("msg", "강제 종료시 비정상적인 동작이 발생할 수 있습니다. 서비스를 재시작 해주세요.");
//            notificationIntent.putExtra("code", "abnormal");
//            notificationIntent.putExtra("bus_name", bus_name);
//            notificationIntent.putExtra("bus_code", bus_code);
//            notificationIntent.putExtra("bus_seat", bus_seat);
//            this.startActivity(notificationIntent);
            //"NCT-Vi"
            Intent notificationIntent = new Intent(this, BusSeatAlertDialog.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            notificationIntent.putExtra("msg", "Đã có sự cố bất thường xảy ra. Vui lòng khởi động lại ứng dụng.");
            notificationIntent.putExtra("code", "abnormal");
            notificationIntent.putExtra("bus_name", bus_name);
            notificationIntent.putExtra("bus_code", bus_code);
            notificationIntent.putExtra("bus_seat", bus_seat);
            this.startActivity(notificationIntent);

            /*
            Intent notificationIntent = new Intent(this, BusSeatServiceActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            notificationIntent.putExtra("bus_name", bus_name);
            notificationIntent.putExtra("bus_code", bus_code);
            notificationIntent.putExtra("bus_seat", bus_seat);
            */

            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            PendingIntent contentIntent;
            contentIntent = PendingIntent.getActivity(this, 78908, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder  builder = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel notificationChannel = new NotificationChannel("ICHAK_NOTIFICATION_ID", "iChak Notifications", importance);
                mNotificationManager.createNotificationChannel(notificationChannel);
                builder = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
            } else {
                builder = new NotificationCompat.Builder(getApplicationContext());
            }

            NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
//            bigTextStyle.bigText("서비스가 강제 종료되었습니다. 강제 종료시 비정상적인 동작이 발생할 수 있습니다. 서비스를 재시작해주세요.");
            //"NCT-Vi"
            bigTextStyle.bigText("Dịch vụ đã tắt. Đã có sự cố bất thường xảy ra. Vui lòng khởi động lại ứng dụng.");

//            builder =  builder
//                            .setSmallIcon(R.drawable.icon_bus_care)
//                            .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
//                            .setContentTitle("아이착이 강제종료됩니다!")
//                            .setSound(alarmSound)
//                            .setContentIntent(contentIntent)
//                            .setOngoing(false)
//                            .setAutoCancel(true)
//                            .setPriority(NotificationManager.IMPORTANCE_HIGH)
//                            .setContentText("강제 종료시 비정상적인 동작이 발생할 수 있습니다. 서비스를 재시작 해주세요.").setStyle(bigTextStyle);
            //"NCT-Vi"
            builder =  builder
                    .setSmallIcon(R.drawable.icon_bus_care)
                    .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                    .setContentTitle("Dịch vụ đã tắt")
                    .setSound(alarmSound)
                    .setContentIntent(contentIntent)
                    .setOngoing(false)
                    .setAutoCancel(true)
                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
                    .setContentText("Đã có sự cố bất thường xảy ra. Vui lòng khởi động lại ứng dụng.").setStyle(bigTextStyle);
            builder.setContentIntent(contentIntent);
            mNotificationManager.notify(7848343, builder.build());
        }
    }
}
