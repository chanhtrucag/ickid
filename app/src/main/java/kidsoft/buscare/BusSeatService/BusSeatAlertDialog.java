package kidsoft.buscare.BusSeatService;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import kidsoft.buscare.BaseActivity;
import kidsoft.buscare.R;

public class BusSeatAlertDialog extends BaseActivity {


    private TextView tvAlertMsg, tvAlertTitle;
    private Button btCheck;
    private Vibrator vib;
    private MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_bus_seat_alert);

        tvAlertMsg =findViewById(R.id.bssa_text);
        tvAlertTitle = findViewById(R.id.bssa_title);
        mediaPlayer = MediaPlayer.create(this, R.raw.emergency_alarm2);
        mediaPlayer.setLooping(true);
        if(getIntent().getStringExtra("code").contains("move") == false) {
            mediaPlayer.start();
        }

        vib = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        long[] pattern = { 0, 2000, 2000 };
        vib.vibrate(pattern, 0);

        btCheck = findViewById(R.id.bssa_ok);
        if(getIntent().getStringExtra("code").contains("abnormal"))
        {
//            tvAlertTitle.setText("아이착이 강제종료됩니다!");
            //"NCT-Vi"
            tvAlertTitle.setText("Dịch vụ đã tắt!");
        }
        else if(getIntent().getStringExtra("code").contains("bluetooth"))
        {
//            tvAlertTitle.setText("블루투스가 종료되었습니다!");
            //"NCT-Vi"
            tvAlertTitle.setText("Kết nối Bluetooth đã tắt!");
        }
        else if(getIntent().getStringExtra("code").contains("location"))
        {
//            tvAlertTitle.setText("위치서비스가 종료되었습니다!");
            //"NCT-Vi"
            tvAlertTitle.setText("Định vị đã tắt");
        }
        else if(getIntent().getStringExtra("code").contains("unknown") || getIntent().getStringExtra("code").contains("battery"))
        {
//            tvAlertTitle.setText("방석을 확인해주세요");
            //"NCT-Vi"
            tvAlertTitle.setText("Kiểm tra lại nệm ngồi");
        }

        tvAlertMsg.setText(getIntent().getStringExtra("msg"));

        btCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent notificationIntent = new Intent(getApplicationContext(), BusSeatServiceActivity.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                notificationIntent.putExtra("bus_name", getIntent().getStringExtra("bus_name"));
                notificationIntent.putExtra("bus_code", getIntent().getStringExtra("bus_code"));
                notificationIntent.putExtra("bus_seat", getIntent().getStringExtra("bus_seat"));

                getApplicationContext().startActivity(notificationIntent);
                //vib.cancel();
                if(getIntent().getStringExtra("code").contains("bluetooth")== true)
                {
                    startActivity(new Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
                }
                else if(getIntent().getStringExtra("code").contains("location")== true)
                {
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }

                finish();
            }
        });

    }

    protected void onDestroy() {
        super.onDestroy();

        mediaPlayer.stop();
        mediaPlayer.release();
        vib.cancel();
    }
}
