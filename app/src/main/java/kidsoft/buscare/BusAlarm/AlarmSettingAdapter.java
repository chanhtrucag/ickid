package kidsoft.buscare.BusAlarm;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import kidsoft.buscare.BusSeatService.BusSeatServiceActivity;
import kidsoft.buscare.Database.DBHelper;
import kidsoft.buscare.Database.models.AlarmInfo;
import kidsoft.buscare.Database.models.BusInfo;
import kidsoft.buscare.R;

import static android.content.Context.AUDIO_SERVICE;

/**
 * Created by DELL3 on 2018-11-29.
 */

public class AlarmSettingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final String TAG = "AlarmSettingAdapter";
    public final static String SERVICE_START_ALARM = "kidsoft.buscare.BusAlarm.AlarmSettingAdapter.SERVICE_START_ALARM";

    public static final int TYPE_DATE = 0;
    public static final int TYPE_TIME = 1;
    public static final int TYPE_ETC = 2;

    private DBHelper _dbHelper;
    private AlarmInfo cAlarmInfo;
    private String bus_name, bus_code;
    private int bus_id;

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<AlarmModel> mItems = new ArrayList<AlarmModel>();
    private RecyclerView recyclerView;
    private static String[] selected_day_of_week = new String[7];
    private boolean timeSet = false, repeatSet = false, vibSet = false;
    private int volumeSet;

    private AlarmManager mAlarmManager;
    private List<PendingIntent> mDaysPendingIntent = new ArrayList<>(); // 0 ~ 6
    private PendingIntent mDatePendingIntent; // 9999

    private Intent serviceStartIntent;

    public static class DateViewHolder extends RecyclerView.ViewHolder {

        private TextView tvSetDate;
        private Switch swTimeSet;
        private Button btSun, btMon, btTue, btWed, btThu, btFri, btSat;
        DatePicker datePicker;

        public DateViewHolder(View itemView){
            super(itemView);

            tvSetDate = itemView.findViewById(R.id.as_selected_date);
            swTimeSet = itemView.findViewById(R.id.as_date_auto_switch);

            btMon = itemView.findViewById(R.id.as_date_mon);
            btTue = itemView.findViewById(R.id.as_date_tue);
            btWed = itemView.findViewById(R.id.as_date_wed);
            btThu = itemView.findViewById(R.id.as_date_thu);
            btFri = itemView.findViewById(R.id.as_date_fri);
            btSat = itemView.findViewById(R.id.as_date_sat);
            btSun = itemView.findViewById(R.id.as_date_sun);

            for(int i =0; i < selected_day_of_week.length; ++i) {
                selected_day_of_week[i] = "0";
            }

        }
    }

    public static class TimeViewHolder extends RecyclerView.ViewHolder {

        private TimePicker timePicker;
        private Switch swTimeSet;

        public TimeViewHolder(View itemView) {
            super(itemView);

            timePicker =itemView.findViewById(R.id.as_timePicker);
            swTimeSet = itemView.findViewById(R.id.as_time_auto_switch);

        }
    }

    public static class EtcViewHolder extends RecyclerView.ViewHolder {

        private TextView tvSetVolume;
        private Switch swRepeatSet;
        private Switch swVibSet;

        public EtcViewHolder(View itemView) {
            super(itemView);

            tvSetVolume = itemView.findViewById(R.id.as_etc_volume_text);
            swRepeatSet = itemView.findViewById(R.id.as_etc_repeat);
            swVibSet = itemView.findViewById(R.id.as_etc_vib);
            }

        }

    public AlarmSettingAdapter(Context context, RecyclerView recyclerView, ArrayList<AlarmModel> items, int bus_id, String bus_name, String bus_code, String bus_seat) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.mItems = items;
        this.recyclerView = recyclerView;
        this._dbHelper = new DBHelper(context);
        this.bus_id = bus_id;
        this.bus_name = bus_name;
        this.bus_code = bus_code;

        mAlarmManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
        //serviceStartIntent = new Intent(mContext, BusSeatServiceActivity.class);
        serviceStartIntent = new Intent(SERVICE_START_ALARM);
        serviceStartIntent.putExtra("BUS_ID", bus_id);
        serviceStartIntent.putExtra("BUS_NAME", bus_name);
        serviceStartIntent.putExtra("BUS_CODE", bus_code);
        serviceStartIntent.putExtra("BUS_SEAT", bus_seat);


        mDatePendingIntent = PendingIntent.getBroadcast(mContext, this.bus_id, serviceStartIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mDaysPendingIntent.clear();
        for(int i=0; i < 7; ++i)
        {
            mDaysPendingIntent.add(i, PendingIntent.getBroadcast(mContext, (this.bus_id * 10 + i), serviceStartIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        }

        List<AlarmInfo> mTmp = _dbHelper.mAlarm.QueryByCode(bus_name, bus_code);

        if(mTmp.size() == 0)
        {
            Log.e(TAG, "No Alarm Settings");
        }
        else
        {
            Log.e(TAG, "Existed Settings");
            cAlarmInfo = mTmp.get(0);


            serviceStartIntent.putExtra("ALARM_REPEAT", cAlarmInfo.getAlarmRepeat());
            serviceStartIntent.putExtra("ALARM_VIB", cAlarmInfo.getAlarmVib());

             /*if(cAlarmInfo.getAlarmAuto() == true)
            {
                if(cAlarmInfo.getAlarmDays().contains("Mon")  || cAlarmInfo.getAlarmDays().contains("Tue")
                        || cAlarmInfo.getAlarmDays().contains("Wed") || cAlarmInfo.getAlarmDays().contains("Thu")
                        || cAlarmInfo.getAlarmDays().contains("Fri") || cAlarmInfo.getAlarmDays().contains("Sat") || cAlarmInfo.getAlarmDays().contains("Sun"))
                {
                    for(int i=0; i < 7; ++i)
                    {
                        mDaysPendingIntent.add(i, PendingIntent.getBroadcast(mContext, (this.bus_id * 10 + i), serviceStartIntent, PendingIntent.FLAG_UPDATE_CURRENT));
                    }
                }
                else
                {
                    mDatePendingIntent = PendingIntent.getBroadcast(mContext, this.bus_id, serviceStartIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                }
            }
            */

        }
    }


        @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case TYPE_DATE:
                view = mLayoutInflater.inflate(R.layout.layout_as_date, parent, false);
                return new DateViewHolder(view);

            case TYPE_TIME:
                view = mLayoutInflater.inflate(R.layout.layout_as_time, parent, false);
                return new TimeViewHolder(view);

            case TYPE_ETC:
                view = mLayoutInflater.inflate(R.layout.layout_as_etc, parent, false);
                return new EtcViewHolder(view);
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).getType();
    }

    private DateViewHolder mDateViewHolder;
    private int year, month, day;
    private Date date;
    private int dayNum;
    private int hour, min;
    private String setDays;


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder convertView, int position) {

        int type = getItemViewType(position);
        if(type == TYPE_DATE)
        {
            mDateViewHolder = (DateViewHolder)convertView;

            final Calendar calendar = Calendar.getInstance();
            if(cAlarmInfo == null)
            {
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                day = calendar.get(Calendar.DAY_OF_MONTH);
                dayNum =  calendar.get(Calendar.DAY_OF_WEEK);
            }
            else
            {
                calendar.setTime(cAlarmInfo.getAlarmDate());
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                day = calendar.get(Calendar.DAY_OF_MONTH);
                dayNum =  calendar.get(Calendar.DAY_OF_WEEK);
            }

            if(cAlarmInfo != null)
            {
                timeSet = cAlarmInfo.getAlarmAuto();
                mDateViewHolder.swTimeSet.setChecked(timeSet);
            }

            mDateViewHolder.swTimeSet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    timeSet = isChecked;
                }
            });


            String day_of_week = "";
            switch(dayNum){
//                case 1:day_of_week = "일";break ;
//                case 2:day_of_week = "월";break ;
//                case 3:day_of_week = "화";break ;
//                case 4:day_of_week = "수";break ;
//                case 5:day_of_week = "목";break ;
//                case 6:day_of_week = "금";break ;
//                case 7:day_of_week = "토";break ;
                //"NCT-Vi"
                case 1:day_of_week = "CN";break ;
                case 2:day_of_week = "T2";break ;
                case 3:day_of_week = "T3";break ;
                case 4:day_of_week = "T4";break ;
                case 5:day_of_week = "T5";break ;
                case 6:day_of_week = "T6";break ;
                case 7:day_of_week = "T7";break ;
            }

            //                    mDateViewHolder.tvSetDate.setText("tháng " + (month+1) + "ngày " + day + "(" + day_of_week + ")");
            //"NCT-Vi"
            mDateViewHolder.tvSetDate.setText("ngày " + day + " tháng " + (month+1) + "(" + day_of_week + ")");

            if(cAlarmInfo !=null) {
                Log.e(TAG, "cAlarmInfo.getAlarmDays " + cAlarmInfo.getAlarmDays());
                if(cAlarmInfo.getAlarmDays().contains("Mon") || cAlarmInfo.getAlarmDays().contains("Tue")
                        || cAlarmInfo.getAlarmDays().contains("Wed") || cAlarmInfo.getAlarmDays().contains("Thu")
                        ||cAlarmInfo.getAlarmDays().contains("Fri") || cAlarmInfo.getAlarmDays().contains("Sat") || cAlarmInfo.getAlarmDays().contains("Sun"))
                {
                    mDateViewHolder.tvSetDate.setTextColor(Color.parseColor("#c9c8c8"));
                }
                else
                {
                    mDateViewHolder.tvSetDate.setTextColor(Color.parseColor("#2a8dff"));
                }


            }


            mDateViewHolder.tvSetDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*
                    year = calendar.get(Calendar.YEAR);
                    month = calendar.get(Calendar.MONTH);
                    day = calendar.get(Calendar.DAY_OF_MONTH);
                    */
                    new DatePickerDialog(mContext, R.style.ASdate_picker, mDateListener, year, month, day).show();
                }
            });


            if(cAlarmInfo !=null) {
                if(cAlarmInfo.getAlarmDays().contains("Mon")) {selected_day_of_week[1] = "Mon";mDateViewHolder.btMon.setSelected(true);
                }
                if(cAlarmInfo.getAlarmDays().contains("Tue")) {selected_day_of_week[2] = "Tue";mDateViewHolder.btTue.setSelected(true);
                }
                if(cAlarmInfo.getAlarmDays().contains("Wed")) {selected_day_of_week[3] = "Wed";mDateViewHolder.btWed.setSelected(true);
                }
                if(cAlarmInfo.getAlarmDays().contains("Thu")) {selected_day_of_week[4] = "Thu";mDateViewHolder.btThu.setSelected(true);
                }
                if(cAlarmInfo.getAlarmDays().contains("Fri")) {selected_day_of_week[5] = "Fri";mDateViewHolder.btFri.setSelected(true);
                }
                if(cAlarmInfo.getAlarmDays().contains("Sat")) {selected_day_of_week[6] = "Sat";mDateViewHolder.btSat.setSelected(true);
                }
                if(cAlarmInfo.getAlarmDays().contains("Sun")) {selected_day_of_week[0] = "Sun";mDateViewHolder.btSun.setSelected(true);
                }
            }

            mDateViewHolder.btMon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {mDateViewHolder.btMon.setSelected(!mDateViewHolder.btMon.isSelected());
                    if(mDateViewHolder.btMon.isSelected() == true) { selected_day_of_week[1] = "Mon";}else{ selected_day_of_week[1] = "0"; }
                    checkDaysSelected();
                }
            });
            mDateViewHolder.btTue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {mDateViewHolder.btTue.setSelected(!mDateViewHolder.btTue.isSelected());
                    if(mDateViewHolder.btTue.isSelected() == true) { selected_day_of_week[2] = "Tue";}else{ selected_day_of_week[2] = "0"; }
                    checkDaysSelected();
                }
            });
            mDateViewHolder.btWed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {mDateViewHolder.btWed.setSelected(!mDateViewHolder.btWed.isSelected());
                    if(mDateViewHolder.btWed.isSelected() == true) { selected_day_of_week[3] = "Wed";}else{ selected_day_of_week[3] = "0"; }
                    checkDaysSelected();
                }
            });
            mDateViewHolder.btThu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {mDateViewHolder.btThu.setSelected(!mDateViewHolder.btThu.isSelected());
                    if(mDateViewHolder.btThu.isSelected() == true) { selected_day_of_week[4] = "Thu";}else{ selected_day_of_week[4] = "0"; }
                    checkDaysSelected();
                }
            });
            mDateViewHolder.btFri.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {mDateViewHolder.btFri.setSelected(!mDateViewHolder.btFri.isSelected());
                    if(mDateViewHolder.btFri.isSelected() == true) { selected_day_of_week[5] = "Fri";}else{ selected_day_of_week[5] = "0"; }
                    checkDaysSelected();
                }
            });
            mDateViewHolder.btSat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {mDateViewHolder.btSat.setSelected(!mDateViewHolder.btSat.isSelected());
                    if(mDateViewHolder.btSat.isSelected() == true) { selected_day_of_week[6] = "Sat";}else{ selected_day_of_week[6] = "0"; }
                    checkDaysSelected();
                }
            });
            mDateViewHolder.btSun.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {mDateViewHolder.btSun.setSelected(!mDateViewHolder.btSun.isSelected());
                    if(mDateViewHolder.btSun.isSelected() == true) { selected_day_of_week[0] = "Sun";}else{ selected_day_of_week[0] = "0"; }
                    checkDaysSelected();
                }
            });

        }
        else if(type == TYPE_TIME)
        {
            TimeViewHolder mTimeViewHolder = (TimeViewHolder)convertView;

            if(cAlarmInfo != null)
            {
                timeSet = cAlarmInfo.getAlarmAuto();
                mTimeViewHolder.swTimeSet.setChecked(timeSet);
            }

            mTimeViewHolder.swTimeSet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    timeSet = isChecked;
                }
            });


            if(cAlarmInfo == null)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    hour = mTimeViewHolder.timePicker.getHour();
                    min  = mTimeViewHolder.timePicker.getMinute();
                }else{
                    hour = mTimeViewHolder.timePicker.getCurrentHour();
                    min  = mTimeViewHolder.timePicker.getCurrentMinute();
                }

            }else
            {

                String sHour = cAlarmInfo.getAlarmTime().substring(0,2);
                String sMin  = cAlarmInfo.getAlarmTime().substring(2,4);

                hour = Integer.parseInt(sHour);
                min = Integer.parseInt(sMin);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    mTimeViewHolder.timePicker.setHour(hour);
                    mTimeViewHolder.timePicker.setMinute(min);
                }else {
                    mTimeViewHolder.timePicker.setCurrentHour(hour);
                    mTimeViewHolder.timePicker.setCurrentMinute(min);
                }
            }

            //mTimeViewHolder.timePicker.setIs24HourView(true);
            mTimeViewHolder.timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                @Override
                public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                    //Log.e(TAG, "timePicker: " + hourOfDay +  " : " + minute);
                    hour = hourOfDay; min = minute;
                }
            });


        }
        else if(type == TYPE_ETC)
        {
            EtcViewHolder mEtcViewHolder = (EtcViewHolder)convertView;

            if(cAlarmInfo != null)
            {
                repeatSet = cAlarmInfo.getAlarmRepeat();
                mEtcViewHolder.swRepeatSet.setChecked(repeatSet);

                vibSet = cAlarmInfo.getAlarmVib();
                mEtcViewHolder.swVibSet.setChecked(vibSet);
            }

            mEtcViewHolder.swRepeatSet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    repeatSet = isChecked;
                }
            });

            mEtcViewHolder.swVibSet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    vibSet = isChecked;
                }
            });

            mEtcViewHolder.tvSetVolume.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(Settings.ACTION_SOUND_SETTINGS));
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void doSaveAlarm()
    {
        Log.e(TAG, "doSaveAlarm()");
        boolean selected_day = false;
        setDays = Arrays.toString(selected_day_of_week);
        for(int i =0; i<selected_day_of_week.length ; ++i) {
            if(selected_day_of_week[i].equals("0") == false) {
                selected_day = true;
                break;
            }
        }
        Log.e(TAG, "setDays "  + setDays  + " / " + selected_day);

        AlarmInfo alarmInfo = new AlarmInfo();
        alarmInfo.setBusName(bus_name);
        alarmInfo.setBusCode(bus_code);

        Calendar cal = Calendar.getInstance();
        //Log.e(TAG, "hour  " +  hour + " min " + min);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, min);
        cal.set(Calendar.SECOND, 0);

        date = cal.getTime();
        alarmInfo.setAlarmDate(date);
        alarmInfo.setAlarmDays(setDays);

        String setTime;

        if(hour<10) {
            setTime = "0" + Integer.toString(hour);
        }
        else {
            setTime = Integer.toString(hour);
        }

        if(min <10) {
            setTime = setTime + "0" + Integer.toString(min);
        }
        else {
            setTime = setTime + Integer.toString(min);
        }

        //Log.e(TAG, "setTime  " +  setTime);

        alarmInfo.setAlarmTime(setTime);
        alarmInfo.setAlarmAuto(timeSet);
        alarmInfo.setAlarmRepeat(repeatSet);
        alarmInfo.setAlarmVib(vibSet);

        AudioManager am = (AudioManager) mContext.getSystemService(AUDIO_SERVICE);
        int volume_level= am.getStreamVolume(AudioManager.STREAM_MUSIC);
        alarmInfo.setAlarmVolume(volume_level);

        SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Log.e(TAG, "Date " +  transFormat.format(alarmInfo.getAlarmDate()));

        List<AlarmInfo> mTmp = _dbHelper.mAlarm.QueryByCode(alarmInfo.getBusName(), alarmInfo.getBusCode());
        if(mTmp.size() == 0) {
            Log.e(TAG, "New Alarm Setting!!");
            _dbHelper.mAlarm.Add(alarmInfo);
        }
        else {
            Log.e(TAG, "Update Alarm Setting!!");
            _dbHelper.mAlarm.Delete(cAlarmInfo);
            _dbHelper.mAlarm.Add(alarmInfo);
        }

        cancelAlarm();

        if( timeSet == true)
        {
            serviceStartIntent.putExtra("ALARM_REPEAT", alarmInfo.getAlarmRepeat());
            serviceStartIntent.putExtra("ALARM_VIB", alarmInfo.getAlarmVib());

            if(selected_day == true)
            {
                // repeat
                Log.e(TAG, "Set Alarm Repeat");
                for(int i=0; i < 7; ++i)
                {
                    if(selected_day_of_week[i].equals("0") == false) {

                        mDaysPendingIntent.add(i, PendingIntent.getBroadcast(mContext, (this.bus_id * 10 + i), serviceStartIntent, PendingIntent.FLAG_UPDATE_CURRENT));

                        Calendar now = Calendar.getInstance();
                        Calendar mAlarm = Calendar.getInstance();
                        //mCalendar.set(Calendar.YEAR, year);
                        //mCalendar.set(Calendar.MONTH, month);
                        mAlarm.set(Calendar.DAY_OF_WEEK, (i+1) );
                        mAlarm.set(Calendar.HOUR_OF_DAY, hour);
                        mAlarm.set(Calendar.MINUTE, min);
                        mAlarm.set(Calendar.SECOND, 0);

                        if(mAlarm.getTimeInMillis() <= now.getTimeInMillis())
                        {
                            mAlarm.add(Calendar.DAY_OF_MONTH, 7);
                            mAlarm.set(Calendar.DAY_OF_WEEK, (i+1) );
                        }

                        Date tmp_date = mAlarm.getTime();
                        Log.e(TAG, "Set Date : " + tmp_date.toString());
                        mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mAlarm.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, mDaysPendingIntent.get(i));
                    }
                }
            }
            else
            {
                // one day
                mDatePendingIntent.cancel();
                mDatePendingIntent = PendingIntent.getBroadcast(mContext, bus_id, serviceStartIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                if(date.after(new Date()))
                {
                    Log.e(TAG, "Set Alarm One Day");
                    mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), mDatePendingIntent);
                }
                else
                {
                    Log.e(TAG, "Passed !");
                }
            }
        }
        else
        {
            Log.e(TAG, "Alarm Canceled !");
            cancelAlarm();
        }
    }

    private void checkDaysSelected()
    {
        boolean checkDays = false;
        for(int i=0; i < 7; ++i) {
            if (selected_day_of_week[i].equals("0") == false) {
                checkDays = true;
                break;
            }
        }

        if(checkDays == true) {
            mDateViewHolder.tvSetDate.setTextColor(Color.parseColor("#c9c8c8"));
        }
        else {
            mDateViewHolder.tvSetDate.setTextColor(Color.parseColor("#2a8dff"));
        }
    }


    public void doDeleteAlarm()
    {
        Log.e(TAG, "doDeleteAlarm()");

        cancelAlarm();

        List<AlarmInfo> mTmp = _dbHelper.mAlarm.QueryByCode(bus_name, bus_code);

        if(mTmp.size() == 0)
        {
            Log.e(TAG, "No Alarm Settings");
        }
        else {
            _dbHelper.mAlarm.Delete(cAlarmInfo);
            Log.e(TAG, "Delete Alarm Settings");
        }
    }

    private void cancelAlarm()
    {
        if (mDatePendingIntent != null) {
            mAlarmManager.cancel(mDatePendingIntent);
            mDatePendingIntent.cancel();
        }

        if(mDaysPendingIntent != null) {
            for(int i=0; i < 7; ++i) {
                mAlarmManager.cancel(mDaysPendingIntent.get(i));
                mDaysPendingIntent.get(i).cancel();
            }
        }
    }

    private void setBootReceiver()
    {
        ComponentName receiver = new ComponentName(mContext, ReBootAlarmReceiver.class);
        PackageManager pm = mContext.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    private void removeBootReceiver()
    {
        ComponentName receiver = new ComponentName(mContext, ReBootAlarmReceiver.class);
        PackageManager pm = mContext.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }


    private DatePickerDialog.OnDateSetListener mDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub

                    year = arg1;
                    month = arg2;
                    day = arg3;

                    Calendar mCalendar = Calendar.getInstance();
                    mCalendar.set(Calendar.YEAR, year);
                    mCalendar.set(Calendar.MONTH, month);
                    mCalendar.set(Calendar.DAY_OF_MONTH,  day);

                    /*
                    date = mCalendar.getTime();
                    SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Log.e(TAG, "Date " +  transFormat.format(date));
                    */

                    int dayNum =   mCalendar.get(Calendar.DAY_OF_WEEK);
                    String day_of_week = "";
                    switch(dayNum){
//                        case 1:day_of_week = "일";break ;
//                        case 2:day_of_week = "월";break ;
//                        case 3:day_of_week = "화";break ;
//                        case 4:day_of_week = "수";break ;
//                        case 5:day_of_week= "목";break ;
//                        case 6:day_of_week = "금";break ;
//                        case 7:day_of_week= "토";break ;
                        //"NCT-Vi"
                        case 1:day_of_week = "CN";break ;
                        case 2:day_of_week = "T2";break ;
                        case 3:day_of_week = "T3";break ;
                        case 4:day_of_week = "T4";break ;
                        case 5:day_of_week = "T5";break ;
                        case 6:day_of_week = "T6";break ;
                        case 7:day_of_week = "T7";break ;
                    }
                    //Log.e(TAG,  year + " / " + (month+1) + " /  " + day);
//                    mDateViewHolder.tvSetDate.setText("tháng " + (month+1) + "ngày " + day + "(" + day_of_week + ")");
                    //"NCT-Vi"
                    mDateViewHolder.tvSetDate.setText("ngày " + day + " tháng " + (month+1) + "(" + day_of_week + ")");
                    mDateViewHolder.tvSetDate.setTextColor(Color.parseColor("#2a8dff"));

                    selected_day_of_week[1] = "0";mDateViewHolder.btMon.setSelected(false);
                    selected_day_of_week[2] = "0";mDateViewHolder.btTue.setSelected(false);
                    selected_day_of_week[3] = "0";mDateViewHolder.btWed.setSelected(false);
                    selected_day_of_week[4] = "0";mDateViewHolder.btThu.setSelected(false);
                    selected_day_of_week[5] = "0";mDateViewHolder.btFri.setSelected(false);
                    selected_day_of_week[6] = "0";mDateViewHolder.btSat.setSelected(false);
                    selected_day_of_week[0] = "0";mDateViewHolder.btSun.setSelected(false);

                }
            };
}
