package kidsoft.buscare.BusAlarm;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import kidsoft.buscare.BaseActivity;
import kidsoft.buscare.Database.DBHelper;
import kidsoft.buscare.Database.models.AlarmInfo;
import kidsoft.buscare.R;

public class AlarmSettingActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private AlarmSettingAdapter mASadapter;
    private ArrayList<AlarmModel> items;

    private Button btSave, btDel;
    private ImageView ibBack;

    private String bus_name, bus_code, bus_seat;
    private int bus_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_setting);

        bus_id = getIntent().getIntExtra("bus_id", 99999);
        bus_name = getIntent().getStringExtra("bus_name");
        bus_code = getIntent().getStringExtra("bus_code");
        bus_seat = getIntent().getStringExtra("bus_seat");

        //Log.e("AlarmSettingActivity",  "Bus " +  bus_id +  " "  + bus_name + " " + bus_code + " " + bus_seat) ;

        btSave = findViewById(R.id.asSaveBtn);
        btDel = findViewById(R.id.asDeleteBtn);
        ibBack = findViewById(R.id.asBackBtn);

        DBHelper _dbHelper = new DBHelper(this);
        List<AlarmInfo> mTmp = _dbHelper.mAlarm.QueryByCode(bus_name, bus_code);
        if(mTmp.size() == 0)
        {
            btDel.setVisibility(View.GONE);
        }

        recyclerView = findViewById(R.id.as_item_list);
        items = new ArrayList<AlarmModel>();

        items.add(new DateItem());
        items.add(new TimeItem());
        items.add(new EtcItem());

        mASadapter = new AlarmSettingAdapter(this, recyclerView, items, bus_id, bus_name, bus_code, bus_seat);

        DividerItemDecoration divider = new
                DividerItemDecoration(recyclerView.getContext(),
                DividerItemDecoration.VERTICAL);

        divider.setDrawable(ContextCompat.getDrawable(this, R.drawable.line_divider));
        recyclerView.addItemDecoration(divider);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mASadapter);

        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mASadapter.doSaveAlarm();
                finish();
            }
        });

        btDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mASadapter.doDeleteAlarm();
                finish();
            }
        });

        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
