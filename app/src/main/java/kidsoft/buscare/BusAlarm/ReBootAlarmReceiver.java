package kidsoft.buscare.BusAlarm;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import kidsoft.buscare.BusList.MainActivity;
import kidsoft.buscare.Database.DBHelper;
import kidsoft.buscare.Database.models.AlarmInfo;
import kidsoft.buscare.Database.models.BusInfo;
import kidsoft.buscare.R;

import static kidsoft.buscare.BusAlarm.AlarmSettingAdapter.SERVICE_START_ALARM;

/**
 * Created by DELL3 on 2018-12-04.
 */

public class ReBootAlarmReceiver extends BroadcastReceiver {

    private static final String BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
    private static final String QUICKBOOT_POWERON = "android.intent.action.QUICKBOOT_POWERON";

    private AlarmManager mAlarmManager;
    private List<PendingIntent> mDaysPendingIntent = new ArrayList<>(); // 0 ~ 6
    private PendingIntent mDatePendingIntent;
    private Intent serviceStartIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(BOOT_COMPLETED) || action.equals(QUICKBOOT_POWERON))
        {
            DBHelper _dbHelper =  new DBHelper(context);
            List<AlarmInfo> alarmInfo  = _dbHelper.mAlarm.GetAllAlarms();
            for(int i=0;  i< alarmInfo.size(); ++i)
            {
                List<BusInfo> busInfo = _dbHelper.mBus.QueryByCode(alarmInfo.get(i).getBusName(), alarmInfo.get(i).getBusCode());

                mAlarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
                serviceStartIntent = new Intent(SERVICE_START_ALARM);
                serviceStartIntent.putExtra("BUS_ID", busInfo.get(0).getId());
                serviceStartIntent.putExtra("BUS_NAME", busInfo.get(0).getBusName());
                serviceStartIntent.putExtra("BUS_CODE", busInfo.get(0).getBusCode());
                serviceStartIntent.putExtra("BUS_SEAT", busInfo.get(0).getBusSeat());

                serviceStartIntent.putExtra("ALARM_REPEAT", alarmInfo.get(i).getAlarmRepeat());
                serviceStartIntent.putExtra("ALARM_VIB", alarmInfo.get(i).getAlarmVib());

                mDatePendingIntent = PendingIntent.getBroadcast(context, busInfo.get(0).getId(), serviceStartIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                mDaysPendingIntent.clear();

                for(int j=0; j < 7; ++j) {
                    mDaysPendingIntent.add(j, PendingIntent.getBroadcast(context, (busInfo.get(0).getId() * 10 + j), serviceStartIntent, PendingIntent.FLAG_UPDATE_CURRENT));
                }
                cancelAlarm();

                if(alarmInfo.get(i).getAlarmAuto() == true)
                {
                    boolean selected_days = false;
                    if(alarmInfo.get(i).getAlarmDays().contains("Mon") || alarmInfo.get(i).getAlarmDays().contains("Tue")
                            || alarmInfo.get(i).getAlarmDays().contains("Wed") || alarmInfo.get(i).getAlarmDays().contains("Thu")
                            || alarmInfo.get(i).getAlarmDays().contains("Fri") || alarmInfo.get(i).getAlarmDays().contains("Sat")
                            || alarmInfo.get(i).getAlarmDays().contains("Sun"))
                    {
                        selected_days = true;
                    }

                    if(selected_days == true)
                    {
                        if( alarmInfo.get(i).getAlarmDays().contains("Mon")) {setDaysAlarm(context, 1, busInfo.get(0), alarmInfo.get(i));}
                        if( alarmInfo.get(i).getAlarmDays().contains("Tue")) {setDaysAlarm(context, 2, busInfo.get(0), alarmInfo.get(i));}
                        if( alarmInfo.get(i).getAlarmDays().contains("Wed")) {setDaysAlarm(context, 3, busInfo.get(0), alarmInfo.get(i));}
                        if( alarmInfo.get(i).getAlarmDays().contains("Thu")) {setDaysAlarm(context, 4, busInfo.get(0), alarmInfo.get(i));}
                        if( alarmInfo.get(i).getAlarmDays().contains("Fri")) {setDaysAlarm(context, 5, busInfo.get(0), alarmInfo.get(i));}
                        if( alarmInfo.get(i).getAlarmDays().contains("Sat")) {setDaysAlarm(context, 6, busInfo.get(0), alarmInfo.get(i));}
                        if( alarmInfo.get(i).getAlarmDays().contains("Sun")) {setDaysAlarm(context, 0, busInfo.get(0), alarmInfo.get(i));}
                    }
                    else
                    {
                        setDateAlarm(context, busInfo.get(0), alarmInfo.get(i));
                    }
                }
            }
        }
    }

    private void setDateAlarm(Context context, BusInfo busInfo, AlarmInfo alarmInfo)
    {
        mDatePendingIntent = PendingIntent.getBroadcast(context, busInfo.getId(), serviceStartIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        String sHour = alarmInfo.getAlarmTime().substring(0,2);
        String sMin  = alarmInfo.getAlarmTime().substring(2,4);

        Calendar mAlarm = Calendar.getInstance();
        mAlarm.setTime(alarmInfo.getAlarmDate());
        mAlarm.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sHour));
        mAlarm.set(Calendar.MINUTE, Integer.parseInt(sMin));
        mAlarm.set(Calendar.SECOND, 0);
        Date date = mAlarm.getTime();
        if(date.after(new Date())) {
            mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, mAlarm.getTimeInMillis(), mDatePendingIntent);
        }
    }


    private void setDaysAlarm(Context context, int numOfday, BusInfo busInfo, AlarmInfo alarmInfo)
    {
        mDaysPendingIntent.add(numOfday, PendingIntent.getBroadcast(context, (busInfo.getId() * 10 + numOfday), serviceStartIntent, PendingIntent.FLAG_UPDATE_CURRENT));

        String sHour = alarmInfo.getAlarmTime().substring(0,2);
        String sMin  = alarmInfo.getAlarmTime().substring(2,4);

        Calendar now = Calendar.getInstance();
        Calendar mAlarm = Calendar.getInstance();
        mAlarm.set(Calendar.DAY_OF_WEEK, (numOfday+1) );
        mAlarm.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sHour));
        mAlarm.set(Calendar.MINUTE, Integer.parseInt(sMin));
        mAlarm.set(Calendar.SECOND, 0);

        if(mAlarm.getTimeInMillis() <= now.getTimeInMillis())
        {
            mAlarm.add(Calendar.DAY_OF_MONTH, 7);
            mAlarm.set(Calendar.DAY_OF_WEEK, (numOfday+1) );
        }

        mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, mAlarm.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, mDaysPendingIntent.get(numOfday));



    }

    private void cancelAlarm()
    {
        if (mDatePendingIntent != null) {
            mAlarmManager.cancel(mDatePendingIntent);
            mDatePendingIntent.cancel();
        }

        if(mDaysPendingIntent != null) {
            for(int i=0; i < 7; ++i) {
                mAlarmManager.cancel(mDaysPendingIntent.get(i));
                mDaysPendingIntent.get(i).cancel();
            }
        }
    }
}
