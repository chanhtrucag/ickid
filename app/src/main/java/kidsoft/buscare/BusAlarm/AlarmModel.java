package kidsoft.buscare.BusAlarm;

/**
 * Created by DELL3 on 2018-11-29.
 */

public abstract class AlarmModel {

    public static final int TYPE_DATE = 0;
    public static final int TYPE_TIME = 1;
    public static final int TYPE_ETC = 2;


    abstract public int getType();

}
