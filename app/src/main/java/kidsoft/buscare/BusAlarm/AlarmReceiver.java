package kidsoft.buscare.BusAlarm;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.TimerTask;

import kidsoft.buscare.BusSeatService.BusSeatServiceActivity;
import kidsoft.buscare.R;

/**
 * Created by DELL3 on 2018-12-04.
 */

public class AlarmReceiver extends BroadcastReceiver {

    private static final String TAG = "AlarmReceiver ";

    private PowerManager.WakeLock sCpuWakeLock;
    private PowerManager pm;

    @Override
    public void onReceive(final Context context, Intent intent) {

        pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        sCpuWakeLock = pm.newWakeLock(
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                        PowerManager.ACQUIRE_CAUSES_WAKEUP |
                        PowerManager.ON_AFTER_RELEASE, "AlarmReceiver");

        WakeUpDevice();


        final int   bus_id          = intent.getIntExtra("BUS_ID", 1357);
        final String bus_name = intent.getStringExtra("BUS_NAME");
        final String bus_code = intent.getStringExtra("BUS_CODE");
        final String bus_type = intent.getStringExtra("BUS_SEAT");

        boolean alarm_repeat = intent.getBooleanExtra("ALARM_REPEAT", true);
        boolean alarm_vib    = intent.getBooleanExtra("ALARM_VIB", true);


        Log.e(TAG, "Alarm Test : " +  bus_id +  " - " +  bus_name +  " - " + bus_code  + " - " + bus_type  + " /  " +  alarm_repeat  + " " + alarm_vib);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent notificationIntent = new Intent(context, BusSeatServiceActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("bus_name", bus_name);
        notificationIntent.putExtra("bus_code", bus_code);
        notificationIntent.putExtra("bus_seat", bus_type);

        PendingIntent contentIntent;
        contentIntent = PendingIntent.getActivity(context, bus_id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        final NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder  builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("ICHAK_NOTIFICATION_ID", "iChak Notifications", importance);
            mNotificationManager.createNotificationChannel(notificationChannel);
            builder = new NotificationCompat.Builder(context, notificationChannel.getId());
        } else {
            builder = new NotificationCompat.Builder(context);
        }

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.bigText("Xe " + bus_name + " Xe chuẩn bị khởi hành. Bắt đầu chuyến");
//        bigTextStyle.bigText("차량 " + bus_name + " 출발 예정입니다. 서비스를 시작해주세요");

        builder =  builder
                        .setSmallIcon(R.drawable.icon_bus_care)
                        .setColor(context.getResources().getColor(R.color.colorPrimaryDark))
//                        .setContentTitle("출발 알림")
                        .setContentTitle("Thông báo khởi hành")
                        .setSound(alarmSound)
                        .setVibrate(alarm_vib ? new long[] { 1000, 1000, 1000, 1000, 1000 } : new long[]{0})
                        .setContentIntent(contentIntent)
                        .setOngoing(false)
                        .setAutoCancel(true)
                        .setPriority(NotificationManager.IMPORTANCE_HIGH)
                        .setContentText("Xe " + bus_name + " Chuẩn bị khởi hành. Bắt đầu chuyến.").setStyle(bigTextStyle);
//                        .setContentText("차량 " + bus_name + " 출발 예정입니다. 서비스를 시작해주세요").setStyle(bigTextStyle);


        builder.setContentIntent(contentIntent);
        mNotificationManager.notify(bus_id, builder.build());

        notificationIntent.putExtra("alarm", true);
        context.startActivity(notificationIntent);

        //Toast.makeText(context, bus_name + " 출발 예정입니다. 서비스를 시작해주세요", Toast.LENGTH_LONG).show(); // For example

        if(alarm_repeat) {
            final NotificationCompat.Builder finalBuilder = builder;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mNotificationManager.notify(bus_id, finalBuilder.build());
                    WakeUpDevice();
                }
            }, 10000);
        }

        /*
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("BusCare");
        alertDialogBuilder
                .setMessage(bus_name + " 출발 예정입니다. 서비스를 시작해주세요")
                .setCancelable(false)
                .setPositiveButton("이동",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent notificationIntent = new Intent(context, BusSeatServiceActivity.class);
                                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                notificationIntent.putExtra("bus_name", bus_name);
                                notificationIntent.putExtra("bus_code", bus_code);
                                notificationIntent.putExtra("bus_seat", bus_type);
                                context.startActivity(notificationIntent);
                            }
                        })
                .setNegativeButton("취소",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog, int id) {
                                        dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        */
    }

    private void WakeUpDevice()
    {
        if(sCpuWakeLock!= null) {
            if (sCpuWakeLock.isHeld() == true) {
                sCpuWakeLock.release();
                sCpuWakeLock = null;
                sCpuWakeLock = pm.newWakeLock(
                        PowerManager.SCREEN_DIM_WAKE_LOCK |
                                PowerManager.ACQUIRE_CAUSES_WAKEUP |
                                PowerManager.ON_AFTER_RELEASE, "AlarmReceiver");
            }
            sCpuWakeLock.acquire(5000);
            sCpuWakeLock.release();
        }
    }
}
