package kidsoft.buscare.BusInfoCamera;

import android.Manifest;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.DefaultDecoderFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import kidsoft.buscare.BaseActivity;
import kidsoft.buscare.R;

public class BarcodeScanActivity extends BaseActivity implements DecoratedBarcodeView.TorchListener{

    private CaptureManager capture;
    private DecoratedBarcodeView barcodeScannerView;
    private BeepManager beepManager;


    private Button btNext;
    private ImageButton btRefresh;
    private ImageView btClose;
    private Boolean switchFlashlightButtonCheck;
    private BackPressCloseHandler backPressCloseHandler;
    private boolean resultScant = true;

    private String lastText;


    private EditText ebSeatNumber;

    private BarcodeCallback callback = new BarcodeCallback() {

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
        @Override
        public void barcodeResult(BarcodeResult result) {

            barcodeScannerView.pause();
            lastText = result.getText();
            barcodeScannerView.setStatusText(result.getText());
            ebSeatNumber.setText(lastText.toString());
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_scan);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA}, 100);


        switchFlashlightButtonCheck = true;

        backPressCloseHandler = new BackPressCloseHandler(this);

        ebSeatNumber = (EditText)findViewById(R.id.eb_barcode_text);
        btRefresh = (ImageButton)findViewById(R.id.refreshButton);
        btNext = (Button)findViewById(R.id.seat_next_btn);
        btClose = findViewById(R.id.seat_closeButton);

        barcodeScannerView = (DecoratedBarcodeView)findViewById(R.id.barcode_scanner);
        Collection<BarcodeFormat> formats = Arrays.asList(BarcodeFormat.QR_CODE, BarcodeFormat.CODE_39);
        barcodeScannerView.getBarcodeView().setDecoderFactory(new DefaultDecoderFactory(formats));
        barcodeScannerView.decodeContinuous(callback);

        beepManager = new BeepManager(this);
        barcodeScannerView.setTorchListener(this);


        btRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ebSeatNumber.setText("");
                barcodeScannerView.resume();
            }
        });

        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSeatInfo();
            }
        });

        btClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultScant = false;
                setSeatInfo();
                //finish();
            }
        });

    }

    private void setSeatInfo()
    {

        if(resultScant == false)
        {
            Log.e("Barcode ", "result seat: " + "Falied");
            Intent resultIntent = new Intent();
            resultIntent.putExtra("result_seat", "CANCEL_REGI");
            setResult(RESULT_OK, resultIntent);
            finish();
        }
        else
        {
            if(ebSeatNumber.getText().toString().isEmpty())
            {
                Log.e("Barcode ", "result seat: " + "Falied");
                Intent resultIntent = new Intent();
                resultIntent.putExtra("result_seat", "CANCEL_REGI");
                setResult(RESULT_OK, resultIntent);
                finish();
            }
            else
            {
                Log.e("Barcode ", "result seat: " + ebSeatNumber.getText().toString());
                Intent resultIntent = new Intent();
                resultIntent.putExtra("result_seat", ebSeatNumber.getText().toString());
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        barcodeScannerView.resume();
        //capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        barcodeScannerView.pause();
        //capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //capture.onDestroy();
    }


    public void onBackPressed() {
       finish();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onTorchOn() {

    }

    @Override
    public void onTorchOff() {

    }
}
