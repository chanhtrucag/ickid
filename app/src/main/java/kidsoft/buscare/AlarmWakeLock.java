package kidsoft.buscare;

import android.content.Context;
import android.os.PowerManager;

/**
 * Created by DELL3 on 2017-05-13.
 */

@SuppressWarnings("deprecation")
public class AlarmWakeLock {

    private static final String TAG = "AlarmWakeLock";
    private static PowerManager.WakeLock mWakeLock;

    public static void wakeLock(Context context) {
        if(mWakeLock != null) {
            return;
        }

        PowerManager powerManager =
                (PowerManager)context.getSystemService(
                        Context.POWER_SERVICE);

        mWakeLock = powerManager.newWakeLock(
                PowerManager.FULL_WAKE_LOCK, TAG);
        mWakeLock.acquire();
    }

    public static void releaseWakeLock() {
        if(mWakeLock != null) {
            mWakeLock.release();
            mWakeLock = null;
        }
    }
}
