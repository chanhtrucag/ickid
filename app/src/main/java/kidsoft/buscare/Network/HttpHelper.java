package kidsoft.buscare.Network;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

@SuppressWarnings("unused")
public class HttpHelper {

    private String baseUrl;
    private AsyncHttpClient client;

    public HttpHelper() {
        client = new AsyncHttpClient();
    }

    /**
     * Constructor
     *
     * @param baseUrl Server base url
     */
    public HttpHelper(String baseUrl) {
        if(baseUrl.charAt(baseUrl.length()-1) != '/')
            baseUrl += "/";

        this.baseUrl = baseUrl;
        Log.e("HttpHelper", "baseUrl : " + this.baseUrl);
        client = new AsyncHttpClient();
    }

    /**
     * HttpGet method
     *
     * @param url relative url
     * @param params request parameters
     * @param responseHandler response handler
     */
    public void Get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.e("HttpHelper ", "GET " + getAbsoluteUrl(url));
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    /**
     * HttpPost method
     *
     * @param url relative url
     * @param params request parameters
     * @param responseHandler response handler
     */
    public void Post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.e("HttpHelper ", "POST " + getAbsoluteUrl(url));
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public void Put(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.e("HttpHelper ", "PUT " + getAbsoluteUrl(url));
        client.put(getAbsoluteUrl(url), params, responseHandler);
    }

    private String getAbsoluteUrl(String relativeUrl) {
        if(relativeUrl.charAt(0) == '/')
            relativeUrl = relativeUrl.substring(1);

        return baseUrl + relativeUrl;
    }

}
