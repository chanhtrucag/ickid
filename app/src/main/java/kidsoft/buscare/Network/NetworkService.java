package kidsoft.buscare.Network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.JsonObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;
import kidsoft.buscare.Database.models.BusInfo;
import kidsoft.buscare.Database.models.SeatInfo;

/**
 * Created by DELL3 on 2019-03-05.
 */

public class NetworkService {

    private static final String FLAG  = "NetworkService";
    private Context ctx;
    private HttpHelper _httpHelper;

    private String domain_name ="input_domain_name";
    private String port_number = "";

    public  NetworkService(@NonNull Context ctx) {
        this.ctx = ctx;

        String baseUrl = domain_name;

        if (port_number != null && !port_number.equals(""))
            baseUrl += ":" + port_number + "/";

        if (baseUrl.equals("")) {
            Log.e(FLAG, "URL ERROR");
            return;
        }

        //Create the Http helper.
        _httpHelper = new HttpHelper(baseUrl);

    }

    // POST /api/m/ichak/buses
    public void saveBusInfo(BusInfo busInfo, int seatNumbers)
    {
        Log.e(FLAG, "saveBusInfo");

        RequestParams params = new RequestParams();

        params.put("name", busInfo.getBusName());
        params.put("number", busInfo.getBusCode());
        params.put("map", busInfo.getBusSeat());
        params.put("seatCount", Integer.toString(seatNumbers));

        Log.e(FLAG, "name: " + busInfo.getBusName());
        Log.e(FLAG, "number: " + busInfo.getBusCode());
        Log.e(FLAG, "map: " + busInfo.getBusSeat());
        Log.e(FLAG, "seatCount: " + Integer.toString(seatNumbers));

        _httpHelper.Post("/api/m/ichak/buses", params, new JsonHttpResponseHandler(){
            @Override
            public void onProgress(long bytesWritten, long totalSize) {
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    if (response != null && response.getBoolean("success")) {

                        Log.e(FLAG, "saveBusInfo Success");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(FLAG, "saveBusInfo Failed");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                //DEBUG
                Log.e(FLAG, "saveBusInfo Failed");
                Log.e(FLAG + "-status", String.valueOf(statusCode));
                Log.e(FLAG + "-resp", throwable.getMessage());
                Log.e(FLAG + "-error" , errorResponse.toString());
            }
        });
    }

    // POST /api/m/ichak/buses/{busNumber}/seats
    public void saveSeatInfo(SeatInfo seatInfo)
    {
        Log.e(FLAG, "saveSeatInfo");

        RequestParams params = new RequestParams();

        params.put("busNumber", seatInfo.getBusCode());
        params.put("seatNumber", seatInfo.getSeatPosition());
        params.put("mac", seatInfo.getSeatNumber());
        params.put("status", seatInfo.getSeatStatus());
        params.put("battery", seatInfo.getSeatBattery());

        Log.e(FLAG, "busNumber: " + seatInfo.getBusCode());
        Log.e(FLAG, "seatNumber: " + seatInfo.getSeatPosition());
        Log.e(FLAG, "mac: " + seatInfo.getSeatNumber());
        Log.e(FLAG, "status: " + seatInfo.getSeatStatus());
        Log.e(FLAG, "battery: " + seatInfo.getSeatBattery());

        String url = "/api/m/ichak/buses/" + seatInfo.getBusCode() + "/seats";

        _httpHelper.Post(url, params, new JsonHttpResponseHandler(){

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    if (response != null && response.getBoolean("success")) {
                        Log.e(FLAG, "saveSeatInfo Success");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(FLAG, "saveSeatInfo Failed");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                //DEBUG
                Log.e(FLAG, "saveSeatInfo Failed");
                Log.e(FLAG + "-status", String.valueOf(statusCode));
                Log.e(FLAG + "-resp", throwable.getMessage());
                Log.e(FLAG + "-error" , errorResponse.toString());

            }
        });
    }


    // POST /api/m/ichak/buses/{busNumber}/seats/{seatNumber}/status
    public void updateSeatInfo(SeatInfo seatInfo, String eventType)
    {
        JsonObject body = new JsonObject();
        RequestParams params = new RequestParams();

        //String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));

        SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        //String dt_str = "2000-01-01T13:00:00.000Z";
        String mDate = null;
        Date putDate = null;
        mDate = mFormat.format(new Date());
        try {
            putDate = mFormat.parse(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        params.put("busNumber", seatInfo.getBusName());
        params.put("seatNumber", seatInfo.getSeatPosition());
        params.put("timestamp", mDate.toString());
        params.put("eventType", eventType);
        params.put("status", seatInfo.getSeatStatus());
        params.put("battery", seatInfo.getSeatBattery());
        params.put("accMoving", Integer.toString(seatInfo.getCntAccMoving()));

        Log.e(FLAG, "------------------- updateSeatInfo -----------------------");
        Log.e(FLAG, "busNumber: " + seatInfo.getBusCode());
        Log.e(FLAG, "seatNumber: " + seatInfo.getSeatPosition());
        Log.e(FLAG, "timestamp: " + mDate.toString());
        Log.e(FLAG, "eventType: " + eventType);
        Log.e(FLAG, "status: " + seatInfo.getSeatStatus());
        Log.e(FLAG, "battery: " + seatInfo.getSeatBattery());
        Log.e(FLAG, "accMoving: "+ Integer.toString(seatInfo.getCntAccMoving()));

        String url = "/api/m/ichak/buses/" + seatInfo.getBusCode() + "/seats/" + seatInfo.getSeatPosition() + "/status";

        //Log.e(FLAG, "updateSeatInfo: " + url);
        _httpHelper.Post(url, params, new JsonHttpResponseHandler(){

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    if (response != null && response.getBoolean("success")) {

                        Log.e(FLAG, "updateSeatInfo Success");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(FLAG, "updateSeatInfo Failed");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                //DEBUG
                Log.e(FLAG, "updateSeatInfo Failed");
                Log.e(FLAG + "-status", String.valueOf(statusCode));
                Log.e(FLAG + "-resp", throwable.getMessage());
                Log.e(FLAG + "-error" , errorResponse.toString());
            }
        });
    }


    // POST /api/m/ichak/buses/{busNumber}/status
    public void updateBusInfo(BusInfo busInfo, String eventType, long latitude, long longitude, int speed, int totalSeatOn )
    {
        JsonObject body = new JsonObject();
        RequestParams params = new RequestParams();

        params.put("busNumber", busInfo.getBusName());
        params.put("timestamp", new Date());
        params.put("eventType", eventType);
        params.put("latitude", Long.toString(latitude));
        params.put("longitude", Long.toString(longitude));
        params.put("speed", Integer.toString(speed));
        params.put("totalSeatOn", Integer.toString(totalSeatOn));

        String url = "/api/m/ichak/buses/" +  busInfo.getBusCode() + "/status";
        _httpHelper.Post(url, params, new JsonHttpResponseHandler(){
            @Override
            public void onProgress(long bytesWritten, long totalSize) {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    if (response != null && response.getBoolean("success")) {

                        Log.e(FLAG, "updateBusInfo Success");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(FLAG, "updateBusInfo Failed");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                //DEBUG
                Log.e(FLAG, "updateBusInfo Failed");
                Log.e(FLAG + "-status", String.valueOf(statusCode));
                Log.e(FLAG + "-resp", throwable.getMessage());
                Log.e(FLAG + "-error" , errorResponse.toString());
            }
        });
    }


}
