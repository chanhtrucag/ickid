package kidsoft.buscare.BusList;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.gson.JsonObject;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import kidsoft.buscare.BaseActivity;
import kidsoft.buscare.Database.DBHelper;
import kidsoft.buscare.Database.models.AlarmInfo;
import kidsoft.buscare.Database.models.BusInfo;
import kidsoft.buscare.Database.models.CompanyInfo;
import kidsoft.buscare.Database.models.SeatInfo;
import kidsoft.buscare.Database.specifications.CompanyInfoAll;
import kidsoft.buscare.Network.NetworkService;
import kidsoft.buscare.R;
import layout.BusList.AdapterBusList;
import layout.BusList.AdapterCompanyList;
import layout.BusList.ItemBusList;
import layout.BusList.ItemCompanyList;

import static kidsoft.buscare.Database.DBHelper.DATABASE_VERSION;

public class MainActivity extends BaseActivity {


    private String TAG = "MainActivity";

    private static final int REQUEST_CODE_SET_BUS_INFO = 1000;
    private static final int REQUEST_CODE_BACKUP_FILE_CHOOSE = 9000;


    private AdapterBusList mBusListAdapter;
    private ListView mBusListView;
    private ArrayList<ItemBusList> mBusListArray;

    private AdapterCompanyList mCompanyListAdapter;
    private Spinner spCompanyList;
    private ArrayList<ItemCompanyList> mCompanyListArray;

    private DBHelper _dbHelper;
    //-- Net  private NetworkService mNetworkService;

    private Button btShareOk, btShareCancel;
    private ImageView btShare, btInfo;

    private String company_name;
    private String company_code;
    private int selected_comp_position;

    private File dataFile;
    private File sdDirectory;
    private String sdcard = Environment.getExternalStorageState();
    private FileOutputStream dataOutputStream;

    private final static String DIRECTORY = "/iChakBackup";
    private String DATA_FILE_NAME = "iChakBackup_", DARA_FILE_TYPE = ".iChak";
    private Timestamp timestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _dbHelper = new DBHelper(getApplicationContext());
        //-- Net  mNetworkService = new NetworkService(this);

        SQLiteDatabase db;
        db = _dbHelper.getWritableDatabase();
        Log.e(TAG, "db.getVersion()  " + db.getVersion());

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE
                        , Manifest.permission.READ_EXTERNAL_STORAGE}, 100);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();

        if (!sdcard.equals(Environment.MEDIA_MOUNTED)) {
            sdDirectory = new File(Environment.getRootDirectory().getAbsolutePath() + DIRECTORY);
            Log.e(TAG, "sdDirectory  " + sdDirectory);
        } else {
            sdDirectory = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + DIRECTORY);
            Log.e(TAG, "sdDirectory  " + sdDirectory);
        }

        if (!sdDirectory.exists()) {
            sdDirectory.mkdir();
        }

        /*
        if(db.getVersion() < 2)
        {
            _dbHelper.onUpgrade(db, db.getVersion(), 2);
        }
        */

        mBusListView = (ListView)findViewById(R.id.busList_list);
        mBusListArray = new ArrayList<>();
        mBusListArray.clear();

        spCompanyList = (Spinner)findViewById(R.id.sp_company_list);
        mCompanyListArray = new ArrayList<>();
        mCompanyListArray.clear();

        // "GREY, RED, ORANGE, YELLOW, GREEN, BLUE"

        //company_name = "수원유치원";
        //company_code = "12AD24564";


        /*
        if(mTmpComp.size() == 0)
        {
            CompanyInfo mComp = new CompanyInfo();

            mComp.setCompanyName(company_name);
            mComp.setCompanyCode(company_code);
            _dbHelper.mComp.Add(mComp);

            mTmpComp = _dbHelper.mComp.Query(new CompanyInfoAll());
        }

        */

        mCompanyListArray.add(0, new ItemCompanyList(true, "",  ""));

//        if(_dbHelper.mComp.CheckDuplicate("캐리유치원") == false)
//        {
//            String[] new_comp = {"캐리유치원", "캐리유치원"};
//            saveNewCompany(new_comp);
//        }

        if(_dbHelper.mComp.CheckDuplicate("Trường mẫu giáo") == false)
        {
            String[] new_comp = {"Trường mẫu giáo", "Trường mẫu giáo"};
            saveNewCompany(new_comp);
        }


        List<CompanyInfo> mTmpComp = _dbHelper.mComp.Query(new CompanyInfoAll());
        Log.e(TAG, "company name : " + mTmpComp.size());
        /*
        if(mTmpComp.size() == 0)
        {
            String[] new_comp = {"캐리유치원", "캐리유치원"};
            saveNewCompany(new_comp);

            addNewCompany();
            mCompanyListArray.add(0, new ItemCompanyList(true, "input_new_company",  "신규 등록"));

        }
        */
//        company_name = "캐리유치원";
//        company_code = "캐리유치원";

        company_name = "Trường mẫu giáo";
        company_code = "Trường mẫu giáo";


        for(int i = 0; i< mTmpComp.size(); ++i)
        {
            Log.e(TAG, "company name : " + mTmpComp.get(i).getId() + " " + mTmpComp.get(i).getCompanyName() + " " + mTmpComp.get(i).getCompanyCode());

//            if(mTmpComp.get(i).getCompanyCode().equals("캐리유치원") == false)
//            {
//                List<BusInfo> mTmpBus = _dbHelper.mBus.QueryByComp(mTmpComp.get(i).getCompanyCode());
//                if(mTmpBus.size()>0) {
//                    for(int j = 0; j <mTmpBus.size(); ++j)
//                    {
//                        mTmpBus.get(j).setCompanyName("캐리유치원");
//                        mTmpBus.get(j).setCompanyCode("캐리유치원");
//                        _dbHelper.mBus.Update(mTmpBus.get(j));
//                    }
//                }
//              _dbHelper.mComp.Delete(mTmpComp.get(i));
//            }

            if(mTmpComp.get(i).getCompanyCode().equals("Trường mẫu giáo") == false)
            {
                List<BusInfo> mTmpBus = _dbHelper.mBus.QueryByComp(mTmpComp.get(i).getCompanyCode());
                if(mTmpBus.size()>0) {
                    for(int j = 0; j <mTmpBus.size(); ++j)
                    {
                        mTmpBus.get(j).setCompanyName("Trường mẫu giáo");
                        mTmpBus.get(j).setCompanyCode("Trường mẫu giáo");
                        _dbHelper.mBus.Update(mTmpBus.get(j));
                    }
                }
                _dbHelper.mComp.Delete(mTmpComp.get(i));
            }

            /*
            mCompanyListArray.add(0, new ItemCompanyList(false, mTmpComp.get(i).getCompanyCode(),  mTmpComp.get(i).getCompanyName()));
            company_name = mTmpComp.get(i).getCompanyName();
            company_code = mTmpComp.get(i).getCompanyCode();
            */
        }

//        mCompanyListArray.add(0, new ItemCompanyList(false, "캐리유치원",  "캐리유치원"));
        mCompanyListArray.add(0, new ItemCompanyList(false, "Trường mẫu giáo",  "Trường mẫu giáo"));
        mCompanyListAdapter = new AdapterCompanyList(getApplicationContext(), R.layout.layout_spinner_company_item, mCompanyListArray);

        /*
        for(int i=0; i<mTmpComp.size(); ++i)
        {
            Log.e(TAG, "---------- in DB, regi company_ : " + mTmpComp.get(i).getCompanyName() + " " + mTmpComp.get(i).getCompanyCode() );
        }

        Log.e(TAG, "in DB, regi company_ 0  : " + mTmpComp.get(0).getCompanyName() + " " + mTmpComp.get(0).getCompanyCode() );
        */

        //List<BusInfo> mTmpBus = _dbHelper.mBus.Query(new BusInfoAll());


        /*
        if(mTmpComp.size()> 0)
        {
            List<BusInfo> mTmpBus = _dbHelper.mBus.QueryByComp(mTmpComp.get(0).getCompanyCode());

            company_name = mTmpComp.get(0).getCompanyName();
            company_code = mTmpComp.get(0).getCompanyCode();


            for(int i=0; i<mTmpBus.size(); ++i)
            {
                Log.e(TAG, "---------- in DB, regi bus_ : " + mTmpBus.get(i).getCompanyName() + " " + mTmpBus.get(i).getBusSeat() + " / " + mTmpBus.get(i).getBusCode() );
            }

            mBusListArray.add(0, new ItemBusList(true, false, "", "", AdapterBusList.ADD_BUS));

            for(int i =0; i<mTmpBus.size(); ++i)
            {
                mBusListArray.add(0, new ItemBusList(false, false, mTmpBus.get(i).getBusName(), mTmpBus.get(i).getBusCode(), Integer.parseInt(mTmpBus.get(i).getBusColor())));
            }
        }
        */

        /*
        mBusListArray.add(0, new ItemBusList(false, false, "햇님반", "12가1234", AdapterBusList.GREY));
        mBusListArray.add(0, new ItemBusList(false, false, "달님반", "12나1234", AdapterBusList.ORANGE));
        mBusListArray.add(0, new ItemBusList(false, false, "꽃님반", "12다1234", AdapterBusList.RED));
        mBusListArray.add(0, new ItemBusList(false, false, "달님반", "12나1234", AdapterBusList.BLUE));
        mBusListArray.add(0, new ItemBusList(false, false, "꽃님반", "12다1234", AdapterBusList.YELLOW));
        mBusListArray.add(0, new ItemBusList(false, false, "꽃님반", "12다1234", AdapterBusList.GREEN));
        */

        btShare = findViewById(R.id.busListShareBt);
        btInfo = findViewById(R.id.busListInfoBt);
        btShareOk = findViewById(R.id.busListShareOkBt);
        btShareCancel = findViewById(R.id.busListShareCancelBt);

        btInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getApplicationContext(), SettingActivity.class);
                startActivity(mIntent);
            }
        });

        btShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                final String[] STRINGS = {"버스 정보 보내기","버스 정보 가져오기"};
                final String[] STRINGS = {"Xuất thông tin xe","Nhập thông tin xe"};
                final ListPopupWindow mPopupWindow = new ListPopupWindow(getApplicationContext());
                mPopupWindow.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, STRINGS));
                mPopupWindow.setVerticalOffset(-1 * v.getHeight());
                mPopupWindow.setAnchorView(v);
                mPopupWindow.setDropDownGravity(Gravity.RIGHT);
                mPopupWindow.setWidth(getApplicationContext().getResources().getDimensionPixelSize(R.dimen.overflow_width_2));
                mPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        mPopupWindow.dismiss();
                        if(position == 0)
                        {
                            btShare.setVisibility(View.GONE);
                            btInfo.setVisibility(View.GONE);
                            btShareOk.setVisibility(View.VISIBLE);
                            btShareCancel.setVisibility(View.VISIBLE);

                            mBusListAdapter.setCheckBoxEnable(true);
                            mBusListAdapter.notifyDataSetChanged();
                            mBusListView.invalidateViews();
                        }
                        else
                        {

                            searchFile();
                            //BusDataLoad();

                        }


                    }
                });

                mPopupWindow.show();

            }
        });

        btShareOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btShare.setVisibility(View.VISIBLE);
                btInfo.setVisibility(View.VISIBLE);
                btShareOk.setVisibility(View.GONE);
                btShareCancel.setVisibility(View.GONE);

                backUpBusData();

                mBusListAdapter.setCheckBoxEnable(false);
                mBusListAdapter.notifyDataSetChanged();
                mBusListView.invalidateViews();
            }
        });


        btShareCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btShare.setVisibility(View.VISIBLE);
                btInfo.setVisibility(View.VISIBLE);
                btShareOk.setVisibility(View.GONE);
                btShareCancel.setVisibility(View.GONE);

                mBusListAdapter.setCheckBoxEnable(false);
                mBusListAdapter.notifyDataSetChanged();
                mBusListView.invalidateViews();
            }
        });





        spCompanyList.setAdapter(mCompanyListAdapter);
        spCompanyList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(mCompanyListAdapter.getItem(position).getCompany_add()==true)
                {

                    /*
                    mBusListArray.clear();
                    mBusListAdapter.notifyDataSetInvalidated();
                    mBusListView.invalidateViews();
                    */

                    if(mCompanyListAdapter.getItem(position).getCompany_code().equals("input_new_company") == false)
                    {
                        spCompanyList.setSelection(selected_comp_position);
                        addNewCompany();
                    }

                    //return;

                }
                else
                {
                    mBusListArray.clear();
                    List<BusInfo> mTmpBus = _dbHelper.mBus.QueryByComp(mCompanyListAdapter.getItem(position).getCompany_code());

                    company_name = mCompanyListAdapter.getItem(position).getCompany_name();
                    company_code = mCompanyListAdapter.getItem(position).getCompany_code();

                    selected_comp_position = position;


                    for(int i=0; i<mTmpBus.size(); ++i)
                    {
                        Log.e(TAG, "---------- in DB, regi bus_ : " + mTmpBus.get(i).getId() + " / " + mTmpBus.get(i).getCompanyName() + " " + mTmpBus.get(i).getBusSeat() + " / " + mTmpBus.get(i).getBusCode() );
                    }


                    mBusListArray.add(0, new ItemBusList(true, false, "", "", AdapterBusList.ADD_BUS, company_name, company_code));

                    for(int i =0; i<mTmpBus.size(); ++i)
                    {

                        List<AlarmInfo> alarmInfo = _dbHelper.mAlarm.QueryByCode(mTmpBus.get(i).getBusName(), mTmpBus.get(i).getBusCode());
                        boolean alarmSet = false;
                        if(alarmInfo.size() != 0 ) {
                            if(alarmInfo.get(0).getAlarmAuto() == true) {
                                alarmSet = true;
                                Log.e(TAG, "---------- Alarm : " + mTmpBus.get(i).getBusName() + "  / " + alarmSet  +  " /  " + alarmInfo.get(0).getAlarmTime());
                            }
                        }

                        mBusListArray.add(0, new ItemBusList(false, alarmSet, mTmpBus.get(i).getBusName(), mTmpBus.get(i).getBusCode(), Integer.parseInt(mTmpBus.get(i).getBusColor()), company_name, company_code));
                    }

                    mBusListAdapter.notifyDataSetInvalidated();
                    mBusListView.invalidateViews();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        mBusListAdapter = new AdapterBusList(getApplicationContext(), this, this, R.layout.layout_buslist_item, mBusListArray, company_name, company_code);
        mBusListView.setAdapter(mBusListAdapter);
        mBusListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                //position -= 1; //list start index 1, array start index 0

                Log.e(TAG, "clicked " + mBusListAdapter.getItem(position).getBus_colar() + " " + mBusListAdapter.getItem(position).getBus_add() + " / " +  position);

                /*
                if(mBusListAdapter.getItem(position).getBus_add() == true)
                {
                    Intent i = new Intent(getApplicationContext(), NewBusAddActivity.class);
                    i.putExtra("company_name", company_name);
                    i.putExtra("company_code", company_code);
                    startActivityForResult(i, REQUEST_CODE_SET_BUS_INFO);

                }
                else
                {
                    List<BusInfo> selectedBus = _dbHelper.mBus.QueryByBusCode(mBusListAdapter.getItem(position).getBus_code());
                    Intent mIntent = new Intent(getApplicationContext(), BusSeatServiceActivity.class);
                    mIntent.putExtra("bus_name", selectedBus.get(0).getBusName());
                    mIntent.putExtra("bus_code", selectedBus.get(0).getBusCode());
                    mIntent.putExtra("bus_seat", selectedBus.get(0).getBusSeat());
                    startActivity(mIntent);


                    Log.e(TAG, "selected bus " + selectedBus.get(0).getBusCode() + " / " + selectedBus.get(0).getBusName() + " / " + selectedBus.get(0).getBusSeat() + " / "+  position);
                    List<SeatInfo> tmp = _dbHelper.mSeat.QuerybyCode(mBusListAdapter.getItem(position).getBus_code());

                    for(int i=0; i<tmp.size(); ++i)
                    {
                        Log.e(TAG, "in DB, regi seat_ : " +  " " + tmp.get(i).getSeatNumber() + " / " + tmp.get(i).getSeatPosition() + "/ "+ tmp.get(i).getBusCode() +"/ " + tmp.get(i).getBusSeat() );
                    }

                }
                */
            }
        });

    }

    private void saveNewCompany(String[] info)
    {
        CompanyInfo mComp = new CompanyInfo();

        mComp.setCompanyName(info[0]);
        mComp.setCompanyCode(info[1]);
        _dbHelper.mComp.Add(mComp);

        /*
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
        */
    }

    public void refreshBusList()
    {
        mBusListArray.clear();
        mBusListArray.add(0, new ItemBusList(true, false, "", "", AdapterBusList.ADD_BUS, company_name, company_code));

        List<BusInfo> mTmpBus = _dbHelper.mBus.QueryByComp(company_code);
        for (int i = 0; i < mTmpBus.size(); ++i) {

            List<AlarmInfo> alarmInfo = _dbHelper.mAlarm.QueryByCode(mTmpBus.get(i).getBusName(), mTmpBus.get(i).getBusCode());
            boolean alarmSet = false;
            if(alarmInfo.size() != 0 ) {
                if(alarmInfo.get(0).getAlarmAuto() == true) {
                    alarmSet = true;
                    Log.e(TAG, "---------- Alarm : " + mTmpBus.get(i).getBusName() + "  / " + alarmSet  +  " /  " + alarmInfo.get(0).getAlarmTime());
                }
            }

            mBusListArray.add(0, new ItemBusList(false,  alarmSet, mTmpBus.get(i).getBusName(), mTmpBus.get(i).getBusCode(), Integer.parseInt(mTmpBus.get(i).getBusColor()), company_name, company_code));
        }

        mBusListAdapter.notifyDataSetInvalidated();
        mBusListView.invalidateViews();
    }


    private void addNewCompany()
    {
        final NewCompAddDialog mAddNewComp = new NewCompAddDialog(this);
        //--final NewCompAddDialog mAddNewComp = new NewCompAddDialog(this, R.style.MaterialDialogSheet);
        //--mAddNewComp.getWindow().setGravity(Gravity.BOTTOM);
        mAddNewComp.show();
        mAddNewComp.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                String[] tmp = mAddNewComp.getNewComInfomation();
                if(tmp[0].isEmpty() == false)
                {
                    Log.e(TAG, "New Comp : "  + tmp[0] + " / " + tmp[1]);

                    if(_dbHelper.mComp.CheckDuplicate(tmp[1]) == false)
                    {
                        saveNewCompany(tmp);
                    }
                    else
                    {
//                        Log.e(TAG, "Existed Comp!!");
//                        AlertDialog.Builder alt_bld = new AlertDialog.Builder(MainActivity.this);
//                        alt_bld.setTitle("이미 존재하는 시설명입니다");
//                        alt_bld.setMessage("새로운 이름으로 다시 입력해주세요.").setCancelable(
//                                false).setPositiveButton("확인",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.cancel();
//                                    }
//                                });
//                        AlertDialog alert = alt_bld.create();
//                        alert.show();

                        Log.e(TAG, "Existed Comp!!");
                        AlertDialog.Builder alt_bld = new AlertDialog.Builder(MainActivity.this);
                        alt_bld.setTitle("Tên trường đã được sử dụng");
                        alt_bld.setMessage("Vui lòng nhập tên trường mới.").setCancelable(
                                false).setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = alt_bld.create();
                        alert.show();
                    }

                }

            }
        });

    }




    public void onResume() {
        super.onResume();

        Log.e(TAG, "onResume()");

        mBusListArray.clear();
        mBusListArray.add(0, new ItemBusList(true, false, "", "", AdapterBusList.ADD_BUS, company_name, company_code));

        List<BusInfo> mTmpBus = _dbHelper.mBus.QueryByComp(company_code);
        for (int i = 0; i < mTmpBus.size(); ++i) {

            List<AlarmInfo> alarmInfo = _dbHelper.mAlarm.QueryByCode(mTmpBus.get(i).getBusName(), mTmpBus.get(i).getBusCode());
            boolean alarmSet = false;
            if(alarmInfo.size() != 0 ) {
                if(alarmInfo.get(0).getAlarmAuto() == true) {
                    alarmSet = true;
                    Log.e(TAG, "---------- Alarm : " + mTmpBus.get(i).getBusName() + "  / " + alarmSet  +  " /  " + alarmInfo.get(0).getAlarmTime());
                }
            }

            mBusListArray.add(0, new ItemBusList(false,  alarmSet, mTmpBus.get(i).getBusName(), mTmpBus.get(i).getBusCode(), Integer.parseInt(mTmpBus.get(i).getBusColor()), company_name, company_code));
        }


        mBusListAdapter.notifyDataSetInvalidated();
        mBusListView.invalidateViews();
    }


    private void searchFile()
    {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent, REQUEST_CODE_BACKUP_FILE_CHOOSE);
    }

    private void BusDataLoad(File file, ArrayList<LoadBusInfoDialog.LoadBusItem> mSelectedBus)
    {

        JSONParser parser = new JSONParser();
        try {
            JSONArray jsonArray = null;

            try {
                JSONObject jsonObject = (JSONObject)parser.parse(new FileReader(file.getAbsoluteFile().getPath()));
                jsonArray = (JSONArray) jsonObject.get("bus");

            } catch (ParseException e) {
                e.printStackTrace();
            }

            for(Object object : jsonArray)
            {
                JSONObject bus = (JSONObject)object;
                BusInfo mBus = new BusInfo();

                for(int i =0; i < mSelectedBus.size(); ++i)
                {
                    if(((String)bus.get("busName")).equals(mSelectedBus.get(i).getBusName()) == true && ((String)bus.get("busCode")).equals(mSelectedBus.get(i).getBusCode())==true)
                    {
                        if(mSelectedBus.get(i).getSelected() == true)
                        {
                            mBus.setCompanyName(company_name);
                            mBus.setCompanyCode(company_code);
                            mBus.setBusName((String)bus.get("busName"));
                            mBus.setBusCode((String)bus.get("busCode"));
                            mBus.setBusColor((String)bus.get("busColor"));
                            mBus.setBusSeat((String)bus.get("busSeat"));

                            if(_dbHelper.mBus.CheckDuplicate(mBus.getBusName(), mBus.getBusCode()) == true)
                            {
//                                Log.e("AdpaterBusSeat", "Already resisted number" );
//                                AlertDialog.Builder alt_bld = new AlertDialog.Builder(MainActivity.this);
//                                alt_bld.setTitle("이미 등록된 차량 정보가 있습니다");
//                                alt_bld.setMessage("기존 " + mBus.getBusName() +  "(" + mBus.getBusCode() + ")" + " 차량 정보를 수정하거나 삭제해주세요.").setCancelable(
//                                        false).setPositiveButton("확인",
//                                        new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.cancel();
//                                            }
//                                        });
//                                AlertDialog alert = alt_bld.create();
//                                alert.show();
//                                break;

                                Log.e("AdpaterBusSeat", "Already resisted number" );
                                AlertDialog.Builder alt_bld = new AlertDialog.Builder(MainActivity.this);
                                alt_bld.setTitle("Tên xe hoặc biển số xe này đã được đăng ký");
                                alt_bld.setMessage("Đang thoát " + mBus.getBusName() +  "(" + mBus.getBusCode() + ")" + " Vui lòng sửa hoặc xóa thông tin xe.").setCancelable(
                                        false).setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                AlertDialog alert = alt_bld.create();
                                alert.show();
                                break;
                            }

                            Log.e(TAG, "---------------------------------------------------------------------------");
                            Log.e(TAG, "Bus : " +  mBus.getCompanyName() + " " + mBus.getCompanyCode() +  " " + mBus.getBusName() +  " " + mBus.getBusCode() +  "  "  + mBus.getBusColor() +  "  "  + mBus.getBusSeat());
                            Log.e(TAG, "---------------------------------------------------------------------------");

                            _dbHelper.mBus.Add(mBus);

                            JSONArray seats  = (JSONArray)bus.get("seats");
                            for(Object object_seat : seats)
                            {
                                JSONObject seat = (JSONObject)object_seat;
                                SeatInfo mSeat = new SeatInfo();

                                mSeat.setBusName((String)seat.get("busName"));
                                mSeat.setBusCode((String)seat.get("busCode"));
                                mSeat.setBusSeat((String)seat.get("busSeat"));
                                mSeat.setSeatPosition((String)seat.get("seatPosition"));
                                mSeat.setSeatNumber((String)seat.get("seatNumber"));
                                mSeat.setSeatStatus("status_empty");
                                mSeat.setSeatBattery((String)seat.get("seatBattery"));

                                Log.e(TAG, "Seat : " + mSeat.getBusName() +  " " + mSeat.getBusCode() +  "  " + mSeat.getBusSeat()  +  " " + mSeat.getSeatPosition()  +  "  "  + mSeat.getSeatNumber()  +  "  " + mSeat.getSeatStatus() +  "  "  + mSeat.getSeatBattery());

                                _dbHelper.mSeat.Add(mSeat);
                            }

                            Log.e(TAG, "---------------------------------------------------------------------------");

                         }
                    }
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        refreshBusList();
    }



    private void  backUpBusData()
    {
        //timestamp = new Timestamp(System.currentTimeMillis());


        dataFile = new File(sdDirectory,  DATA_FILE_NAME + System.currentTimeMillis() + DARA_FILE_TYPE);

        if(dataFile.isFile()) {
            Log.e(TAG, "REMAINED FILE " + dataFile.getAbsolutePath());
            dataFile.delete();
        }

        try {
            if (dataFile.createNewFile()) {
                Log.e(TAG, "NEW FILE " + dataFile.getAbsolutePath());
            } else
                Log.e(TAG, "FAIL MAKE NEW FILE");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            dataOutputStream = new FileOutputStream(dataFile, true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int checkedItems = 0;
        for(int i =0; i < mBusListAdapter.getCount(); ++i)
        {
            if(mBusListAdapter.getItem(i).getBus_checked() == true)
            {
                ++checkedItems;
            }
        }

        Log.e(TAG, "checkedItems " +  checkedItems );


        try {
            String startString = "{ \"bus\":[" ;
            dataOutputStream.write(startString.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        int cntItems = 0;
        for(int i =0; i < mBusListAdapter.getCount(); ++i)
        {
            if(mBusListAdapter.getItem(i).getBus_checked() == true)
            {
                Log.e(TAG, "Selected Bus " +  mBusListAdapter.getItem(i).getBus_name() + " / " + mBusListAdapter.getItem(i).getBus_code() );
                //JSONArray jsonArry = _dbHelper.mBus.getBackupBusInfo(mBusListAdapter.getItem(i).getBus_name(), mBusListAdapter.getItem(i).getBus_code());

                String result = _dbHelper.mBus.getBackupInfo(mBusListAdapter.getItem(i).getBus_name(), mBusListAdapter.getItem(i).getBus_code()) + "\n";

                Log.e(TAG, result);

                try {
                    dataOutputStream.write(result.getBytes());


                    if(cntItems == (checkedItems-1)) {
                        Log.e(TAG, "END " +  cntItems );
                    }
                    else {
                        Log.e(TAG, "cntItems " +  cntItems );
                        String betweenString = "," ;
                        dataOutputStream.write(betweenString.getBytes());
                        ++cntItems;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            String endString = "] }" ;
            dataOutputStream.write(endString.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            dataOutputStream.flush();
            dataOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if( dataFile.exists() == true)
        {
            Log.e(TAG, "file : " + dataFile.getAbsolutePath());
            //Uri path = Uri.fromFile(dataFile);
            Uri path = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", dataFile);
            Log.e(TAG, "this.getApplicationContext().getPackageName() : " + this.getApplicationContext().getPackageName());
            Intent intentSend  = new Intent(Intent.ACTION_SEND);
            intentSend.setType("text/plain");
            intentSend.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intentSend.putExtra(Intent.EXTRA_STREAM,  path);
//            startActivity(Intent.createChooser(intentSend, "공유"));
            startActivity(Intent.createChooser(intentSend, "Chia sẻ"));
        }
        else
        {
            Log.e(TAG, "There is no file : " + dataFile.getAbsolutePath());
        }


        /*
        Gson gson = new Gson();

        try {
            JsonReader reader = new JsonReader(new FileReader(dataFile.getAbsoluteFile().getPath()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        */

        /*
        JSONParser parser = new JSONParser();
        try {
            JSONArray jsonArray = null;

            try {

                JSONObject jsonObject = (JSONObject)parser.parse(new FileReader(dataFile.getAbsoluteFile().getPath()));

                jsonArray = (JSONArray) jsonObject.get("bus");

            } catch (ParseException e) {
                e.printStackTrace();
            }

            for(Object object : jsonArray)
            {
                JSONObject bus = (JSONObject)object;
                BusInfo mBus = new BusInfo();

                mBus.setCompanyName(company_name);
                mBus.setCompanyCode(company_code);
                mBus.setBusName((String)bus.get("busName"));
                mBus.setBusCode((String)bus.get("busCode"));
                mBus.setBusColor((String)bus.get("busColor"));
                mBus.setBusSeat((String)bus.get("busSeat"));


                Log.e(TAG, "---------------------------------------------------------------------------");
                Log.e(TAG, "Bus : " +  mBus.getCompanyName() + " " + mBus.getCompanyCode() +  " " + mBus.getBusName() +  " " + mBus.getBusCode() +  "  "  + mBus.getBusColor() +  "  "  + mBus.getBusSeat());
                Log.e(TAG, "---------------------------------------------------------------------------");

                JSONArray seats  = (JSONArray)bus.get("seats");
                for(Object object_seat : seats)
                {
                    JSONObject seat = (JSONObject)object_seat;
                    SeatInfo mSeat = new SeatInfo();

                    mSeat.setBusName((String)seat.get("busName"));
                    mSeat.setBusCode((String)seat.get("busCode"));
                    mSeat.setBusSeat((String)seat.get("busSeat"));
                    mSeat.setSeatPosition((String)seat.get("seatPosition"));
                    mSeat.setSeatNumber((String)seat.get("seatNumber"));
                    mSeat.setSeatStatus((String)seat.get("seatStatus"));
                    mSeat.setSeatBattery("seatBattery");

                    Log.e(TAG, "Seat : " + mSeat.getBusName() +  " " + mSeat.getBusCode() +  "  " + mSeat.getBusSeat()  +  " " + mSeat.getSeatPosition()  +  "  "  + mSeat.getSeatNumber()  +  "  " + mSeat.getSeatStatus() +  "  "  + mSeat.getSeatBattery());
                }

                Log.e(TAG, "---------------------------------------------------------------------------");

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        */



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if((resultCode == RESULT_OK) && (requestCode == REQUEST_CODE_SET_BUS_INFO)) {
            Log.e(TAG, "result : " + data.getStringExtra("result"));

            if(data.getStringExtra("result").equals("add")) {
                mBusListArray.clear();
                mBusListArray.add(0, new ItemBusList(true, false, "", "", AdapterBusList.ADD_BUS, company_name, company_code));

                List<BusInfo> mTmpBus = _dbHelper.mBus.QueryByComp(company_code);
                for (int i = 0; i < mTmpBus.size(); ++i) {
                    mBusListArray.add(0, new ItemBusList(false, false, mTmpBus.get(i).getBusName(), mTmpBus.get(i).getBusCode(), Integer.parseInt(mTmpBus.get(i).getBusColor()), company_name, company_code));
                }

                mBusListAdapter.notifyDataSetInvalidated();
                mBusListView.invalidateViews();

            }
            /*else if(data.getStringExtra("result").equals("new_company"))
            {

            }
            */
        }else if((resultCode == RESULT_OK) && (requestCode == REQUEST_CODE_BACKUP_FILE_CHOOSE))
        {

            Uri uri = data.getData();
            String path = getPath(getApplicationContext(), uri);
            Log.e(TAG, "Chosen file : " + path);

            final File backupFile = new File(path);

            final LoadBusInfoDialog mLoadBusinfoDialog = new  LoadBusInfoDialog(this, backupFile);
            mLoadBusinfoDialog.show();
            mLoadBusinfoDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    if(mLoadBusinfoDialog.getResult() == true)
                    {
                        ArrayList<LoadBusInfoDialog.LoadBusItem> mSelectedBus = mLoadBusinfoDialog.getSelectedList();
                        if(mSelectedBus.size() > 0)
                        {
                            for(int i=0; i< mSelectedBus.size(); ++i)
                            {
                                if(mSelectedBus.get(i).getSelected() == true)
                                {
                                    Log.e(TAG, "Selected Bus : " + mSelectedBus.get(i).getBusName() + " " +  mSelectedBus.get(i).getBusCode());
                                }
                            }
                            if (backupFile.exists()) {
                                Log.e(TAG, "file Ok ");
                                BusDataLoad(backupFile, mSelectedBus);
                            } else {
                                Log.e(TAG, "file No ");
                            }

                        }
                    }
                }
            });



        }
    }

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


}
