package kidsoft.buscare.BusList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kidsoft.buscare.BaseActivity;
import kidsoft.buscare.R;
import layout.BusSetting.SettingAdapter;
import layout.BusSetting.ItemContact;
import layout.BusSetting.ItemDisplay;
import layout.BusSetting.ItemOpenSource;
import layout.BusSetting.SettingModel;

public class SettingActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private SettingAdapter mSettingAdapter;
    private ArrayList<SettingModel> items;
    private TextView mVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        mVersion = findViewById(R.id.settingInfo_version);
        recyclerView = findViewById(R.id.setting_list);
        items = new ArrayList<SettingModel>();

        items.add(new ItemDisplay());
        items.add(new ItemOpenSource());
        items.add(new ItemContact());

        mSettingAdapter = new SettingAdapter(this, this, items);

        DividerItemDecoration divider = new
                DividerItemDecoration(recyclerView.getContext(),
                DividerItemDecoration.VERTICAL);

        divider.setDrawable(ContextCompat.getDrawable(this, R.drawable.line_grey_divider));
        recyclerView.addItemDecoration(divider);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mSettingAdapter);

        try {
//            mVersion.setText("설치버전       "+getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
            mVersion.setText("Phiên bản       "+getPackageManager().getPackageInfo(getPackageName(), 0).versionName);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }
}
