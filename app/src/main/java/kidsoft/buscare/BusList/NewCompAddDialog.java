package kidsoft.buscare.BusList;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import kidsoft.buscare.R;

public class NewCompAddDialog extends Dialog {

    String[] compInfo;

    EditText ebCompanyName;
    Button btOK, btCancel;

    public NewCompAddDialog(@NonNull Context context) {
        super(context);
    }

    public NewCompAddDialog(MainActivity mainActivity, int materialDialogSheet) {
        super(mainActivity, materialDialogSheet);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_new_company_add);

        compInfo = new String[2];
        compInfo[0] = ""; // comp name
        compInfo[1] = ""; // comp code

        ebCompanyName = findViewById(R.id.nc_add_name);
        btOK = findViewById(R.id.nc_add_ok);
        btCancel = findViewById(R.id.nc_add_cancel);

        btOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compInfo[0] = ebCompanyName.getText().toString();
                compInfo[1] = compInfo[0];
                dismiss();
            }
        });


        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


    }

    public String[] getNewComInfomation()
    {
        return compInfo;
    }
}
