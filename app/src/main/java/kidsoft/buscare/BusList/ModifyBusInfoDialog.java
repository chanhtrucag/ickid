package kidsoft.buscare.BusList;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import kidsoft.buscare.R;

public class ModifyBusInfoDialog extends Dialog {

    private EditText ebBusName, ebBusCode;
    private Button btOk;
    private ImageView ibClose;

    private String busName, busCode;
    private boolean result ;


    public ModifyBusInfoDialog(@NonNull Context context) {
        super(context);
    }

    public ModifyBusInfoDialog(Context context, String busName, String busCode) {
        super(context);

        this.busName = busName;
        this.busCode = busCode;
    }

    public ModifyBusInfoDialog(MainActivity mainActivity, int materialDialogSheet) {
        super(mainActivity, materialDialogSheet);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_modify_bus_info);

        ebBusName = findViewById(R.id.bm_bus_name_eb);
        ebBusCode = findViewById(R.id.bm_bus_code_eb);

        ebBusName.setText(busName);
        ebBusCode.setText(busCode);

        btOk = findViewById(R.id.bm_ok);
        ibClose = findViewById(R.id.bm_close);

        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = false;
                dismiss();
            }
        });

        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(busName.equals(ebBusName.getText().toString()) && busCode.equals(ebBusCode.getText().toString()))
                {
                    result = false;
                }
                else
                {
                    result = true;
                    busName = ebBusName.getText().toString();
                    busCode = ebBusCode.getText().toString();
                }

                dismiss();
            }
        });

    }

    public boolean getResult()
    {
        return result;
    }

    public String[] getNewBusInfo()
    {
        String[] busInfo = new String[2];
        busInfo[0] = busName;
        busInfo[1] = busCode;

        return busInfo;
    }
}
