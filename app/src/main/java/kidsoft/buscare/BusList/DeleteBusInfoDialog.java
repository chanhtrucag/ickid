package kidsoft.buscare.BusList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import kidsoft.buscare.R;

public class


DeleteBusInfoDialog extends Dialog {

    private String bus_name, bus_code;

    private TextView tvTitle;
    private Button btOk, btClose;
    private boolean result;

    public DeleteBusInfoDialog(@NonNull Context context) {
        super(context);
    }

    public DeleteBusInfoDialog(Context context, String bus_name, String bus_code) {
        super(context);

        this.bus_name = bus_name;
        this.bus_code = bus_code;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_delete_bus_info);

        tvTitle = findViewById(R.id.bd_title);
//        tvTitle.setText(bus_name + "(" + bus_code + ")" + "를 정말 삭제하시겠습니까?");
        tvTitle.setText(bus_name + "(" + bus_code + ")" + "Xác nhận xóa?");

        btOk = findViewById(R.id.bd_ok);
        btClose = findViewById(R.id.bd_cancel);

        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = true;
                dismiss();
            }
        });

        btClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = false;
                dismiss();
            }
        });

    }

    public boolean getResult()
    {
        return result;
    }
}
