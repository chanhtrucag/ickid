package kidsoft.buscare.BusList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import kidsoft.buscare.BaseActivity;
import kidsoft.buscare.R;

public class InfoActivity extends BaseActivity {

    private TextView mVersion, mOpenInfo;
    private Switch swAlways;
    private SharedPreferences settings;
    private boolean flagAlways;
    private ImageView imgOptInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        settings = getSharedPreferences("settings", MODE_PRIVATE);

        mVersion = findViewById(R.id.info_app_version);
        mOpenInfo = findViewById(R.id.info_open_licence);
        swAlways = findViewById(R.id.option_always_switch);
        imgOptInfo = findViewById(R.id.option_info);

        flagAlways = settings.getBoolean("DisplayAlwaysOn", false);
        swAlways.setChecked(flagAlways);

        swAlways.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor prefEditor = settings.edit();
                prefEditor.putBoolean("DisplayAlwaysOn", isChecked);
                prefEditor.apply();
            }
        });


        imgOptInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AlertDialog.Builder alt_bld = new AlertDialog.Builder(InfoActivity.this);
//                alt_bld.setTitle("항상 화면 켜짐");
//                alt_bld.setMessage("항상 화면 켜짐을 실행하면 차량 이동중에 자리 이탈 발생시 알람이 울립니다. 실행시에는 배터리 소모가 증가합니다.").setCancelable(
//                        false).setPositiveButton("확인",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.cancel();
//                            }
//                        });
//                AlertDialog alert = alt_bld.create();
//                alert.show();

                AlertDialog.Builder alt_bld = new AlertDialog.Builder(InfoActivity.this);
                alt_bld.setTitle("Luôn mở màn hình");
                alt_bld.setMessage("Luôn mở màn hình để theo dõi chi tiết sự thay đổi trạng thái của gối. Thiết bị sẽ tiêu thụ nhiều pin hơn khi mở chế độ này.").setCancelable(
                        false).setPositiveButton("Thành công",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = alt_bld.create();
                alert.show();
            }
        });


        try {
            mVersion.setText(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        mOpenInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mIntent = new Intent(getApplicationContext(), LicenceActivity.class);
                startActivity(mIntent);

            }
        });



    }
}
