package kidsoft.buscare.BusList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kidsoft.buscare.Database.models.BusInfo;
import kidsoft.buscare.R;

public class LoadBusInfoDialog extends Dialog {

    private static final String TAG = "LoadBusInfoDialog ";

    private boolean result = true;
    private File path;

    private AdapterLoadBusInfo mBusListAdapter;
    private ListView mLoadBusList;
    private ArrayList<LoadBusItem> mLoadBus;
    private Context context;
    private Button btOk, btCancel;


    public LoadBusInfoDialog(@NonNull Context context, File path) {
        super(context);
        this.context = context;
        this.path = path;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_load_bus_data);

        mLoadBusList = findViewById(R.id.ibd_busList);
        mLoadBus = new ArrayList<>();

        btOk = findViewById(R.id.lbd_ok);
        btCancel = findViewById(R.id.lbd_cancel);

        JSONParser parser = new JSONParser();
        String busName, busCode;

        Log.e(TAG, "path "  +  path.getAbsoluteFile().getPath());

        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = false;
                dismiss();
            }
        });




        try {
            JSONArray jsonArray = null;

            try {
                JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(path.getAbsoluteFile().getPath()));
                jsonArray = (JSONArray) jsonObject.get("bus");

            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(jsonArray == null)
            {
                result = false;
                return;
            }


            for (Object object : jsonArray) {
                JSONObject bus = (JSONObject) object;
                BusInfo mBus = new BusInfo();

                busName = (String) bus.get("busName");
                busCode = (String) bus.get("busCode");

                Log.e(TAG, "Bus "  +  busName  +  "  "  +  busCode);

                mLoadBus.add(0, new LoadBusItem(false, busName, busCode));

            }
        }catch (IOException e) {
            e.printStackTrace();
        }


        mBusListAdapter = new AdapterLoadBusInfo(context, R.layout.layout_load_bus_item, mLoadBus);
        mLoadBusList.setAdapter(mBusListAdapter);

        Log.e(TAG, "List "  + mBusListAdapter.getSize() );

        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = true;
                dismiss();
            }
        });



        //mBusListAdapter.notifyDataSetInvalidated();
        //mLoadBusList.invalidate();
    }


    public boolean getResult()
    {
        return result;
    }

    public ArrayList<LoadBusItem> getSelectedList()
    {
        return  mBusListAdapter.getList();
    }


    public class LoadBusItem {

        public boolean selected = false;
        public String busName;
        public String busCode;

        public LoadBusItem(boolean selected, String busName, String busCode) {

            this.selected = selected;
            this.busName = busName;
            this.busCode = busCode;
        }


        public boolean getSelected(){return  selected;}
        public String getBusName(){ return busName;}
        public String getBusCode(){ return busCode;}
    }

    public class AdapterLoadBusInfo extends ArrayAdapter<LoadBusItem> {

        private ArrayList<LoadBusItem> itemList = new ArrayList<>();

        private  class ItemViewHolder {

            public CheckBox checkBox;
            public TextView busInfo;

        }

        public AdapterLoadBusInfo(@NonNull Context context, int resource, ArrayList<LoadBusItem> itemList) {
            super(context, resource, itemList);

            this.itemList = itemList;

            //Log.e(TAG, "AdapterLoadBusInfo "  +  itemList.size());

        }

        public ArrayList<LoadBusItem> getList()
        {
            return itemList;
        }


        public int getSize()
        {
            return itemList.size();
        }

        @NonNull
        public View getView(int position, final View convertView, @NonNull ViewGroup parent)
        {
            View row = convertView;
            ItemViewHolder viewHolder;

            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.layout_load_bus_item, parent, false);
                viewHolder = new ItemViewHolder();
                viewHolder.checkBox = row.findViewById(R.id.lb_item_checkBox);
                viewHolder.busInfo = row.findViewById(R.id.lb_item_info);
                row.setTag(viewHolder);

            }else {
                viewHolder = (ItemViewHolder) row.getTag();
            }

            final LoadBusItem item = itemList.get(position);

            viewHolder.busInfo.setText(item.getBusName() + "(" + item.getBusCode() + ")");
            viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    item.selected = isChecked;
                    Log.e(TAG, "Clicked checkBox "  +  item.getBusName() +  "  " +  item.getBusCode());
                }
            });


            if(item.getSelected() == true)
            {
                viewHolder.checkBox.setChecked(true);
            }
            else
            {
                viewHolder.checkBox.setChecked(false);
            }

            return row;
        }
    }

}
