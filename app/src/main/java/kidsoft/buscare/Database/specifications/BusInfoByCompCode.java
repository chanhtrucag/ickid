package kidsoft.buscare.Database.specifications;

import kidsoft.buscare.Database.models.CompanyInfo;

/**
 * Created by DELL3 on 2018-11-06.
 */

public class BusInfoByCompCode implements SqlSpecification {

    private String company_code;

    public  BusInfoByCompCode(String company_code)
    {
        this.company_code = company_code;
    }


    @Override
    public String toSqlQuery() {
        return String.format("SELECT * FROM %s WHERE company_code = %s;", CompanyInfo.TABLE_NAME, this.company_code);
    }
}
