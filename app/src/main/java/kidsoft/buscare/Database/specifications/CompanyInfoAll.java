package kidsoft.buscare.Database.specifications;

import kidsoft.buscare.Database.models.CompanyInfo;

/**
 * Created by DELL3 on 2018-11-06.
 */

public class CompanyInfoAll implements SqlSpecification{
    @Override
    public String toSqlQuery() {
        return "SELECT * FROM "+ CompanyInfo.TABLE_NAME +";";
    }
}
