package kidsoft.buscare.Database.specifications;

import kidsoft.buscare.Database.models.BusInfo;

/**
 * Created by DELL3 on 2018-11-06.
 */

public class BusInfoAll implements SqlSpecification {
    @Override
    public String toSqlQuery() {
        return "SELECT * FROM "+ BusInfo.TABLE_NAME +";";
    }
}
