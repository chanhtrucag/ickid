package kidsoft.buscare.Database.specifications;

import kidsoft.buscare.Database.models.SeatInfo;

/**
 * Created by DELL3 on 2018-11-06.
 */

public class SeatInfoByBusCode implements SqlSpecification {

    private String busCode;

    public SeatInfoByBusCode(String busCode){
        this.busCode = busCode;
    }

    @Override
    public String toSqlQuery() {
        return String.format("SELECT * FROM %s WHERE bus_code=%s;",
                SeatInfo.TABLE_NAME,
                this.busCode);
    }
}
