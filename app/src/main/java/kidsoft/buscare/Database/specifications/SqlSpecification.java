package kidsoft.buscare.Database.specifications;

/**
 * Created by Rene Heinsen on 3/6/2017.
 */

public interface SqlSpecification {
    String toSqlQuery();
}
