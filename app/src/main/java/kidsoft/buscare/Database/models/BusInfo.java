package kidsoft.buscare.Database.models;

import java.util.List;

/**
 * Created by DELL3 on 2018-11-05.
 */

public class BusInfo {

    private Integer id = 0;
    private String companyName;
    private String companyCode;
    private String busName;
    private String busCode;
    private String busColor;
    private String busSeat;
    private List<SeatInfo> seats;

    public BusInfo()
    {

    }

    public BusInfo(String companyName, String companyCode, String busName, String busCode, String busColor, String busSeat)
    {
        this.id = 0 ;
        this.companyName = companyName;
        this.companyCode = companyCode;
        this.busName = busName;
        this.busCode = busCode;
        this.busColor = busColor;
        this.busSeat = busSeat;
    }

    public BusInfo(Integer id, String companyName, String companyCode, String busName, String busCode, String busColor, String busSeat)
    {
        this.id = id ;
        this.companyName = companyName;
        this.companyCode = companyCode;
        this.busName = busName;
        this.busCode = busCode;
        this.busColor = busColor;
        this.busSeat = busSeat;
    }

    public BusInfo(Integer id, String companyName, String companyCode, String busName, String busCode, String busColor, String busSeat, List<SeatInfo> seats)
    {
        this.id = id ;
        this.companyName = companyName;
        this.companyCode = companyCode;
        this.busName = busName;
        this.busCode = busCode;
        this.busColor = busColor;
        this.busSeat = busSeat;

        this.seats = seats;
    }


    public Integer getId() {
        return id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public String getBusName(){
        return busName;
    }

    public String getBusCode() {
        return busCode;
    }

    public String getBusColor() {
        return busColor;
    }

    public String getBusSeat() {
        return busSeat;
    }

    public List<SeatInfo> getSeats(){return seats;}






    public void setId(Integer id) {
        this.id = id;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public void setBusName(String busName){
        this.busName = busName;
    }

    public void setBusCode(String busCode) {
        this.busCode = busCode;
    }

    public void setBusColor(String busColor) {
        this.busColor = busColor;
    }

    public void setBusSeat(String busSeat) {
        this.busSeat = busSeat;
    }

    public void setSeats(List<SeatInfo> seats){this.seats = seats;}


    public static final String TABLE_NAME = "BusInfo";
    public static final String CREATE_TABLE_QUERY = "CREATE TABLE " + TABLE_NAME + "(" +
            "Id INTEGER PRIMARY KEY," +
            "company_name TEXT NOT NULL," +
            "company_code TEXT NOT NULL," +
            "bus_name TEXT NOT NULL," +
            "bus_code TEXT NOT NULL," +
            "bus_color TEXT NOT NULL," +
            "bus_seat TEXT NOT NULL);" +
            "CREATE INDEX bus_code_index ON" + TABLE_NAME + " (bus_code);";

    public static final String DROP_TABLE_QUERY = "DROP TABLE IF EXISTS " + TABLE_NAME;
    public static final String TRUNCATE_TABLE_QUERY = "DELETE FROM " + TABLE_NAME + "; " +  "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = '" + TABLE_NAME + "';";
    public static final String DELETE_NOT_IN = "DELETE FROM " + TABLE_NAME + " WHERE Id NOT IN(%s);";


}
