package kidsoft.buscare.Database.models;

/**
 * Created by DELL3 on 2018-11-05.
 */

public class CompanyInfo {
    private Integer id = 0;
    private String companyName;
    private String companyCode;

    public CompanyInfo() {

    }

    public CompanyInfo(String companyName, String companyCode) {
        this.id = 0;
        this.companyName = companyName;
        this.companyCode = companyCode;
    }

    public CompanyInfo(Integer id, String companyName, String companyCode) {
        this.id = id;
        this.companyName = companyName;
        this.companyCode = companyCode;
    }

    public Integer getId() {
        return id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public static final String TABLE_NAME = "CompanyInfo";
    public static final String CREATE_TABLE_QUERY = "CREATE TABLE " + TABLE_NAME + "(" +
            "Id INTEGER PRIMARY KEY," +
            "company_name TEXT NOT NULL," +
            "company_code TEXT NOT NULL);" +
            "CREATE INDEX company_code_index ON" + TABLE_NAME + " (company_code);";

    public static final String DROP_TABLE_QUERY = "DROP TABLE IF EXISTS " + TABLE_NAME;
    public static final String UPDATE_NEW_COMPANY = "UPDATE " + TABLE_NAME + " SET is_new_company = 0 WHERE Id IN (%s);";
    public static final String DELETE_NOT_IN = "DELETE FROM " + TABLE_NAME + " WHERE Id NOT IN(%s);";


}

