package kidsoft.buscare.Database.models;

import java.util.Date;

/**
 * Created by DELL3 on 2018-11-05.
 */

public class AlarmInfo {

    private Integer id = 0;
    private String busName;
    private String busCode;
    private Date   alarmDate;
    private String alarmDays;
    private String alarmTime;
    private Boolean alarmAuto;
    private Boolean alarmRepeat;
    private Boolean alarmVib;
    private Integer alarmVolume;

    public AlarmInfo()
    {

    }


    public AlarmInfo(String busName, String busCode, Date   alarmDate, String alarmDays, String alarmTime, Boolean alarmAuto, Boolean alarmRepeat, Boolean alarmVib, Integer alarmVolume)
    {
        this.id = 0;
        this.busName = busName;
        this.busCode = busCode;
        this.alarmDate = alarmDate;
        this.alarmDays = alarmDays;
        this.alarmTime = alarmTime;
        this.alarmAuto = alarmAuto;
        this.alarmRepeat = alarmRepeat;
        this.alarmVib = alarmVib;
        this.alarmVolume = alarmVolume;
    }

    public AlarmInfo(Integer id, String busName, String busCode, Date   alarmDate, String alarmDays, String alarmTime, Boolean alarmAuto, Boolean alarmRepeat, Boolean alarmVib, Integer alarmVolume)
    {
        this.id = id;
        this.busName = busName;
        this.busCode = busCode;
        this.alarmDate = alarmDate;
        this.alarmDays = alarmDays;
        this.alarmTime = alarmTime;
        this.alarmAuto = alarmAuto;
        this.alarmRepeat = alarmRepeat;
        this.alarmVib = alarmVib;
        this.alarmVolume = alarmVolume;
    }

    public Integer getId() {
        return id;
    }

    public String getBusName(){
        return busName;
    }

    public String getBusCode() {
        return busCode;
    }

    public Date getAlarmDate(){
        return alarmDate;
    }

    public String getAlarmDays() {
        return alarmDays;
    }

    public String getAlarmTime(){
        return alarmTime;
    }

    public Boolean getAlarmAuto() {
        return alarmAuto;
    }

    public Boolean getAlarmRepeat(){
        return alarmRepeat;
    }

    public Boolean getAlarmVib(){
        return alarmVib;
    }

    public Integer getAlarmVolume() {
        return  alarmVolume;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setBusName(String busName){
        this.busName = busName;
    }

    public void setBusCode(String busCode) {
        this.busCode = busCode;
    }

    public void setAlarmDate(Date alarmDate){
        this.alarmDate =  alarmDate;
    }

    public void setAlarmDays(String alarmDays) {
        this.alarmDays = alarmDays;
    }

    public void setAlarmTime(String alarmTime){
        this.alarmTime = alarmTime;
    }

    public void setAlarmAuto(boolean alarmAuto) {
        this.alarmAuto =  alarmAuto;
    }

    public void setAlarmRepeat(boolean alarmRepeat){
        this.alarmRepeat = alarmRepeat;
    }

    public void setAlarmVib(boolean alarmVib){
        this.alarmVib = alarmVib;
    }

    public void setAlarmVolume(int alarmVolume) {
        this.alarmVolume = alarmVolume;
    }

    public static final String TABLE_NAME = "AlarmInfo";
    public static final String CREATE_TABLE_QUERY = "CREATE TABLE " + TABLE_NAME + "(" +
            "Id INTEGER PRIMARY KEY," +
            "bus_name TEXT NOT NULL," +
            "bus_code TEXT NOT NULL," +
            "alarm_date DATETIME," +
            "alarm_days TEXT," +
            "alarm_time TEXT," +
            "alarm_auto BOOLEAN," +
            "alarm_repeat BOOLEAN," +
            "alarm_vib BOOLEAN," +
            "alarm_volume INTEGER);" +
            "CREATE INDEX bus_code_index ON" + TABLE_NAME + " (bus_code);";


    public static final String DROP_TABLE_QUERY = "DROP TABLE IF EXISTS " + TABLE_NAME;
    public static final String TRUNCATE_TABLE_QUERY = "DELETE FROM " + TABLE_NAME + "; " +  "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = '" + TABLE_NAME + "';";
    public static final String DELETE_NOT_IN = "DELETE FROM " + TABLE_NAME + " WHERE Id NOT IN(%s);";

}
