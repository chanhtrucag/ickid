package kidsoft.buscare.Database.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by DELL3 on 2018-11-05.
 */

public class SeatInfo implements Parcelable {


    private Integer id = 0;

    private String busName;
    private String busCode;
    private String busSeat;

    private String seatPosition;
    private String seatNumber;
    private String seatStatus;
    private String seatBattery;

    private boolean sync, lowBattery = false;
    private int rssi;
    private Date syncTime;
    private int cntError, cntMoved = 0;
    private String preSeatStatus;
    private boolean cautionStatus = false;
    private boolean caution_error = false;
    private int AccMoving = 0;
    private boolean seatUser = false;

    public SeatInfo()
    {

    }

    public SeatInfo(String busName, String busCode, String busSeat, String seatPosition, String seatNumber, String seatStatus, String seatBattery)
    {
        this.id = 0;
        this.busName = busName;
        this.busCode = busCode;
        this.busSeat = busSeat;

        this.seatPosition = seatPosition;
        this.seatNumber = seatNumber;
        this.seatStatus = seatStatus;
        this.seatBattery = seatBattery;
    }

    public SeatInfo(Integer id, String busName, String busCode, String busSeat, String seatPosition, String seatNumber, String seatStatus, String seatBattery)
    {
        this.id = id;
        this.busName = busName;
        this.busCode = busCode;
        this.busSeat = busSeat;

        this.seatPosition = seatPosition;
        this.seatNumber = seatNumber;
        this.seatStatus = seatStatus;
        this.seatBattery = seatBattery;
    }


    public Integer getId() {
        return id;
    }

    public String getBusName(){
        return busName;
    }

    public String getBusCode() {
        return busCode;
    }

    public String getBusSeat() {
        return busSeat;
    }

    public String getSeatPosition(){
        return seatPosition;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public String getSeatStatus() {
        return seatStatus;
    }

    public String getSeatBattery() {
        return seatBattery;
    }

    public boolean getSync() {return sync; }
    public Date getSyncTime() {return syncTime; }
    public int getRssi() {return rssi; }
    public int getCntError() {return cntError; }
    public int getCntMoved() {return cntMoved; }
    public int getCntAccMoving() {return AccMoving;}
    public String getPreStatus(){return preSeatStatus;}
    public boolean getCautionStatus(){return cautionStatus;}

    public boolean getLowBattery(){return lowBattery; }
    public boolean getSeatUser(){return seatUser;}


    public void setId(Integer id) {
        this.id = id;
    }

    public void setBusName(String busName){
        this.busName = busName;
    }

    public void setBusCode(String busCode) {
        this.busCode = busCode;
    }

    public void setBusSeat(String busSeat) {
        this.busSeat = busSeat;
    }


    public void setSeatPosition(String seatPosition){
        this.seatPosition = seatPosition;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public void setSeatStatus(String seatStatus) {
        this.seatStatus = seatStatus;
    }

    public void setSeatBattery(String seatBattery) {
        this.seatBattery = seatBattery;
    }

    public void setSync(boolean sync) {this.sync = sync; }
    public void setSyncTime(Date syncTime) {this.syncTime = syncTime; }
    public void setRssi(int rssi) {this.rssi = rssi; }
    public void setPreSeatStatus(String status){this.preSeatStatus = status;}
    public void setCautionStatus(boolean status){this.cautionStatus = status;}

    public void setErrorStatus(boolean status){
        this.caution_error = status;
    }
    public boolean getErrorStatus() {
        return caution_error;
    }

    public void setCntError(int cntError) {
        if(cntError > 0)
        {
            this.cntError += cntError;
        }
        else if(cntError == -1)
        {
            this.cntError = 0;
        }
    }

    public void setCntMoved(int cntMoved) {
        if(cntMoved > 0)
        {
            this.cntMoved += cntMoved;
        }
        else if(cntMoved == -1)
        {
            this.cntMoved = 1;
        }
    }

    private void setAccMoving(int accMoving)
    {
        this.AccMoving += accMoving;
    }

    public void setLowBattery(boolean lowBattery){this.lowBattery = lowBattery; }
    public void setSeatUser(boolean seatUser){this.seatUser = seatUser;}



    public static final String TABLE_NAME = "SeatInfo";
    public static final String CREATE_TABLE_QUERY = "CREATE TABLE " + TABLE_NAME + "(" +
            "Id INTEGER PRIMARY KEY," +
            "bus_name TEXT NOT NULL," +
            "bus_code TEXT NOT NULL," +
            "bus_seat TEXT NOT NULL," +
            "seat_position TEXT NOT NULL," +
            "seat_number TEXT NOT NULL," +
            "seat_status TEXT NOT NULL," +
            "seat_battery TEXT NOT NULL);" +
            "CREATE INDEX seat_number_index ON" + TABLE_NAME + " (seat_number);";

    public static final String DROP_TABLE_QUERY = "DROP TABLE IF EXISTS " + TABLE_NAME;
    public static final String TRUNCATE_TABLE_QUERY = "DELETE FROM " + TABLE_NAME + "; " +  "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = '" + TABLE_NAME + "';";
    public static final String DELETE_NOT_IN = "DELETE FROM " + TABLE_NAME + " WHERE Id NOT IN(%s);";

    @Override
    public int describeContents() {
        return 0;
    }

    protected SeatInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);

        dest.writeString(busName);
        dest.writeString(busCode);
        dest.writeString(busSeat);

        dest.writeString(seatPosition);
        dest.writeString(seatNumber);
        dest.writeString(seatStatus);
        dest.writeString(seatBattery);

    }

    private void readFromParcel(Parcel parcel) {
        id = parcel.readInt();

        busName = parcel.readString();
        busCode = parcel.readString();
        busSeat = parcel.readString();

        seatPosition = parcel.readString();
        seatNumber = parcel.readString();
        seatStatus = parcel.readString();
        seatBattery = parcel.readString();


    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public SeatInfo createFromParcel(Parcel in) {
            return new SeatInfo(in);
        }

        @Override
        public SeatInfo[] newArray(int size) {
            return new SeatInfo[size];
        }
    };
}
