package kidsoft.buscare.Database.repositories;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kidsoft.buscare.Database.DBHelper;
import kidsoft.buscare.Database.models.BusInfo;
import kidsoft.buscare.Database.models.SeatInfo;
import kidsoft.buscare.Database.specifications.SqlSpecification;

/**
 * Created by DELL3 on 2018-11-05.
 */

public class BusRepository implements IRepository<BusInfo, Integer>{

    private DBHelper _dbHelper = null;
    private SQLiteDatabase db;

    public BusRepository(DBHelper dbHelper){
        this._dbHelper = dbHelper;
    }


    @Override
    public BusInfo Find(Integer id) {
        db = _dbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.query(BusInfo.TABLE_NAME, null, "Id = ?", new String[] { id.toString() }, null, null, null, null);

            if (cursor == null || cursor.getCount() < 1)
                return null;

            cursor.moveToFirst();
            BusInfo object = GetObjectFromCursor(cursor);
            cursor.close();

            return object;
        } finally {
            db.close();
        }
    }

    @Override
    public Integer Add(BusInfo busInfo) {
        db = _dbHelper.getWritableDatabase();
        ContentValues values = GetContentValues(busInfo, false);

        try {
            // insert row
            Long l_id = db.insert(BusInfo.TABLE_NAME, null, values);
            Integer id = l_id.intValue();
            busInfo.setId(id);
            return id;
        } finally {
            db.close();
        }
    }

    @Override
    public void Delete(BusInfo busInfo) {

        db = _dbHelper.getWritableDatabase();
        try {
            // delete row
            db.delete(BusInfo.TABLE_NAME, "Id = ?", new String[] {busInfo.getId().toString() });
        } finally {
            db.close();
        }
    }

    @Override
    public Integer Update(BusInfo busInfo) {
        db = _dbHelper.getWritableDatabase();
        ContentValues values = GetContentValues(busInfo, false);

        try {
            // update row
            return db.update(BusInfo.TABLE_NAME, values, "Id = ?", new String[] { busInfo.getId().toString() });
        } finally {
            db.close();
        }
    }


    public List<BusInfo> QueryByCode(String busName, String busCode)
    {
        db = _dbHelper.getReadableDatabase();
        List<BusInfo> objects = new ArrayList<>();

        try {
            Cursor cursor = db.rawQuery("SELECT * FROM " + BusInfo.TABLE_NAME + " WHERE bus_name='"+busName+"'" + " AND bus_code='"+busCode+"'", new String[]{});

            for (int i = 0, size = cursor.getCount(); i < size; i++) {
                cursor.moveToPosition(i);

                objects.add(GetObjectFromCursor(cursor));
            }

            cursor.close();
            return objects;

        } finally {

            db.close();
        }
    }

    public List<BusInfo> QueryByBusCode(String busCode) {
        db = _dbHelper.getReadableDatabase();
        List<BusInfo> objects = new ArrayList<>();

        try {
            Cursor cursor = db.rawQuery("SELECT * FROM BusInfo WHERE bus_code='"+busCode+"'", new String[]{});

            for (int i = 0, size = cursor.getCount(); i < size; i++) {
                cursor.moveToPosition(i);

                objects.add(GetObjectFromCursor(cursor));
            }

            cursor.close();

            return objects;
        } finally {
            db.close();
        }

    }




    public List<BusInfo> QueryByComp(String compCode) {
        db = _dbHelper.getReadableDatabase();
        List<BusInfo> objects = new ArrayList<>();

       // String.format("SELECT * FROM %s WHERE company_code = %s;", CompanyInfo.TABLE_NAME, this.company_code);

        try {
            Cursor cursor = db.rawQuery("SELECT * FROM BusInfo WHERE company_code='"+compCode+"'", new String[]{});

            for (int i = 0, size = cursor.getCount(); i < size; i++) {
                cursor.moveToPosition(i);

                objects.add(GetObjectFromCursor(cursor));
            }

            cursor.close();

            return objects;
        } finally {
            db.close();
        }
    }
    public boolean CheckDuplicate(String busName, String busCode)
    {
        db = _dbHelper.getReadableDatabase();
        List<BusInfo> objects = new ArrayList<>();

        try {
            Cursor cursor = db.rawQuery("SELECT * FROM " + BusInfo.TABLE_NAME + " WHERE bus_code='"+busCode+"'"+ " AND bus_name='"+busName+"'", new String[]{});

            Log.e("BusRepo", "Comp : " + busCode +  " " + cursor.getCount());

            if(cursor.getCount() == 0)
            {
                cursor.close();
                db.close();
                return false;
            }
            else
            {
                cursor.close();
                db.close();
                return true;
            }

        } finally {
        }
    }


    @Override
    public List<BusInfo> Query(SqlSpecification specification) {
        db = _dbHelper.getReadableDatabase();
        List<BusInfo> objects = new ArrayList<>();

        try {
            Cursor cursor = db.rawQuery(specification.toSqlQuery(), new String[]{});

            for (int i = 0, size = cursor.getCount(); i < size; i++) {
                cursor.moveToPosition(i);

                objects.add(GetObjectFromCursor(cursor));
            }

            cursor.close();

            return objects;
        } finally {
            db.close();
        }
    }

    public  String getBackupInfo(String busName, String busCode) {

        Gson gson = new Gson();

        //JSONArray resultSet = new JSONArray();

        List<BusInfo> businfos = new ArrayList<>();
        db = _dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + BusInfo.TABLE_NAME + " WHERE bus_name='"+busName+"'" + " AND bus_code='"+busCode+"'", new String[]{});

        for (int i = 0, size = cursor.getCount(); i < size; i++) {
            cursor.moveToPosition(i);

            businfos.add(GetObjectFromCursor(cursor));
        }
        cursor.close();
        db.close();

        List<SeatInfo> seatInfos = new ArrayList<>();
        seatInfos = _dbHelper.mSeat.QuerybyCode(busName, busCode);

        businfos.get(0).setSeats(seatInfos);
        String resultSet = gson.toJson(businfos.get(0));

        return resultSet;
    }


    public JSONArray getBackupBusInfo(String busName, String busCode) {

        db = _dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + BusInfo.TABLE_NAME + " WHERE bus_name='"+busName+"'" + " AND bus_code='"+busCode+"'", new String[]{});

        JSONArray resultSet = new JSONArray();
        JSONObject returnObj = new JSONObject();

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {

                    try {
                        if (cursor.getString(i) != null) {
                            //Log.e("BusRepo", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.e("BusRepo", e.getMessage());
                    }
                }

            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }

        cursor.close();
        //Log.e("BusRepo", resultSet.toString());

        return resultSet;
    }

    @Override
    public ContentValues GetContentValues(BusInfo busInfo, Boolean addkey) {
        ContentValues values = new ContentValues();

        if(addkey)
            values.put("Id", busInfo.getId());

        values.put("company_name", busInfo.getCompanyName());
        values.put("company_code",busInfo.getCompanyCode());

        values.put("bus_name", busInfo.getBusName());
        values.put("bus_code", busInfo.getBusCode());
        values.put("bus_color", busInfo.getBusColor());
        values.put("bus_seat", busInfo.getBusSeat());

        return values;
    }

    @Override
    public BusInfo GetObjectFromCursor(Cursor cursor) {
        return new BusInfo(
                cursor.getInt(cursor.getColumnIndex("Id")),

                cursor.getString(cursor.getColumnIndex("company_name")),
                cursor.getString(cursor.getColumnIndex("company_code")),

                cursor.getString(cursor.getColumnIndex("bus_name")),
                cursor.getString(cursor.getColumnIndex("bus_code")),
                cursor.getString(cursor.getColumnIndex("bus_color")),
                cursor.getString(cursor.getColumnIndex("bus_seat"))
        );
    }
}
