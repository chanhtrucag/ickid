package kidsoft.buscare.Database.repositories;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.List;

import kidsoft.buscare.Database.specifications.SqlSpecification;

/**
 * Created by Rene Heinsen on 3/6/2017.
 */

@SuppressWarnings("WeakerAccess")
public interface IRepository<TValue, TKey> {
    TValue Find(TKey id);
    TKey Add(TValue value);
    void Delete(TValue value);
    //TKey Delete (TKey id);
    Integer Update(TValue value);
    List<TValue> Query(SqlSpecification specification);
    ContentValues GetContentValues(TValue value, Boolean addkey);
    TValue GetObjectFromCursor (Cursor cursor);
}

