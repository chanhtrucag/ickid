package kidsoft.buscare.Database.repositories;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kidsoft.buscare.Database.DBHelper;
import kidsoft.buscare.Database.models.SeatInfo;
import kidsoft.buscare.Database.specifications.SqlSpecification;

/**
 * Created by DELL3 on 2018-11-05.
 */

public class SeatInfoRepository implements IRepository<SeatInfo, Integer>{

    private DBHelper _dbHelper = null;
    private SQLiteDatabase db;


    public SeatInfoRepository(DBHelper dbHelper){
        this._dbHelper = dbHelper;
    }

    @Override
    public SeatInfo Find(Integer id) {
        db = _dbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.query(SeatInfo.TABLE_NAME, null, "Id = ?", new String[] { id.toString() }, null, null, null, null);

            if (cursor == null || cursor.getCount() < 1)
                return null;

            cursor.moveToFirst();
            SeatInfo object = GetObjectFromCursor(cursor);
            cursor.close();

            return object;
        } finally {
            db.close();
        }
    }

    @Override
    public Integer Add(SeatInfo seatInfo) {
        db = _dbHelper.getWritableDatabase();
        ContentValues values = GetContentValues(seatInfo, false);

        try {
            // insert row
            Long l_id = db.insert(SeatInfo.TABLE_NAME, null, values);
            Integer id = l_id.intValue();
            seatInfo.setId(id);
            return id;
        } finally {
            db.close();
        }
    }

    @Override
    public void Delete(SeatInfo seatInfo) {
        db = _dbHelper.getWritableDatabase();
        try {
            // delete row
            db.delete(SeatInfo.TABLE_NAME, "Id = ?", new String[] {seatInfo.getId().toString() });
        } finally {
            db.close();
        }
    }

    @Override
    public Integer Update(SeatInfo seatInfo) {
        db = _dbHelper.getWritableDatabase();
        ContentValues values = GetContentValues(seatInfo, false);

        try {
            // update row
            return db.update(seatInfo.TABLE_NAME, values, "Id = ?", new String[] { seatInfo.getId().toString() });
        } finally {
            db.close();
        }
    }

    public List<SeatInfo> QuerybyCode(String busName, String busCode) {
        db = _dbHelper.getReadableDatabase();
        List<SeatInfo> objects = new ArrayList<>();

        //SELECT * FROM %s WHERE bus_code = %s;",SeatInfo.TABLE_NAME, this.busCode

        try {
            Cursor cursor = db.rawQuery("SELECT * FROM SeatInfo WHERE bus_code='"+busCode+"'" + " AND bus_name='"+busName+"'" , new String[]{});

            for (int i = 0, size = cursor.getCount(); i < size; i++) {
                cursor.moveToPosition(i);

                objects.add(GetObjectFromCursor(cursor));
            }

            cursor.close();

            return objects;
        } finally {
            db.close();
        }
    }

    public List<SeatInfo> QuerybySeatCode(String SeatCode, String BusName, String BusCode) {
        db = _dbHelper.getReadableDatabase();
        List<SeatInfo> objects = new ArrayList<>();

        //SELECT * FROM %s WHERE bus_code = %s;",SeatInfo.TABLE_NAME, this.busCode

        try {
            Cursor cursor = db.rawQuery("SELECT * FROM SeatInfo WHERE seat_number='"+SeatCode+"'" + " AND bus_name='"+BusName+"'" + " AND bus_code='"+BusCode+"'" , new String[]{});

            for (int i = 0, size = cursor.getCount(); i < size; i++) {
                cursor.moveToPosition(i);

                objects.add(GetObjectFromCursor(cursor));
            }

            cursor.close();

            return objects;
        } finally {
            db.close();
        }
    }


    @Override
    public List<SeatInfo> Query(SqlSpecification specification) {
        db = _dbHelper.getReadableDatabase();
        List<SeatInfo> objects = new ArrayList<>();

        try {
            Cursor cursor = db.rawQuery(specification.toSqlQuery(), new String[]{});

            for (int i = 0, size = cursor.getCount(); i < size; i++) {
                cursor.moveToPosition(i);

                objects.add(GetObjectFromCursor(cursor));
            }

            cursor.close();

            return objects;
        } finally {
            db.close();
        }
    }


    public JSONArray getBackupSeatInfo(String busName, String busCode) {

        db = _dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + SeatInfo.TABLE_NAME + " WHERE bus_name='"+busName+"'" + " AND bus_code='"+busCode+"'", new String[]{});

        JSONArray resultSet = new JSONArray();
        JSONObject returnObj = new JSONObject();

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {

                    try {
                        if (cursor.getString(i) != null) {
                            //Log.e("SeatRepo", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.e("SeatRepo", e.getMessage());
                    }
                }

            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }

        cursor.close();
        //Log.e("SeatRepo", resultSet.toString());
        return resultSet;
    }

    @Override
    public ContentValues GetContentValues(SeatInfo seatInfo, Boolean addkey) {
        ContentValues values = new ContentValues();

        if(addkey)
            values.put("Id", seatInfo.getId());

        values.put("bus_name", seatInfo.getBusName());
        values.put("bus_code", seatInfo.getBusCode());
        values.put("bus_seat", seatInfo.getBusSeat());

        values.put("seat_position", seatInfo.getSeatPosition());
        values.put("seat_number", seatInfo.getSeatNumber());
        values.put("seat_status", seatInfo.getSeatStatus());
        values.put("seat_battery", seatInfo.getSeatBattery());

        return values;
    }

    @Override
    public SeatInfo GetObjectFromCursor(Cursor cursor) {
        return new SeatInfo(
                cursor.getInt(cursor.getColumnIndex("Id")),

                cursor.getString(cursor.getColumnIndex("bus_name")),
                cursor.getString(cursor.getColumnIndex("bus_code")),
                cursor.getString(cursor.getColumnIndex("bus_seat")),

                cursor.getString(cursor.getColumnIndex("seat_position")),
                cursor.getString(cursor.getColumnIndex("seat_number")),
                cursor.getString(cursor.getColumnIndex("seat_status")),
                cursor.getString(cursor.getColumnIndex("seat_battery"))

        );
    }
}
