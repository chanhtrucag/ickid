package kidsoft.buscare.Database.repositories;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import kidsoft.buscare.Database.DBHelper;
import kidsoft.buscare.Database.models.CompanyInfo;
import kidsoft.buscare.Database.specifications.SqlSpecification;

/**
 * Created by DELL3 on 2018-11-05.
 */

public class CompanyRepository implements IRepository<CompanyInfo, Integer> {

    private DBHelper _dbHelper = null;
    private SQLiteDatabase db;

    public CompanyRepository(DBHelper dbHelper){
        this._dbHelper = dbHelper;
    }

    @Override
    public CompanyInfo Find(Integer id) {
        db = _dbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.query(CompanyInfo.TABLE_NAME, null, "Id = ?", new String[] { id.toString() }, null, null, null, null);

            if (cursor == null || cursor.getCount() < 1)
                return null;

            cursor.moveToFirst();
            CompanyInfo object = GetObjectFromCursor(cursor);
            cursor.close();

            return object;
        } finally {
            db.close();
        }
    }

    @Override
    public Integer Add(CompanyInfo companyInfo) {
        db = _dbHelper.getWritableDatabase();
        ContentValues values = GetContentValues(companyInfo, false);

        try {
            // insert row
            Long l_id = db.insert(CompanyInfo.TABLE_NAME, null, values);
            Integer id = l_id.intValue();
            companyInfo.setId(id);
            return id;
        } finally {
            db.close();
        }
    }

    @Override
    public void Delete(CompanyInfo companyInfo) {
        db = _dbHelper.getWritableDatabase();
        try {
            // delete row
            db.delete(CompanyInfo.TABLE_NAME, "Id = ?", new String[] {companyInfo.getId().toString() });
        } finally {
            db.close();
        }
    }

    @Override
    public Integer Update(CompanyInfo companyInfo) {
        db = _dbHelper.getWritableDatabase();
        ContentValues values = GetContentValues(companyInfo, false);

        try {
            // update row
            return db.update(CompanyInfo.TABLE_NAME, values, "Id = ?", new String[] { companyInfo.getId().toString() });
        } finally {
            db.close();
        }
    }

    public boolean CheckDuplicate(String compCode)
    {
        db = _dbHelper.getReadableDatabase();
        List<CompanyInfo> objects = new ArrayList<>();

        try {
            Cursor cursor = db.rawQuery("SELECT * FROM " + CompanyInfo.TABLE_NAME + " WHERE company_code='"+compCode+"'", new String[]{});


            //Log.e("CompanyRepo", "Comp : " + compCode +  " " + cursor.getCount());

            if(cursor.getCount() == 0)
            {
                cursor.close();
                db.close();
                return false;
            }
            else
            {
                cursor.close();
                db.close();
                return true;
            }

        } finally {
        }
    }

    @Override
    public List<CompanyInfo> Query(SqlSpecification specification) {
        db = _dbHelper.getReadableDatabase();
        List<CompanyInfo> objects = new ArrayList<>();

        try {
            Cursor cursor = db.rawQuery(specification.toSqlQuery(), new String[]{});

            for (int i = 0, size = cursor.getCount(); i < size; i++) {
                cursor.moveToPosition(i);

                objects.add(GetObjectFromCursor(cursor));
            }

            cursor.close();

            return objects;
        } finally {
            db.close();
        }
    }

    @Override
    public ContentValues GetContentValues(CompanyInfo companyInfo, Boolean addkey) {

        ContentValues values = new ContentValues();

        if(addkey)
            values.put("Id", companyInfo.getId());

        values.put("company_name", companyInfo.getCompanyName());
        values.put("company_code", companyInfo.getCompanyCode());

        return values;
    }

    @Override
    public CompanyInfo GetObjectFromCursor(Cursor cursor) {

        return new CompanyInfo(
                cursor.getInt(cursor.getColumnIndex("Id")),
                cursor.getString(cursor.getColumnIndex("company_name")),
                cursor.getString(cursor.getColumnIndex("company_code"))
        );
    }
}
