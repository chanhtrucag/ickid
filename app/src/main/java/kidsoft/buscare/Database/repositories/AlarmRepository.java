package kidsoft.buscare.Database.repositories;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import kidsoft.buscare.Database.DBHelper;
import kidsoft.buscare.Database.models.AlarmInfo;
import kidsoft.buscare.Database.specifications.SqlSpecification;

/**
 * Created by DELL3 on 2018-11-05.
 */

public class AlarmRepository implements IRepository<AlarmInfo, Integer>{

    private DBHelper _dbHelper = null;
    private SQLiteDatabase db;

    public AlarmRepository(DBHelper dbHelper){
        this._dbHelper = dbHelper;
    }


    @Override
    public AlarmInfo Find(Integer id) {
        db = _dbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.query(AlarmInfo.TABLE_NAME, null, "Id = ?", new String[] { id.toString() }, null, null, null, null);

            if (cursor == null || cursor.getCount() < 1)
                return null;

            cursor.moveToFirst();
            AlarmInfo object = GetObjectFromCursor(cursor);
            cursor.close();

            return object;
        } finally {
            db.close();
        }
    }

    @Override
    public Integer Add(AlarmInfo alarmInfo) {
        db = _dbHelper.getWritableDatabase();
        ContentValues values = GetContentValues(alarmInfo, false);

        try {
            // insert row
            Long l_id = db.insert(AlarmInfo.TABLE_NAME, null, values);
            Integer id = l_id.intValue();
            alarmInfo.setId(id);
            return id;
        } finally {
            db.close();
        }
    }

    @Override
    public void Delete(AlarmInfo alarmInfo) {
        db = _dbHelper.getWritableDatabase();
        try {
            // delete row
            db.delete(AlarmInfo.TABLE_NAME, "Id = ?", new String[] {alarmInfo.getId().toString() });
        } finally {
            db.close();
        }
    }

    @Override
    public Integer Update(AlarmInfo alarmInfo) {
        db = _dbHelper.getWritableDatabase();
        ContentValues values = GetContentValues(alarmInfo, false);

        try {
            // update row
            return db.update(AlarmInfo.TABLE_NAME, values, "Id = ?", new String[] { alarmInfo.getId().toString() });
        } finally {
            db.close();
        }
    }

    public  List<AlarmInfo> GetAllAlarms() {
        db = _dbHelper.getReadableDatabase();
        List<AlarmInfo> objects = new ArrayList<>();

        try {
            Cursor cursor = db.rawQuery("SELECT * FROM AlarmInfo"   , new String[]{});

            for (int i = 0, size = cursor.getCount(); i < size; i++) {
                cursor.moveToPosition(i);
                objects.add(GetObjectFromCursor(cursor));
            }
            cursor.close();
            return objects;
        } finally {
            db.close();
        }


    }

    public  List<AlarmInfo> QueryByCode(String BusName, String BusCode) {

        db = _dbHelper.getReadableDatabase();
        List<AlarmInfo> objects = new ArrayList<>();
        try {
            Cursor cursor = db.rawQuery("SELECT * FROM AlarmInfo WHERE bus_name='"+BusName+"'" + " AND bus_code='"+BusCode+"'" , new String[]{});

            for (int i = 0, size = cursor.getCount(); i < size; i++) {
                cursor.moveToPosition(i);

                objects.add(GetObjectFromCursor(cursor));
            }

            cursor.close();

            return objects;
        } finally {
            db.close();
        }

    }


    @Override
    public List<AlarmInfo> Query(SqlSpecification specification) {
        db = _dbHelper.getReadableDatabase();
        List<AlarmInfo> objects = new ArrayList<>();

        try {
            Cursor cursor = db.rawQuery(specification.toSqlQuery(), new String[]{});

            for (int i = 0, size = cursor.getCount(); i < size; i++) {
                cursor.moveToPosition(i);

                objects.add(GetObjectFromCursor(cursor));
            }

            cursor.close();

            return objects;
        } finally {
            db.close();
        }
    }

    @Override
    public ContentValues GetContentValues(AlarmInfo alarmInfo, Boolean addkey) {
        ContentValues values = new ContentValues();

        if(addkey)
            values.put("Id", alarmInfo.getId());

        values.put("bus_name", alarmInfo.getBusName());
        values.put("bus_code", alarmInfo.getBusCode());
        values.put("alarm_date", alarmInfo.getAlarmDate().getTime());
        values.put("alarm_days", alarmInfo.getAlarmDays());
        values.put("alarm_time", alarmInfo.getAlarmTime());
        values.put("alarm_auto", alarmInfo.getAlarmAuto() ? 1 : 0);
        values.put("alarm_repeat", alarmInfo.getAlarmRepeat() ? 1 : 0);
        values.put("alarm_vib", alarmInfo.getAlarmVib() ? 1 : 0);
        values.put("alarm_volume", alarmInfo.getAlarmVolume());


        return values;
    }

    @Override
    public AlarmInfo GetObjectFromCursor(Cursor cursor) {
        return new AlarmInfo(
                cursor.getInt(cursor.getColumnIndex("Id")),

                cursor.getString(cursor.getColumnIndex("bus_name")),
                cursor.getString(cursor.getColumnIndex("bus_code")),
                new Date(cursor.getLong(cursor.getColumnIndex("alarm_date"))),
                cursor.getString(cursor.getColumnIndex("alarm_days")),
                cursor.getString(cursor.getColumnIndex("alarm_time")),
                cursor.getInt(cursor.getColumnIndex("alarm_auto")) > 0,
                cursor.getInt(cursor.getColumnIndex("alarm_repeat")) > 0,
                cursor.getInt(cursor.getColumnIndex("alarm_vib")) > 0,
                cursor.getInt(cursor.getColumnIndex("alarm_volume"))
        );
    }
}
