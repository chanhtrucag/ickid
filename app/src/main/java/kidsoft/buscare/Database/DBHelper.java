package kidsoft.buscare.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import kidsoft.buscare.BusInfo.SeatInfoActivity;
import kidsoft.buscare.Database.models.AlarmInfo;
import kidsoft.buscare.Database.models.BusInfo;
import kidsoft.buscare.Database.models.CompanyInfo;
import kidsoft.buscare.Database.models.SeatInfo;
import kidsoft.buscare.Database.repositories.AlarmRepository;
import kidsoft.buscare.Database.repositories.BusRepository;
import kidsoft.buscare.Database.repositories.CompanyRepository;
import kidsoft.buscare.Database.repositories.SeatInfoRepository;

/**
 * Created by DELL3 on 2018-11-05.
 */

public class DBHelper extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "BusCare.db";
    public static final Integer DATABASE_VERSION = 2;
    // ver 1 add CompanyInfo, BusInfo, SeatInfo
    // ver 2 add AlarmInfo

    public final CompanyRepository mComp;
    public final BusRepository mBus;
    public final SeatInfoRepository mSeat;
    public final AlarmRepository mAlarm;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mComp = new CompanyRepository(this);
        mBus = new BusRepository(this);
        mSeat = new SeatInfoRepository(this);
        mAlarm = new AlarmRepository(this);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Create tables
        db.execSQL(CompanyInfo.CREATE_TABLE_QUERY);
        db.execSQL(BusInfo.CREATE_TABLE_QUERY);
        db.execSQL(SeatInfo.CREATE_TABLE_QUERY);
        db.execSQL(AlarmInfo.CREATE_TABLE_QUERY);

        Log.e("DBHelper" , "DB Build");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if(oldVersion < newVersion)
        {
            Log.e("DBHelper" , "old " + oldVersion + "->" + "new " + newVersion);
            db.execSQL(AlarmInfo.CREATE_TABLE_QUERY);
        }

        // Create tables again
        //onCreate(db);

    }
}
