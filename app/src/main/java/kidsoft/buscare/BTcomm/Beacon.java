package kidsoft.buscare.BTcomm;

/**
 * Created by DELL3 on 2018-11-09.
 */

public class Beacon {

    private String address, name, data, value;
    private int rssi;
    private String now;
    private String status;

    public Beacon(String address, int rssi, String now, String name, String data, String value, String status) {

        this.address = address;
        this.name =  name;
        this.data = data;
        this.value = value;
        this.status = status;

        this.rssi = rssi;
        this.now = now;
    }

    public String getAddress() {
        return address;
    }
    public String getName() {
        return name;
    }
    public String getData() {
        return data;
    }
    public String getValue() {
        return value;
    }

    public String getStatus() {
        return status;
    }
    public int getRssi() {
        return rssi;
    }
    public String getNow() {
        return now;
    }




}
