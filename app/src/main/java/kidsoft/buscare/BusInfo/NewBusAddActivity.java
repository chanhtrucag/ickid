package kidsoft.buscare.BusInfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import kidsoft.buscare.BaseActivity;
import kidsoft.buscare.Database.DBHelper;
import kidsoft.buscare.R;
import layout.BusList.AdapterBusList;

public class NewBusAddActivity extends BaseActivity {

    public final static String MINI_BUS = "3X5", SMALL_BUS = "4X6", MEDIUM_BUS = "5X9", ETC_BUS = "5X11";

    private static final int REQUEST_CODE_SET_BUS_INFO = 1000;


    private String company_name, company_code;
    private EditText etBusName, etBusNumber;
    private RadioGroup rgBusColor;
    private Spinner spBusSeat;

    private ImageView btCancel;
    private Button btNext;

    private String bus_name, bus_number;
    private String bus_seat = "";
    private int bus_color = AdapterBusList.GREY;

    private DBHelper _dbHelper;
    public static Activity fa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_bus_add);

        fa = this;

        company_name = getIntent().getStringExtra("company_name");
        company_code = getIntent().getStringExtra("company_code");

        etBusName = (EditText)findViewById(R.id.busAdd_name);
        etBusNumber = (EditText)findViewById(R.id.busAdd_code);
        rgBusColor = (RadioGroup)findViewById(R.id.busAdd_color);
        spBusSeat = (Spinner)findViewById(R.id.busAdd_size);

        _dbHelper = new DBHelper(getApplicationContext());

        //ArrayAdapter adapter = new ArrayAdapter(this,R.layout.spinner_text_color);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_text_color)
                {

                    public View getView(int position, View convertView, ViewGroup parent) {
                        View v = super.getView(position, convertView, parent);

                        //((TextView) v).setTextSize(16);
                        ((TextView) v).setTextColor(getResources().getColorStateList(R.color.spinnerTextColor));

                        return v;
                    }

                    public View getDropDownView(int position, View convertView, ViewGroup parent) {
                        View v = super.getDropDownView(position, convertView, parent);
                        //v.setBackgroundResource(R.drawable.spinner_bg);

                        ((TextView) v).setTextColor(getResources().getColorStateList(R.color.spinnerTextColor));

                        //((TextView) v).setTypeface(fontStyle);
                        //((TextView) v).setGravity(Gravity.CENTER);

                        return v;
                    }
                };

//        adapter.add("좌석 배열       미지정");
//        adapter.add("좌석 배열       3X5 (9인승)");
//        adapter.add("좌석 배열       4X6 (12인승)");
//        adapter.add("좌석 배열       5X9 (20인승)");
//        adapter.add("좌석 배열       5X11(기타)");
        adapter.add("Sơ đồ        Không xác định");
        adapter.add("Sơ đồ        3X5 (9 chỗ)");
        adapter.add("Sơ đồ        4X6 (12 chỗ)");
        adapter.add("Sơ đồ        5X9 (20 chỗ)");
        adapter.add("Sơ đồ        5X11(55 chỗ)");


        spBusSeat.setAdapter(adapter);

        btCancel = findViewById(R.id.busBackBtn);
        btNext = findViewById(R.id.busAdd_next_btn);

        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("result","cancel");
                setResult(RESULT_OK,resultIntent);
                finish();
            }
        });

        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(etBusName.getText().toString().isEmpty() == false)
                {
                    if(etBusNumber.getText().toString().isEmpty() == false)
                    {
                        if(bus_seat.isEmpty() == false)
                        {
                            if(_dbHelper.mBus.CheckDuplicate(etBusName.getText().toString(), etBusNumber.getText().toString()) == true)
                            {
//                                Log.e("AdpaterBusSeat", "Already resisted number" );
//                                AlertDialog.Builder alt_bld = new AlertDialog.Builder(NewBusAddActivity.this);
//                                alt_bld.setTitle("이미 등록된 차량 정보가 있습니다");
//                                alt_bld.setMessage("다른 이름이나 번호로 등록해주세요.").setCancelable(
//                                        false).setPositiveButton("확인",
//                                        new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.cancel();
//                                            }
//                                        });
//                                AlertDialog alert = alt_bld.create();
//                                alert.show();
                                Log.e("AdpaterBusSeat", "Already resisted number" );
                                AlertDialog.Builder alt_bld = new AlertDialog.Builder(NewBusAddActivity.this);
                                alt_bld.setTitle("Tên xe hoặc biển số xe này đã được đăng ký");
                                alt_bld.setMessage("Vui lòng đăng ký với tên xe hoặc biển số xe khác.").setCancelable(
                                        false).setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                AlertDialog alert = alt_bld.create();
                                alert.show();
                            }
                            else
                            {
                                setSeatInfo();
                            }

                        }
                    }
                    else
                    {

                    }
                }
                else
                {

                }
            }
        });


        rgBusColor.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId)
                {
                    case R.id.bus_grey:
                        bus_color = AdapterBusList.GREY;
                        break;
                    case R.id.bus_red:
                        bus_color = AdapterBusList.RED;
                        break;
                    case R.id.bus_orange:
                        bus_color = AdapterBusList.ORANGE;
                        break;
                    case R.id.bus_yellow:
                        bus_color = AdapterBusList.YELLOW;
                        break;
                    case R.id.bus_green:
                        bus_color = AdapterBusList.GREEN;
                        break;
                    case R.id.bus_blue:
                        bus_color = AdapterBusList.BLUE;
                        break;
                    default:
                        bus_color = AdapterBusList.GREY;
                        break;
                }
            }
        });

        spBusSeat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                switch (position)
                {
                    case 0 :
                        bus_seat = "";
                        break;
                    case 1 :
                        bus_seat = MINI_BUS;
                        break;
                    case 2 :
                        bus_seat = SMALL_BUS;
                        break;
                    case 3 :
                        bus_seat = MEDIUM_BUS;
                        break;
                    case 4 :
                        bus_seat = ETC_BUS;
                        break;
                    default:
                        bus_seat = "";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }



    void setSeatInfo()
    {
        Intent i = new Intent(getApplicationContext(), SeatInfoActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);

        //company_name = "수원유치원";
        //company_code = "12AD24564";

        i.putExtra("company_name", company_name);
        i.putExtra("company_code", company_code);

        i.putExtra("bus_name", etBusName.getText().toString());
        i.putExtra("bus_number", etBusNumber.getText().toString());
        i.putExtra("bus_color", bus_color);
        i.putExtra("bus_seat", bus_seat);

        startActivity(i);
        //finish();


    }

    /*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if((resultCode == RESULT_OK) && (requestCode == REQUEST_CODE_SET_BUS_INFO)) {
            setResult(RESULT_OK, data);
            finish();
        }
    }
    */
}
