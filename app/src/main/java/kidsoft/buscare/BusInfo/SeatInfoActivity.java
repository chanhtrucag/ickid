package kidsoft.buscare.BusInfo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import kidsoft.buscare.BaseActivity;
import kidsoft.buscare.BusInfoCamera.BarcodeScanActivity;
import kidsoft.buscare.BusSeatService.BusSeatDeleteDialog;
import kidsoft.buscare.Database.DBHelper;
import kidsoft.buscare.Database.models.BusInfo;
import kidsoft.buscare.Database.models.CompanyInfo;
import kidsoft.buscare.Database.models.SeatInfo;
import kidsoft.buscare.Database.specifications.BusInfoAll;
import kidsoft.buscare.Database.specifications.BusInfoByCompCode;
import kidsoft.buscare.Database.specifications.CompanyInfoAll;
import kidsoft.buscare.Database.specifications.SeatInfoByBusCode;
import kidsoft.buscare.Network.NetworkService;
import kidsoft.buscare.R;
import layout.BusSeat.AdapterBusSeat;
import layout.BusSeat.CenterItem;
import layout.BusSeat.ItemBusSeat;
import layout.BusSeat.OnSeatSelected;

public class SeatInfoActivity extends BaseActivity implements OnSeatSelected {

    public static final String TAG = "SeatInfoActivity";

    public static final int REQUEST_CODE_SET_BUS_INFO = 1000;
    public static final int REQUEST_CODE_SET_SEAT_INFO = 2000;

    private int COLUMNS = 4;

    private String company_name, company_code;
    private String bus_name, bus_number;
    private String bus_seat, bus_color;
    private int number_of_seat = 0, bus_color_int;

    private RecyclerView recyclerView;
    private AdapterBusSeat adapter;

    private Button btReg, btDelOk, btDelCancel;
    public  Button  btDelAll;
    private ImageView btBack, btDel;
    private RelativeLayout bssTopDetail;
    
    private CompanyInfo mComp;
    private BusInfo mBus;
    private ArrayList<SeatInfo> mSeat;
    
    private DBHelper _dbHelper;
    //-- Net private NetworkService mNetworkService;

    private boolean doDelete =  false, delAll = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seat_info);


        company_name = getIntent().getStringExtra("company_name");
        company_code = getIntent().getStringExtra("company_code");

        bus_name = getIntent().getStringExtra("bus_name");
        bus_number = getIntent().getStringExtra("bus_number");
        bus_color_int = getIntent().getIntExtra("bus_color", 0);
        bus_color = Integer.toString(bus_color_int);
        bus_seat = getIntent().getStringExtra("bus_seat");

        _dbHelper = new DBHelper(getApplicationContext());

        if(bus_seat.equals(NewBusAddActivity.MINI_BUS)) {
            COLUMNS = 3;
            number_of_seat = 15;
        }else if(bus_seat.equals(NewBusAddActivity.SMALL_BUS)) {
            COLUMNS = 4;
            number_of_seat = 24;
        }else if(bus_seat.equals(NewBusAddActivity.MEDIUM_BUS)) {
            COLUMNS = 5;
            number_of_seat = 45;
        }else if(bus_seat.equals(NewBusAddActivity.ETC_BUS)) {
            COLUMNS = 5;
            number_of_seat = 55;
        }else {
            COLUMNS = 5;
            number_of_seat = 55;
        }

        List<ItemBusSeat> items = new ArrayList<>();

        for(int i=0; i<number_of_seat; ++i)
        {
            items.add(new CenterItem(String.valueOf(i)));
        }

        bssTopDetail = findViewById(R.id.busSeatTopDetail);

        GridLayoutManager manager = new GridLayoutManager(this, COLUMNS);
        recyclerView = (RecyclerView) findViewById(R.id.busSeat_items);
        recyclerView.setLayoutManager(manager);

        adapter = new AdapterBusSeat(this, items, this);
        recyclerView.setAdapter(adapter);

        btDel = findViewById(R.id.busSeatDelete);
        btDelOk = findViewById(R.id.busSeatDelOk);
        btDelCancel= findViewById(R.id.busSeatDelCancel);


        btReg = findViewById(R.id.busReg_btn);
        btReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBusInfo();
            }
        });

        btBack = findViewById(R.id.busSeatBackBtn);
        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AlertDialog.Builder alt_bld = new AlertDialog.Builder(SeatInfoActivity.this);
//                alt_bld.setTitle("이전 페이지로 돌아가시겠습니까?");
//                alt_bld.setMessage("이전 페이지로 돌아가시면\n남겨진 좌석 정보는 삭제됩니다.").setCancelable(
//                        false).setPositiveButton("확인",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.cancel();
//                                finish();
//                            }
//                        }).setNegativeButton("취소",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.cancel();
//                            }
//                        });
//                AlertDialog alert = alt_bld.create();
//                alert.show();
                AlertDialog.Builder alt_bld = new AlertDialog.Builder(SeatInfoActivity.this);
                alt_bld.setTitle("Xác nhận trở về trang trước?");
                alt_bld.setMessage("Nếu trở về trang trước\ntoàn bộ thông tin ghế chưa được lưu sẽ bị xóa.").setCancelable(
                        false).setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        }).setNegativeButton("Hủy",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = alt_bld.create();
                alert.show();
            }
        });

        btDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                doDelete = true;
                bssTopDetail.setVisibility(View.GONE);
                btDelOk.setVisibility(View.VISIBLE);
                btDelCancel.setVisibility(View.VISIBLE);
                btDelAll.setVisibility(View.VISIBLE);
                adapter.clearDelSelection();
            }
        });

        btDelAll  = findViewById(R.id.BusSeatDelAll);
        btDelAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //if(delAll == false)
                {
                    btDelAll.setTextColor(Color.parseColor("#60a9ff"));
                    delAll = true;
                    List<Integer> items = adapter.getSelectedItems();
                    for(int i = 0; i < items.size(); ++i)
                    {
                        if(adapter.isSelectedForDel(items.get(i)) == false)
                        {
                            adapter.selectToDelete(items.get(i));
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
                /*
                else
                {
                    btDelAll.setTextColor(Color.parseColor("#6b6b6b"));
                    delAll =  false;
                    List<Integer> delitems = adapter.getSelectedDeleteItems();
                    for(int i = 0; i < delitems.size(); ++i)
                    {
                        if(adapter.isSelectedForDel(delitems.get(i)) == true)
                        {
                            adapter.selectToDelete(delitems.get(i));
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
                */
            }
        });

        btDelOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final BusSeatDeleteDialog busSeatDeleteDialog = new BusSeatDeleteDialog(SeatInfoActivity.this, adapter.getSelectedIDeltemCount());
                busSeatDeleteDialog.show();
                busSeatDeleteDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if(busSeatDeleteDialog.getResult() == true)
                        {
                            btDelAll.setTextColor(Color.parseColor("#6b6b6b"));

                            bssTopDetail.setVisibility(View.VISIBLE);
                            btDelOk.setVisibility(View.GONE);
                            btDelCancel.setVisibility(View.GONE);
                            btDelAll.setVisibility(View.GONE);

                            //List<Integer> items = adapter.getSelectedItems();
                            List<Integer> delitems = adapter.getSelectedDeleteItems();
                            for(int i = 0; i<  delitems.size(); ++i)
                            {
                                Log.e(TAG, "del item list " + delitems.get(i));

                                adapter.selectToDelete(delitems.get(i));
                                adapter.toggleSelection(delitems.get(i));
                                adapter.getItemInfo(delitems.get(i)).setSeatInfo("");
                                adapter.notifyDataSetChanged();
                            }

                            doDelete = false;
                            adapter.clearDelSelection();
                        }
                    }
                });

            }
        });

        btDelCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btDelAll.setTextColor(Color.parseColor("#6b6b6b"));

                bssTopDetail.setVisibility(View.VISIBLE);
                btDelOk.setVisibility(View.GONE);
                btDelCancel.setVisibility(View.GONE);
                btDelAll.setVisibility(View.GONE);

                List<Integer> delitems = adapter.getSelectedDeleteItems();
                for(int i = 0; i<  delitems.size(); ++i)
                {
                    Log.e(TAG, "del canceled item list " + delitems.get(i));
                    adapter.selectToDelete(delitems.get(i));
                    adapter.notifyDataSetChanged();
                }

                doDelete = false;
                delAll = false;
                adapter.clearDelSelection();
            }
        });


    }

    public void onBackPressed() {

//        AlertDialog.Builder alt_bld = new AlertDialog.Builder(SeatInfoActivity.this);
//        alt_bld.setTitle("이전 페이지로 돌아가시겠습니까?");
//            alt_bld.setMessage("이전 페이지로 돌아가시면\n남겨진 좌석 정보는 삭제됩니다.").setCancelable(
//                    false).setPositiveButton("확인",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.cancel();
//                            finish();
//                        }
//                    }).setNegativeButton("취소",
//                    new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.cancel();
//                        }
//            });
//        AlertDialog alert = alt_bld.create();
//        alert.show();

        AlertDialog.Builder alt_bld = new AlertDialog.Builder(SeatInfoActivity.this);
        alt_bld.setTitle("Xác nhận trở về trang trước?");
        alt_bld.setMessage("Nếu trở về trang trước\ntoàn bộ thông tin ghế chưa được lưu sẽ bị xóa.").setCancelable(
                false).setPositiveButton("확인",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                }).setNegativeButton("Hủy",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alt_bld.create();
        alert.show();
    }


    public boolean getDoDelete()
    {
        return this.doDelete;
    }

    private void setBusInfo()
    {
        //-- Net  mNetworkService = new NetworkService(this);
        List<Integer> items = adapter.getSelectedItems();

        Log.e(TAG, "---------- company_name : " + company_name);
        Log.e(TAG, "---------- company_code : " + company_code);
        Log.e(TAG, "---------- bus_name : " + bus_name);
        Log.e(TAG, "---------- bus_number : " + bus_number);
        Log.e(TAG, "---------- bus_color : " + bus_color);
        Log.e(TAG, "---------- bus_seat : " + bus_seat);

        mComp = new CompanyInfo();
        mBus = new BusInfo();
        mSeat = new ArrayList<SeatInfo>();

        //mComp.setCompanyName(company_name);
        //mComp.setCompanyCode(company_code);

        mBus.setCompanyName(company_name);
        mBus.setCompanyCode(company_code);
        mBus.setBusName(bus_name);
        mBus.setBusCode(bus_number);
        mBus.setBusColor(bus_color);
        mBus.setBusSeat(bus_seat);

        //_dbHelper.mComp.Add(mComp);
        _dbHelper.mBus.Add(mBus);
        //-- Net  mNetworkService.saveBusInfo(mBus, items.size());


        List<CompanyInfo> mTmpComp = _dbHelper.mComp.Query(new CompanyInfoAll());
        for(int i=0; i<mTmpComp.size(); ++i)
        {
            Log.e(TAG, "---------- in DB, regi company_ : " + mTmpComp.get(i).getCompanyName() + " " + mTmpComp.get(i).getCompanyCode() );
        }

        //List<BusInfo> mTmpBus = _dbHelper.mBus.Query(new BusInfoAll());
        List<BusInfo> mTmpBus = _dbHelper.mBus.QueryByComp(company_code);

        for(int i=0; i<mTmpBus.size(); ++i)
        {
            Log.e(TAG, "---------- in DB, regi bus_ : " + mTmpBus.get(i).getCompanyName() + " " + mTmpBus.get(i).getBusSeat() + " / " + mTmpBus.get(i).getBusCode() );
        }



        for(int i=0; i < items.size(); ++i)
        {
            SeatInfo mSeat = new SeatInfo();

            mSeat.setBusName(bus_name);
            mSeat.setBusCode(bus_number);
            mSeat.setBusSeat(bus_seat);

            mSeat.setSeatNumber(adapter.getItemInfo(items.get(i)).getSeatSerialNumber());
            mSeat.setSeatPosition(items.get(i).toString());
            mSeat.setSeatStatus("status_empty");
            mSeat.setSeatBattery("-1");

            _dbHelper.mSeat.Add(mSeat);
            //-- Net  mNetworkService.saveSeatInfo(mSeat);

            Log.e(TAG, "---------- regi seat  : " + items.get(i) + " " + adapter.getItemInfo(items.get(i)).getSeatSerialNumber());
        }

        List<SeatInfo> tmp = _dbHelper.mSeat.QuerybyCode(bus_name, bus_number);
        //List<SeatInfo> tmp = _dbHelper.mSeat.Query(new SeatInfoByBusCode(bus_number));

        for(int i=0; i<tmp.size(); ++i)
        {
            Log.e(TAG, "---------- in DB, regi seat_ : " +  " " + tmp.get(i).getSeatNumber() + " / " + tmp.get(i).getSeatPosition() + "/ "+ tmp.get(i).getBusCode() );
        }


        NewBusAddActivity.fa.finish();
        Intent resultIntent = new Intent();
        resultIntent.putExtra("result","add");
        setResult(RESULT_OK,resultIntent);
        finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        adapter.onActivityResult(requestCode, resultCode, data);
        if((resultCode == RESULT_OK) && (requestCode == REQUEST_CODE_SET_SEAT_INFO)) {

            if(data.getStringExtra("result_seat").toString().contains("CANCEL_REGI") == false)
            {
                Log.e(TAG, "-------------------------result seat: " + data.getStringExtra("result_seat"));
            }
            else
            {
                Log.e(TAG, "-------------------------Canceled" );
            }
        }
    }

    @Override
    public void onSeatSelected(int count) {

    }
}
